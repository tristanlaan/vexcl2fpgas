#include "datatypes.hpp"

#define WIDTH 16
#define LOGWIDTH 4
#define LWIDTH 8
#define LOGLWIDTH 3


#define BURSTBUFFERSIZE 256
#define BURSTBUFFERSPACE 16
#define LBURSTBUFFERSPACE 32

extern "C" {
    #if defined(cl_khr_fp64)
    #  pragma OPENCL EXTENSION cl_khr_fp64: enable
    #elif defined(cl_amd_fp64)
    #  pragma OPENCL EXTENSION cl_amd_fp64: enable
    #endif

    void affinetrans
            (
            int n,
            lfix_v *prm_1,
            const fix_v *prm_2,
            const fix_v *prm_3_1,
            const fix_v *prm_3_2_expr_1,
            int prm_3_2_slice_1,
            int prm_3_2_slice_2,
            int prm_3_2_slice_3,
            int prm_3_2_slice_4,
            int prm_3_start,
            int prm_3_length0,
            int prm_3_stride0,
            int prm_3_length1,
            int prm_3_stride1
            )
    {
        #pragma HLS INTERFACE m_axi port=prm_1 offset=slave bundle=aximm1 max_write_burst_length = 32
        #pragma HLS INTERFACE m_axi port=prm_2 offset=slave bundle=aximm2 max_read_burst_length = 32
        #pragma HLS INTERFACE m_axi port=prm_3_1 offset=slave bundle=aximm2 max_read_burst_length = 32
        #pragma HLS INTERFACE m_axi port=prm_3_2_expr_1 offset=slave bundle=aximm3 max_read_burst_length = 32

        #pragma HLS INTERFACE s_axilite port=n
        #pragma HLS INTERFACE s_axilite port=prm_1
        #pragma HLS INTERFACE s_axilite port=prm_2
        #pragma HLS INTERFACE s_axilite port=prm_3_1
        #pragma HLS INTERFACE s_axilite port=prm_3_2_expr_1
        #pragma HLS INTERFACE s_axilite port=prm_3_2_slice_1
        #pragma HLS INTERFACE s_axilite port=prm_3_2_slice_2
        #pragma HLS INTERFACE s_axilite port=prm_3_2_slice_3
        #pragma HLS INTERFACE s_axilite port=prm_3_2_slice_4
        #pragma HLS INTERFACE s_axilite port=prm_3_stride0
        #pragma HLS INTERFACE s_axilite port=prm_3_length0
        #pragma HLS INTERFACE s_axilite port=prm_3_length1
        #pragma HLS INTERFACE s_axilite port=prm_3_stride1

        fix_v burstbuffer1[BURSTBUFFERSPACE];
        fix_v burstbuffer2[BURSTBUFFERSPACE];
        fix_v burstbuffer3[BURSTBUFFERSPACE];
        lfix_v prm_3_sum[LBURSTBUFFERSPACE];
        lfix_v burstbuffer4[LBURSTBUFFERSPACE];

        for (int idx = 0; idx < n; idx += BURSTBUFFERSIZE) {

            int chunk_size = BURSTBUFFERSIZE;
            // boundary checks
            if ((idx + BURSTBUFFERSIZE) > n) {
                chunk_size = n - idx;
            }

            for (int j = 0; j < chunk_size; ++j) {
                int p = idx + j;
                burstbuffer1[j >> LOGWIDTH].data[j % WIDTH] = prm_2[p >> LOGWIDTH].data[p % WIDTH];
            }

            for (int j = 0; j < chunk_size; ++j) {
                lfix_t *sum = &prm_3_sum[j >> LOGLWIDTH].data[j % LWIDTH];
                *sum = 0;
                {
                    int pos = idx + j;
                    int ptr1 = prm_3_start + (pos % prm_3_length0) * prm_3_stride0;
                    for(int i1 = 0, ptr2 = ptr1; i1 < prm_3_length1; i1 += BURSTBUFFERSIZE, ptr2 += prm_3_stride1 * BURSTBUFFERSIZE)
                    {
                        int idx = ptr2;

                        int chunk_size2 = BURSTBUFFERSIZE;
                        // boundary checks
                        if ((i1 + BURSTBUFFERSIZE) > prm_3_length1) {
                            chunk_size2 = prm_3_length1 - i1;
                        }

                        for (int j2 = 0; j2 < chunk_size2; ++j2) {
                            int p1 = idx + j2;
                            int p2 = ( prm_3_2_slice_1 * (((prm_3_2_slice_2 + idx + j2) / prm_3_2_slice_3) % prm_3_2_slice_4 ));
                            burstbuffer2[j2 >> LOGWIDTH].data[j2 % WIDTH] = prm_3_1[p1 >> LOGWIDTH].data[p1 % WIDTH];
                            burstbuffer3[j2 >> LOGWIDTH].data[j2 % WIDTH] = prm_3_2_expr_1[p2 >> LOGWIDTH].data[p2 % WIDTH];
                        }

                        for (int j2 = 0; j2 < chunk_size2; ++j2) {
                            *sum += (burstbuffer2[j2 >> LOGWIDTH].data[j2 % WIDTH] * burstbuffer3[j2 >> LOGWIDTH].data[j2 % WIDTH]);
                        }
                    }
                }
            }

            for (int j = 0; j < chunk_size; ++j) {
                int p = idx + j;
                burstbuffer4[j >> LOGLWIDTH].data[j % LWIDTH] = burstbuffer1[j >> LOGWIDTH].data[j % WIDTH] + prm_3_sum[j / 8].data[j % LWIDTH];
            }

            for (int j = 0; j < chunk_size; ++j) {
                int p = idx + j;
                prm_1[p >> LOGLWIDTH].data[p % LWIDTH] = burstbuffer4[j >> LOGLWIDTH].data[j % LWIDTH];
            }

        }
    }
}
