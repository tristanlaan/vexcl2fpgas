#pragma once

#ifdef __GNUC__
// save diagnostic state
#pragma GCC diagnostic push
// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wint-in-bool-context"
#endif

#include "ap_fixed.h"

#ifdef __GNUC__
// turn the warnings back on
#pragma GCC diagnostic pop
#endif

typedef ap_fixed<18, 7, AP_RND, AP_SAT> fix_t;
typedef ap_fixed<64, 53, AP_RND, AP_SAT> lfix_t;

typedef struct {
    fix_t data[16];
} fix_v;

typedef struct {
    lfix_t data[8];
} lfix_v;
