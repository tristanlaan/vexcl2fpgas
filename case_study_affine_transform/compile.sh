#!/bin/bash
# Change paths to your installation directories if necessary
source /data/tools/Xilinx/Vitis/2020.2/settings64.sh
source /opt/xilinx/xrt/setup.sh
export PLATFORM_REPO_PATHS=/opt/xilinx/platforms/xilinx_u250_gen3x16_xdma_3_1_202020_1/

declare -a directories=("vitis" "vitis_opt1" "vitis_opt2" "vitis_opt3" "vitis_opt4" "vitis_opt5")

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

for dir in ${directories[@]}; do
        cd $SCRIPT_DIR/$dir/hw
        { time make; } 2>&1 | tee ../../${dir}_compilation.out
done
