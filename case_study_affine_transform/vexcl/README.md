# Affine transformation implemented in VexCL

## Requirements
VexCL requires the boost lib. On Ubuntu this can be installed with
`sudo apt update && sudo apt install libboost-all-dev`.

## Build
Run the following commands:
```shell
cmake .
make
./affinetrans > affinetranskernel.cl
```
