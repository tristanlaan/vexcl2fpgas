#include <iostream>
#include <vector>
#include <nlohmann/json.hpp>
#include <filesystem>
namespace fs = std::filesystem;
using json = nlohmann::json;

#define CL_TARGET_OPENCL_VERSION 120
#define VEXCL_SHOW_KERNELS

#include <vexcl/vexcl.hpp>

template <typename T>
int get_size(T array) {
    int total = 1;
    for (auto &e : array) {
        total *= e;
    }
    return total;
}

template <typename Vec>
void read_input(char *filename, Vec &a, Vec &x, Vec &y) {
    fs::path json_file(filename);
    std::error_code ec;

    if (!fs::exists(json_file, ec) || ec) {
        std::cerr << "ERROR: " << json_file << " is not a valid file" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::ifstream i(filename);
    json js;
    i >> js;

    size_t m = js.at("A").at("size")[0];
    size_t n = js.at("A").at("size")[1];

    a.resize(m * n);

    for (size_t i = 0; i < m; ++i) {
        for (size_t j = 0; j < n; ++j) {
            a[i * n + j] = js.at("A").at("data")[i][j];
        }
    }

    if (js.at("x").at("size")[0] != n) {
        std::cerr << "ERROR: " << "A and x size mismatch" << std::endl;
        exit(EXIT_FAILURE);
    }

    x.resize(n);

    for (size_t i = 0; i < n; ++i) {
        x[i] = js.at("x").at("data")[i];
    }

    if (js.at("y").at("size")[0] != m) {
        std::cerr << "ERROR: " << "A and y size mismatch" << std::endl;
        exit(EXIT_FAILURE);
    }

    y.resize(m);

    for (size_t i = 0; i < m; ++i) {
        y[i] = js.at("y").at("data")[i];
    }
}

template <typename Vec>
void generate_input(Vec &a, Vec &x, Vec &y) {
    size_t m = 250;
    size_t n = 250;
    a.resize(m * n);
    x.resize(n);
    y.resize(m);

    for (size_t i = 0; i < n; ++i) {
        x[i] = (double) ((n - i) % 64);
    }

    for (size_t i = 0; i < m; ++i) {
        y[i] = (double) (i % 64);
    }

    for (size_t i = 0; i < m; ++i) {
        for (size_t j = 0; j < n; ++j) {
            if (i == j) {
                a[i * n + j] = 1.0;
            } else {
                a[i * n + j] = ((i > j) ? 1.0 : -1.0) * (((i - j) % 64) / 10.0);
            }
        }
    }
}

template <typename Vec>
void print_input(std::ostream& ostream, size_t m, size_t n, Vec X, Vec Y,
                 Vec A) {
    ostream << "x = [";
    for (size_t i = 0; i < n; ++i) {
        ostream << X[i];
        if (i != n - 1) {
            ostream << ", ";
        }
    }
    ostream << "]" << std::endl;

    ostream << "y = [";
    for (size_t i = 0; i < m; ++i) {
        ostream << Y[i];
        if (i != m - 1) {
            ostream << ", ";
        }
    }
    ostream << "]" << std::endl;

    ostream << "A = [";
    for (size_t i = 0; i < m; ++i) {
        ostream << "[";
        for (size_t j = 0; j < n; ++j) {
            ostream << A[i * n + j];
            if (j != n - 1) {
                ostream << ", ";
            }
        }
        ostream << "]";
        if (i != m - 1) {
            ostream << "," << std::endl << "     ";
        }
    }
    ostream << "]" << std::endl;
}

template <typename Vec>
std::string vector_to_string(size_t m, Vec T) {
    std::ostringstream ostream;
    ostream << "[";
    for (size_t i = 0; i < m; ++i) {
        ostream << T[i];
        if (i != m - 1) {
            ostream << ", ";
        }
    }
    ostream << "]";

    return ostream.str();
}

template <typename Vec>
Vec calculate_results(size_t m, size_t n, Vec A, Vec x, Vec y) {
    Vec res = Vec(m * n);

    for (size_t i = 0; i < m; ++i) {
        double sum = 0;

        // Calculate matrix multiplication of current row
        for (size_t j = 0; j < n; ++j) {
            sum += A[i * n + j] * x[j];
        }

        // Store results
        res[i] = y[i] + sum;
    }

    return res;
}

template <typename Vec>
bool verify_results(std::ostream& ostream, size_t m, size_t n, Vec t,  Vec A, Vec x, Vec y) {
    Vec ref = calculate_results(m, n, A, x, y);
    std::string ref_str = vector_to_string(m, ref);
    std::string t_str = vector_to_string(m, t);

    for (size_t i = 0; i < m; ++i) {
        double err = abs((t[i] - ref[i]) / ref[i]);
        if (err > 0.01) {
            ostream << "ERROR: results mismatch, " << t[i] << " != " << ref[i] << " (err: " << err << ")" <<  std::endl;
            if (std::getenv("VERBOSE")) {
                ostream << "t = y + A * x = " << t_str << std::endl;
                ostream << "reference = " << ref_str << std::endl;
            }
            return false;
        }
    }

    ostream << "Results correctly verified" << std::endl;
    if (std::getenv("VERBOSE")) {
        ostream << "t = y + A * x = " << t_str << std::endl;
    }

    return true;
}

/* Make a m×n matrix of x by replicating the vector m times vertically and
 * multiplying the new matrix with matrix A elementwise.
 * Then reduce to a vector of size M by summing horizontally.
 */
template <class M, class V>
auto prod(size_t m, size_t n, M &&A, V &&x) {
    using namespace vex;
    // Specify M×N matrix shape.
    auto MxN = extents[m][n];
    // Reshape x to a matrix by copying x into each row of X.
    auto X = reshape(x, MxN, extents[1]);
    // Multiply A with X elementwise.
    auto E = A * X;
    // Reduce matrix E to a vector of size M by summing over dimension 1.
    return reduce<SUM>(MxN, E, 1);
}

int main(int argc, char **argv) {
    vex::Context ctx(vex::Filter::DoublePrecision);
    std::cerr << ctx << std::endl;

    std::vector<double> a, x, y;

    if (argc > 1) {
        read_input(argv[1], a, x, y);
    } else {
        generate_input(a, x, y);
    }

    size_t m = y.size();
    size_t n = x.size();

    std::vector<double> t(m);

    std::cerr << "Read input" << std::endl;

    if (std::getenv("VERBOSE")) {
        print_input(std::cerr, m, n, x, y, a);
    }

    auto start = std::chrono::steady_clock::now();

    // Transfer host-side doubles into device-side cl_double vectors
    vex::vector<cl_double> A(ctx, a.size(), reinterpret_cast<cl_double*>(a.data()));
    vex::vector<cl_double> X(ctx, x.size(), reinterpret_cast<cl_double*>(x.data()));
    vex::vector<cl_double> Y(ctx, y.size(), reinterpret_cast<cl_double*>(y.data()));
    // Store result in T
    vex::vector<cl_double> T(ctx, t.size());

    T = Y + prod(m, n, A, X);

    vex::copy(T.begin(), T.end(), reinterpret_cast<cl_double*>(t.data()));

    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    std::ofstream time;
    time.open("time.txt", std::ios::out | std::ios::trunc);
    time << elapsed.count() << std::endl;
    time.close();

    if (!verify_results(std::cerr, m, n, t, a, x, y)) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
