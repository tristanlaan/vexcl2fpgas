#include <iostream>
#include <sstream>
#include <vector>

#if __GNUC__ > 8 || (__GNUC__ == 8 && __GNUC_MINOR__ >= 1)
    #include <filesystem>
    namespace fs = std::filesystem;
#else
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#endif

#include "xcl2.hpp"
#include "datatypes.hpp"
#include "extern_vars.hpp"

template <typename Vec>
void print_input(std::ostream& ostream, size_t m, size_t n, Vec X, Vec Y,
                 Vec A) {
    ostream << "X = [";
    for (size_t i = 0; i < n; ++i) {
        ostream << X[i];
        if (i != n - 1) {
            ostream << ", ";
        }
    }
    ostream << "]" << std::endl;

    ostream << "Y = [";
    for (size_t i = 0; i < m; ++i) {
        ostream << Y[i];
        if (i != m - 1) {
            ostream << ", ";
        }
    }
    ostream << "]" << std::endl;

    ostream << "A = [";
    for (size_t i = 0; i < m; ++i) {
        ostream << "[";
        for (size_t j = 0; j < n; ++j) {
            ostream << A[i * n + j];
            if (j != n - 1) {
                ostream << ", ";
            }
        }
        ostream << "]";
        if (i != m - 1) {
            ostream << "," << std::endl << "     ";
        }
    }
    ostream << "]" << std::endl;
}

template <typename Vec>
std::string vector_to_string(size_t m, Vec T) {
    std::ostringstream ostream;
    ostream << "[";
    for (size_t i = 0; i < m; ++i) {
        ostream << T[i];
        if (i != m - 1) {
            ostream << ", ";
        }
    }
    ostream << "]";

    return ostream.str();
}

template <typename Vec>
std::vector<double> calculate_results(size_t m, size_t n, Vec A, Vec x, Vec y) {
    auto res = std::vector<double>(m * n);

    for (size_t i = 0; i < m; ++i) {
        double sum = 0;

        // Calculate matrix multiplication of current row
        for (size_t j = 0; j < n; ++j) {
            sum += A[i * n + j].to_double() * x[j].to_double();
        }

        // Store results
        res[i] = y[i].to_double() + sum;
    }

    return res;
}

template <typename Vec, typename lVec>
bool verify_results(std::ostream& ostream, size_t m, size_t n, lVec t,  Vec A, Vec x, Vec y) {
    std::vector<double> ref = calculate_results(m, n, A, x, y);
    std::string ref_str = vector_to_string(m, ref);
    std::string t_str = vector_to_string(m, t);

    for (size_t i = 0; i < m; ++i) {
        double err = abs((t[i].to_double() - ref[i]) / ref[i]);
        if (err > 0.01) {
            ostream << "ERROR: results mismatch, " << t[i] << " != " << ref[i] << " (err: " << err << ")" <<  std::endl;

            if (std::getenv("VERBOSE")) {
                ostream << "t = y + A * x = " << t_str << std::endl;
                ostream << "reference = " << ref_str << std::endl;
            }
            return false;
        }
    }

    ostream << "Results correctly verified" << std::endl;
    if (std::getenv("VERBOSE")) {
        ostream << "t = y + A * x = " << t_str << std::endl;
    }

    return true;
}

int main(int argc, char **argv) {
    // Initialize the OpenCL environment
    cl_int err;
    std::string binaryFile = "affinetrans.xclbin";
    std::vector<cl::Device> devices = xcl::get_xilinx_devices();
    const cl::Device device = devices.front();
    cl::Context context(device, NULL, NULL, NULL, &err);
    std::vector<unsigned char> fileBuf = xcl::read_binary_file(binaryFile);
    cl::Program::Binaries bins{{reinterpret_cast<char*>(fileBuf.data()), fileBuf.size()}};
    cl::Program program(context, devices, bins, NULL, &err);
    cl::CommandQueue q(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
    cl::Kernel krnl_affine_transform(program, "affinetrans", &err);

    std::vector<fix_t, aligned_allocator<fix_t>> a(A_data, A_data + A_size), x(x_data, x_data + x_size), y(y_data, y_data + y_size);

    size_t m = y.size();
    size_t n = x.size();

    std::vector<lfix_t, aligned_allocator<lfix_t>> t(m);

    std::cerr << "Read input" << std::endl;


    if (std::getenv("VERBOSE")) {
        print_input(std::cerr, m, n, x, y, a);
    }

    auto start = std::chrono::steady_clock::now();

    // Create the buffers and allocate memory
    cl::Buffer A(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(fix_t) * a.size(), a.data(), &err);
    cl::Buffer X(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(fix_t) * x.size(), x.data(), &err);
    cl::Buffer Y(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(fix_t) * y.size(), y.data(), &err);
    cl::Buffer T(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, sizeof(lfix_t) * t.size(), t.data(), &err);

    // Set kernel arguments
    krnl_affine_transform.setArg(0, (int) m);
    krnl_affine_transform.setArg(1, T);
    krnl_affine_transform.setArg(2, Y);
    krnl_affine_transform.setArg(3, A);
    krnl_affine_transform.setArg(4, X);
    krnl_affine_transform.setArg(5, 1);
    krnl_affine_transform.setArg(6, 0);
    krnl_affine_transform.setArg(7, 1);
    krnl_affine_transform.setArg(8, (int) n);
    krnl_affine_transform.setArg(9, 0);
    krnl_affine_transform.setArg(10, (int) m);
    krnl_affine_transform.setArg(11, (int) n);
    krnl_affine_transform.setArg(12, (int) n);
    krnl_affine_transform.setArg(13, 1);

    // Schedule transfer of inputs to device memory, execution of kernel, and transfer of outputs back to host memory
    q.enqueueMigrateMemObjects({Y, A, X}, 0);
    q.enqueueTask(krnl_affine_transform);
    q.enqueueMigrateMemObjects({T}, CL_MIGRATE_MEM_OBJECT_HOST);

    // Wait for all scheduled operations to finish
    q.finish();

    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    std::ofstream time;
    time.open("time.txt", std::ios::out | std::ios::trunc);
    time << elapsed.count() << std::endl;
    time.close();

    if (!verify_results(std::cerr, m, n, t, a, x, y)) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
