#!/usr/bin/env python3
from pathlib import Path

def generate_config_file(file: Path, cu_count: int, mem_interface_per_param: bool):
    with file.open('w') as f:
        print("platform=xilinx_u250_gen3x16_xdma_3_1_202020_1", file=f)
        print("debug=1", file=f)
        print("save-temps=1", file=f)

        print(file=f)
        print("[connectivity]", file=f)
        print(f"nk=affinetrans:{cu_count}:{'.'.join([f'affinetrans_{i}' for i in range(1, cu_count + 1)])}", file=f)

        for i in range(cu_count):
            print(file=f)
            print(f"sp=affinetrans_{i + 1}.prm_1:DDR[{0 if mem_interface_per_param else i % 4}]", file=f)
            print(f"sp=affinetrans_{i + 1}.prm_2:DDR[{1 if mem_interface_per_param else i % 4}]", file=f)
            print(f"sp=affinetrans_{i + 1}.prm_3_1:DDR[{2 if mem_interface_per_param else i % 4}]", file=f)
            print(f"sp=affinetrans_{i + 1}.prm_3_2_expr_1:DDR[{3 if mem_interface_per_param else i % 4}]", file=f)

        print(file=f)
        print("[profile]", file=f)
        print("data=all:all:all", file=f)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description="Generate affine transformation configuration file")
    parser.add_argument('-f', '--file', type=lambda f: Path(f).absolute(), metavar='FILE',
                        required=True, help="File to store configuration")
    parser.add_argument('-m', '--mem', type=bool, default=False, metavar='BOOL',
                        help="Use different memory interface per parameter")
    parser.add_argument('cu_count', type=int, metavar='CU_COUNT',
                        help="Amount of compute units to use")
    args = parser.parse_args()

    generate_config_file(args.file, args.cu_count, args.mem)
