#include <iostream>
#include <sstream>
#include <vector>
#include <nlohmann/json.hpp>

#if __GNUC__ > 8 || (__GNUC__ == 8 && __GNUC_MINOR__ >= 1)
    #include <filesystem>
    namespace fs = std::filesystem;
#else
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#endif

using json = nlohmann::json;

#include "xcl2.hpp"
#include "datatypes.hpp"

template <typename T>
int get_size(T array) {
    int total = 1;
    for (auto &e : array) {
        total *= e;
    }
    return total;
}

template <typename Vec>
void read_input(char *filename, Vec &a, Vec &x, Vec &y) {
    fs::path json_file(filename);
    std::error_code ec;

    if (!fs::exists(json_file, ec) || ec) {
        std::cerr << "ERROR: " << json_file << " is not a valid file" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::ifstream i(filename);
    json js;
    i >> js;

    size_t m = js.at("A").at("size")[0];
    size_t n = js.at("A").at("size")[1];

    a.resize(m * n);

    for (size_t i = 0; i < m; ++i) {
        for (size_t j = 0; j < n; ++j) {
            double value = js.at("A").at("data")[i][j];
            a[i * n + j] = value;
        }
    }

    if (js.at("x").at("size")[0] != n) {
        std::cerr << "ERROR: " << "A and x size mismatch" << std::endl;
        exit(EXIT_FAILURE);
    }

    x.resize(n);

    for (size_t i = 0; i < n; ++i) {
        double value = js.at("x").at("data")[i];
        x[i] = value;
    }

    if (js.at("y").at("size")[0] != m) {
        std::cerr << "ERROR: " << "A and y size mismatch" << std::endl;
        exit(EXIT_FAILURE);
    }

    y.resize(m);

    for (size_t i = 0; i < m; ++i) {
        double value = js.at("y").at("data")[i];
        y[i] = value;
    }
}

template <typename Vec>
void generate_input(Vec &a, Vec &x, Vec &y) {
    size_t m = 250;
    size_t n = 250;
    a.resize(m * n);
    x.resize(n);
    y.resize(m);

    for (size_t i = 0; i < n; ++i) {
        x[i] = (double) n - i;
    }

    for (size_t i = 0; i < m; ++i) {
        y[i] = (double) i;
    }

    for (size_t i = 0; i < m; ++i) {
        for (size_t j = 0; j < n; ++j) {
            if (i == j) {
                a[i * n + j] = 1.0;
            } else {
                a[i * n + j] = (double) i - j;
            }
        }
    }
}

template <typename Vec>
void print_input(std::ostream& ostream, size_t m, size_t n, Vec X, Vec Y,
                 Vec A) {
    ostream << "X = [";
    for (size_t i = 0; i < n; ++i) {
        ostream << X[i];
        if (i != n - 1) {
            ostream << ", ";
        }
    }
    ostream << "]" << std::endl;

    ostream << "Y = [";
    for (size_t i = 0; i < m; ++i) {
        ostream << Y[i];
        if (i != m - 1) {
            ostream << ", ";
        }
    }
    ostream << "]" << std::endl;

    ostream << "A = [";
    for (size_t i = 0; i < m; ++i) {
        ostream << "[";
        for (size_t j = 0; j < n; ++j) {
            ostream << A[i * n + j];
            if (j != n - 1) {
                ostream << ", ";
            }
        }
        ostream << "]";
        if (i != m - 1) {
            ostream << "," << std::endl << "     ";
        }
    }
    ostream << "]" << std::endl;
}

template <typename Vec>
std::string vector_to_string(size_t m, Vec T) {
    std::ostringstream ostream;
    ostream << "[";
    for (size_t i = 0; i < m; ++i) {
        ostream << T[i];
        if (i != m - 1) {
            ostream << ", ";
        }
    }
    ostream << "]";

    return ostream.str();
}

template <typename Vec>
std::vector<double> calculate_results(size_t m, size_t n, Vec A, Vec x, Vec y) {
    auto res = std::vector<double>(m * n);

    for (size_t i = 0; i < m; ++i) {
        double sum = 0;

        // Calculate matrix multiplication of current row
        for (size_t j = 0; j < n; ++j) {
            sum += A[i * n + j].to_double() * x[j].to_double();
        }

        // Store results
        res[i] = y[i].to_double() + sum;
    }

    return res;
}

template <typename Vec, typename lVec>
bool verify_results(std::ostream& ostream, size_t m, size_t n, lVec t,  Vec A, Vec x, Vec y) {
    std::vector<double> ref = calculate_results(m, n, A, x, y);
    std::string ref_str = vector_to_string(m, ref);
    std::string t_str = vector_to_string(m, t);

    for (size_t i = 0; i < m; ++i) {
        double err = abs((t[i].to_double() - ref[i]) / ref[i]);
        if (err > 0.01) {
            ostream << "ERROR: results mismatch, " << t[i] << " != " << ref[i] << " (err: " << err << ")" <<  std::endl;

            if (std::getenv("VERBOSE")) {
                ostream << "t = y + A * x = " << t_str << std::endl;
                ostream << "reference = " << ref_str << std::endl;
            }
            return false;
        }
    }

    ostream << "Results correctly verified" << std::endl;
    if (std::getenv("VERBOSE")) {
        ostream << "t = y + A * x = " << t_str << std::endl;
    }

    return true;
}

int main(int argc, char **argv) {
    // Initialize the OpenCL environment
    cl_int err;
    std::string binaryFile = "affinetrans.xclbin";
    std::vector<cl::Device> devices = xcl::get_xilinx_devices();
    const cl::Device device = devices.front();
    cl::Context context(device, NULL, NULL, NULL, &err);
    std::vector<unsigned char> fileBuf = xcl::read_binary_file(binaryFile);
    cl::Program::Binaries bins{{reinterpret_cast<char*>(fileBuf.data()), fileBuf.size()}};
    cl::Program program(context, devices, bins, NULL, &err);
    cl::CommandQueue q(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
    cl::Kernel krnl_affine_transform(program, "affinetrans", &err);

    std::vector<fix_t, aligned_allocator<fix_t>> a, x, y;

    if (argc > 1) {
        read_input(argv[1], a, x, y);
    } else {
        generate_input(a, x, y);
    }

    size_t m = y.size();
    size_t n = x.size();

    std::vector<lfix_t, aligned_allocator<lfix_t>> t(m);

    std::cerr << "Read input" << std::endl;

    if (std::getenv("VERBOSE")) {
        print_input(std::cerr, m, n, x, y, a);
    }

    auto start = std::chrono::steady_clock::now();

    // Create the buffers and allocate memory
    cl::Buffer A(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(fix_t) * a.size(), a.data(), &err);
    cl::Buffer X(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(fix_t) * x.size(), x.data(), &err);
    cl::Buffer Y(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(fix_t) * y.size(), y.data(), &err);
    cl::Buffer T(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, sizeof(lfix_t) * t.size(), t.data(), &err);

    // Set kernel arguments
    krnl_affine_transform.setArg(0, (int) m);
    krnl_affine_transform.setArg(1, T);
    krnl_affine_transform.setArg(2, Y);
    krnl_affine_transform.setArg(3, A);
    krnl_affine_transform.setArg(4, X);
    krnl_affine_transform.setArg(5, (int) n);
    krnl_affine_transform.setArg(6, (int) n);
    krnl_affine_transform.setArg(7, (int) n);

    // Schedule transfer of inputs to device memory, execution of kernel, and transfer of outputs back to host memory
    q.enqueueMigrateMemObjects({Y, A, X}, 0);
    q.enqueueTask(krnl_affine_transform);
    q.enqueueMigrateMemObjects({T}, CL_MIGRATE_MEM_OBJECT_HOST);

    // Wait for all scheduled operations to finish
    q.finish();

    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    std::ofstream time;
    time.open("time.txt", std::ios::out | std::ios::trunc);
    time << elapsed.count() << std::endl;
    time.close();

    if (!verify_results(std::cerr, m, n, t, a, x, y)) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
