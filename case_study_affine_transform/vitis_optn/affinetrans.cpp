#include <hls_stream.h>
#include "datatypes.hpp"

static void read_input(const fix_t *prm_2, const fix_t *prm_3_1, const fix_t *prm_3_2, hls::stream<fix_t>& inStream, int size, int width) {
    for (int i = 0; i < size; ++i) {
        int ptr = i * width;

        for (int j = 0; j < width; ++j) {
            inStream << prm_3_1[ptr + j];
            inStream << prm_3_2[j];
        }

        inStream << prm_2[i];
    }
}

static void compute(hls::stream<fix_t>& inStream,
                    hls::stream<lfix_t>& outStream,
                    int prm_3_2_slice_4,
                    int prm_3_stride0,
                    int prm_3_length1,
                    int size) {
        for(int idx = 0; idx < size; ++idx)
        {
            #pragma HLS LOOP_TRIPCOUNT min = size max = size
            lfix_t prm_3_sum = 0;
            {
                int ptr1 = idx * prm_3_stride0;
                for(int idx2 = 0; idx2 < prm_3_length1; ++idx2)
                {
                    prm_3_sum = prm_3_sum + (inStream.read() * inStream.read());
                }
            }
            outStream << ( inStream.read() + prm_3_sum );
        }
}

static void write_output(lfix_t *out, hls::stream<lfix_t>& outStream, int size) {
    for (int i = 0; i < size; ++i) {
        out[i] = outStream.read();
    }
}


extern "C" {
    #if defined(cl_khr_fp64)
    #  pragma OPENCL EXTENSION cl_khr_fp64: enable
    #elif defined(cl_amd_fp64)
    #  pragma OPENCL EXTENSION cl_amd_fp64: enable
    #endif

    void affinetrans
            (
            int n,
            lfix_t *prm_1,
            const fix_t *prm_2,
            const fix_t *prm_3_1,
            const fix_t *prm_3_2_expr_1,
            int prm_3_2_slice_4,
            int prm_3_stride0,
            int prm_3_length1
            )
    {
        static hls::stream<fix_t> inStream("input_stream");
        static hls::stream<lfix_t> outStream("output_stream");
        #pragma HLS STREAM variable = inStream depth = 32
        #pragma HLS STREAM variable = outStream depth = 32

        #pragma HLS dataflow
        read_input(prm_2, prm_3_1, prm_3_2_expr_1, inStream, n, prm_3_length1);
        compute(inStream, outStream, prm_3_2_slice_4, prm_3_stride0, prm_3_length1, n);
        write_output(prm_1, outStream, n);
    }
}
