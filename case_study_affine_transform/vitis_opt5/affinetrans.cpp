#include "datatypes.hpp"

#define BURSTBUFFERSIZE 256

extern "C" {
    #if defined(cl_khr_fp64)
    #  pragma OPENCL EXTENSION cl_khr_fp64: enable
    #elif defined(cl_amd_fp64)
    #  pragma OPENCL EXTENSION cl_amd_fp64: enable
    #endif

    void affinetrans
            (
            int n,
            lfix_t *prm_1,
            const fix_t *prm_2,
            const fix_t *prm_3_1,
            const fix_t *prm_3_2_expr_1,
            int prm_3_2_slice_1,
            int prm_3_2_slice_2,
            int prm_3_2_slice_3,
            int prm_3_2_slice_4,
            int prm_3_start,
            int prm_3_length0,
            int prm_3_stride0,
            int prm_3_length1,
            int prm_3_stride1
            )
    {
        #pragma HLS INTERFACE m_axi port=prm_1 offset=slave bundle=aximm1 max_write_burst_length = 256
        #pragma HLS INTERFACE m_axi port=prm_2 offset=slave bundle=aximm2 max_read_burst_length = 256
        #pragma HLS INTERFACE m_axi port=prm_3_1 offset=slave bundle=aximm2 max_read_burst_length = 256
        #pragma HLS INTERFACE m_axi port=prm_3_2_expr_1 offset=slave bundle=aximm3 max_read_burst_length = 256

        #pragma HLS INTERFACE s_axilite port=n
        #pragma HLS INTERFACE s_axilite port=prm_1
        #pragma HLS INTERFACE s_axilite port=prm_2
        #pragma HLS INTERFACE s_axilite port=prm_3_1
        #pragma HLS INTERFACE s_axilite port=prm_3_2_expr_1
        #pragma HLS INTERFACE s_axilite port=prm_3_2_slice_1
        #pragma HLS INTERFACE s_axilite port=prm_3_2_slice_2
        #pragma HLS INTERFACE s_axilite port=prm_3_2_slice_3
        #pragma HLS INTERFACE s_axilite port=prm_3_2_slice_4
        #pragma HLS INTERFACE s_axilite port=prm_3_stride0
        #pragma HLS INTERFACE s_axilite port=prm_3_length0
        #pragma HLS INTERFACE s_axilite port=prm_3_length1
        #pragma HLS INTERFACE s_axilite port=prm_3_stride1

        fix_t burstbuffer1[BURSTBUFFERSIZE];
        fix_t burstbuffer2[BURSTBUFFERSIZE];
        fix_t burstbuffer3[BURSTBUFFERSIZE];
        lfix_t prm_3_sum[BURSTBUFFERSIZE];
        lfix_t burstbuffer4[BURSTBUFFERSIZE];

        for (int idx = 0; idx < n; idx += BURSTBUFFERSIZE) {

            int chunk_size = BURSTBUFFERSIZE;
            // boundary checks
            if ((idx + BURSTBUFFERSIZE) > n) {
                chunk_size = n - idx;
            }

            for (int j = 0; j < chunk_size; ++j) {
                burstbuffer1[j] = prm_2[idx + j];
            }

            for (int j = 0; j < chunk_size; ++j) {
                prm_3_sum[j] = 0;
                {
                    int pos = idx + j;
                    int ptr1 = prm_3_start + (pos % prm_3_length0) * prm_3_stride0;
                    for(int i1 = 0, ptr2 = ptr1; i1 < prm_3_length1; i1 += BURSTBUFFERSIZE, ptr2 += prm_3_stride1 * BURSTBUFFERSIZE)
                    {
                        int idx = ptr2;

                        int chunk_size2 = BURSTBUFFERSIZE;
                        // boundary checks
                        if ((i1 + BURSTBUFFERSIZE) > prm_3_length1) {
                            chunk_size2 = prm_3_length1 - i1;
                        }

                        for (int j2 = 0; j2 < chunk_size2; ++j2) {
                            burstbuffer2[j2] = prm_3_1[idx + j2];
                            burstbuffer3[j2] = prm_3_2_expr_1[( prm_3_2_slice_1 * (((prm_3_2_slice_2 + idx + j2) / prm_3_2_slice_3) % prm_3_2_slice_4 ))];
                        }

                        for (int j2 = 0; j2 < chunk_size2; ++j2) {
                            prm_3_sum[j] += (burstbuffer2[j2] * burstbuffer3[j2]);
                        }
                    }
                }
            }

            for (int j = 0; j < chunk_size; ++j) {
                burstbuffer4[j] = burstbuffer1[j] + prm_3_sum[j];
            }

            for (int j = 0; j < chunk_size; ++j) {
                prm_1[idx + j] = burstbuffer4[j];
            }

        }
    }
}
