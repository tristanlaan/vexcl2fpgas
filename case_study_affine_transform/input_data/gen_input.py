#!/usr/bin/python3
import sys
import numpy as np
import json


def gen_input(m, n, min=-64.0, max=64.0):
    rng = np.random.default_rng()
    A = min + rng.random((m, n)) * (max - min)
    x = min + rng.random(n) * (max - min)
    y = min + rng.random(m) * (max - min)
    data = {
        'A': {'size': list(A.shape), 'data': A.tolist()},
        'x': {'size': list(x.shape), 'data': x.tolist()},
        'y': {'size': list(y.shape), 'data': y.tolist()}
    }

    json.dump(data, sys.stdout)
    print()  # Add newline to end


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
        description="Generate matrix input for affine transform")
    parser.add_argument('-m', type=int, default=25,
                        help="Width of matrix (defaults to 25)")
    parser.add_argument('-n', type=int, default=25,
                        help="Height of matrix (defaults to 25)")
    parser.add_argument('--min', type=float, default=-64.0,
                        help="Lowest number to be drawn from distribution (defaults to -64.0)")
    parser.add_argument('--max', type=float, default=64.0,
                        help="Highest number to be drawn from distribution (defaults to 64.0)")

    args = parser.parse_args()

    gen_input(args.m, args.n, min=args.min, max=args.max)
