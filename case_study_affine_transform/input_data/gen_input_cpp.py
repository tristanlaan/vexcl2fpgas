#!/usr/bin/python3
import sys
import numpy as np
import json


def gen_input(m, n, min=-64.0, max=64.0):
    rng = np.random.default_rng()
    A = min + rng.random(m * n) * (max - min)
    x = min + rng.random(n) * (max - min)
    y = min + rng.random(m) * (max - min)

    print("#include \"extern_vars.hpp\"")
    print()
    print(f"const double A_data_loc[] = {{{', '.join(str(t) for t in A)}}};")
    print(f"const double *A_data = A_data_loc;")
    print(f"const unsigned long A_size = {len(A)};")
    print(f"const double x_data_loc[] = {{{', '.join(str(t) for t in x)}}};")
    print(f"const double *x_data = x_data_loc;")
    print(f"const unsigned long x_size = {len(x)};")
    print(f"const double y_data_loc[] = {{{', '.join(str(t) for t in y)}}};")
    print(f"const double *y_data = y_data_loc;")
    print(f"const unsigned long y_size = {len(y)};")


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
        description="Generate matrix input for affine transform")
    parser.add_argument('-m', type=int, default=25, help="Width of matrix (defaults to 25)")
    parser.add_argument('-n', type=int, default=25, help="Height of matrix (defaults to 25)")
    parser.add_argument('--min', type=float, default=-64.0,
                        help="Lowest number to be drawn from distribution (defaults to -64.0)")
    parser.add_argument('--max', type=float, default=64.0,
                        help="Highest number to be drawn from distribution (defaults to 64.0)")

    args = parser.parse_args()

    gen_input(args.m, args.n, min=args.min, max=args.max)
