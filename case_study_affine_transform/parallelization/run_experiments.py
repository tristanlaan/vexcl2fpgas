#!/usr/bin/python3
import os
import time
import random
import subprocess
from pathlib import Path
from extract_data import read_data, write_data


def compile_vexcl(root: Path, program: Path, cwd: Path) -> Path:
    vexcl = root / 'vexcl'

    subprocess.run(['g++', '-w', '-g', '-std=c++17', program,
                   '-o', cwd / 'exp.exe', f'-I{vexcl}', '-DBOOST_ALL_NO_LIB',
                   '-DBOOST_ATOMIC_DYN_LINK', '-DBOOST_FILESYSTEM_DYN_LINK',
                   '-DBOOST_SYSTEM_DYN_LINK', '-DBOOST_THREAD_DYN_LINK',
                   '-DVEXCL_BACKEND_OPENCL', '-rdynamic', '-lOpenCL',
                   '-lboost_system'], cwd=cwd, env=os.environ, check=True)

    return cwd / 'exp.exe'


def compile_host(root: Path, host: Path, max_cu: int, cwd: Path) -> Path:
    xcl2 = root / 'xcl2'
    xrt = os.environ['XILINX_XRT']
    hls = os.environ['XILINX_HLS']

    subprocess.run(['g++', '-w', '-g', '-std=c++17', host, xcl2 / 'xcl2.cpp',
                   '-o', cwd / 'exp.exe', f'-I{xcl2}', f'-I{xrt}/include/',
                   f'-I{hls}/include/', f'-L{xrt}/lib/', '-lOpenCL',
                   '-lpthread', '-lrt', '-lstdc++fs',
                   f'-DTOTAL_CU_COUNT={max_cu}'], cwd=cwd, env=os.environ,
                   check=True)

    return cwd / 'exp.exe'


def run_vexcl_kernel(executable: Path, cwd: Path, m: int, n: int) -> dict:
    powerlog = cwd / 'power.txt'
    with subprocess.Popen(['nvidia-smi', '-q', '-d', 'POWER', '-lms', '20', '-f', f'{powerlog}'], cwd=cwd, env=os.environ) as proc:
        output = subprocess.run(
            [str(executable), str(m), str(n)], cwd=cwd, env=os.environ, stdout=subprocess.DEVNULL)
        proc.terminate()
        proc.wait()
    data = read_data(cwd)
    data['returncode'] = output.returncode

    return data


def run_kernel(executable: Path, cwd: Path, compute_units: int, m: int, n: int) -> dict:
    output = subprocess.run([str(executable), str(compute_units), str(m), str(n)], cwd=cwd, env=os.environ)
    data = read_data(cwd)
    data['returncode'] = output.returncode

    return data


def gen_data(m: int, n: int, program: Path, filename: Path) -> None:
    with filename.open('w') as f:
        subprocess.run([program, '-m', str(m), '-n', str(n)],
                       stdout=f, check=True)


def run_experiments(root: Path, data_folder: Path):
    versions = [('vexcl', 0), *[('vitis', i) for i in (1, 2, 3, 4, 8, 12, 31)]]
    sizes = [(5000, 5000), (10000, 10000), (20000, 20000), (1000, 100000), (100000, 1000)]
    compute_units = [1, 2, 3, 4, 5] + list(range(6, 31, 2)) + [31]

    experiments = root / 'experiments'
    experiments.mkdir(exist_ok=True)

    runs = 10

    executables = []
    os.environ['LD_LIBRARY_PATH'] = f"{root / 'input_data'}:{os.environ['LD_LIBRARY_PATH']}"

    print("Compiling host code...")
    for version, max_cu in versions:
        if version == 'vexcl':
            cwd = experiments / 'vexcl'
            host = root / 'vexcl_experiments.cpp'
        else:
            cwd = experiments / f'{version}_{max_cu}'
            host = root / 'host_experiments.cpp'

        cwd.mkdir(exist_ok=True)

        if version == 'vexcl':
            exe = compile_vexcl(root, host, cwd)
        else:
            exe = compile_host(root, host, max_cu, cwd)
            bin = cwd / 'affinetrans.xclbin'
            log_ini = cwd / 'xrt.ini'
            if bin.is_symlink():
                bin.unlink()
            if log_ini.is_symlink():
                log_ini.unlink()
            bin.symlink_to(root / 'kernels' / f'affinetrans{max_cu}.xclbin')
            log_ini.symlink_to(root / 'hw' / 'xrt.ini')

        executables.append(exe)
    print("Compiled host code")

    for m, n in sizes:
        for run in range(runs):
            print('#' * 80)
            print(f'##### SIZE ({m}x{n}) RUN {run}')
            print('#' * 80)
            for v, exe in zip(versions, executables):
                version, max_cu = v
                print('*' * 80)
                if max_cu == 0:
                    print(f'***** VERSION "{version}"')
                    local_compute_units = [0]
                else:
                    print(f'***** VERSION "{version}"/{max_cu}')
                    local_compute_units = [cu for cu in compute_units if cu <= max_cu]
                print('*' * 80)
                random.shuffle(local_compute_units)
                for cu in local_compute_units:
                    if version != 'vexcl':
                        print('·' * 80)
                        print(f'····· {cu} CUs')
                        print('·' * 80)
                    output_file = data_folder \
                        / f'{version}-{max_cu}___{cu}___{m}x{n}___{run}.json'

                    if version == 'vexcl':
                        data = run_vexcl_kernel(exe, experiments / 'vexcl', m, n)
                    else:
                        data = run_kernel(exe, experiments / f'{version}_{max_cu}', cu, m, n)
                    write_data(data, output_file)
                    time.sleep(10)


if __name__ == '__main__':
    from datetime import datetime

    root = Path(__file__).resolve().parent
    data_folder = root / 'experiment_data'
    data_folder.mkdir(exist_ok=True)

    start = datetime.now()
    run_experiments(root, data_folder)
    end = datetime.now()

    delta = end - start
    print(f"Took {delta} hours to complete experiments")
