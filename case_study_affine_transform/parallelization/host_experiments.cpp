#include <random>
#include <iostream>
#include <sstream>
#include <vector>

#if __GNUC__ > 8 || (__GNUC__ == 8 && __GNUC_MINOR__ >= 1)
    #include <filesystem>
    namespace fs = std::filesystem;
#else
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#endif

#ifndef TOTAL_CU_COUNT
#define TOTAL_CU_COUNT 31
#endif

#include "xcl2.hpp"

#define MEM_BANKS 4

#define BLOCK_ALIGN(type) (4096 / sizeof(double))

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

/* Source: https://stackoverflow.com/a/35687575 */
template<typename Numeric, typename Generator = std::mt19937>
Numeric random(Numeric from, Numeric to)
{
    thread_local static Generator gen(std::random_device{}());

    using dist_type = typename std::conditional
    <
        std::is_integral<Numeric>::value
        , std::uniform_int_distribution<Numeric>
        , std::uniform_real_distribution<Numeric>
    >::type;

    thread_local static dist_type dist;

    return dist(gen, typename dist_type::param_type{from, to});
}

template <typename Vec>
void generate_input(size_t m, size_t n, Vec &a, Vec &x, Vec &y) {
    a.resize(m * n);
    x.resize(n);
    y.resize(m);

    for (size_t i = 0; i < n; ++i) {
        x[i] = random<double>(-64.0, 64.0);
    }

    for (size_t i = 0; i < m; ++i) {
        y[i] = random<double>(-64.0, 64.0);
    }

    for (size_t i = 0; i < m * n; ++i) {
        a[i] = random<double>(-64.0, 64.0);
    }
}

template <typename Vec>
void print_input(std::ostream& ostream, size_t m, size_t n, Vec X, Vec Y,
                 Vec A) {
    ostream << "X = [";
    for (size_t i = 0; i < n; ++i) {
        ostream << X[i];
        if (i != n - 1) {
            ostream << ", ";
        }
    }
    ostream << "]" << std::endl;

    ostream << "Y = [";
    for (size_t i = 0; i < m; ++i) {
        ostream << Y[i];
        if (i != m - 1) {
            ostream << ", ";
        }
    }
    ostream << "]" << std::endl;

    ostream << "A = [";
    for (size_t i = 0; i < m; ++i) {
        ostream << "[";
        for (size_t j = 0; j < n; ++j) {
            ostream << A[i * n + j];
            if (j != n - 1) {
                ostream << ", ";
            }
        }
        ostream << "]";
        if (i != m - 1) {
            ostream << "," << std::endl << "     ";
        }
    }
    ostream << "]" << std::endl;
}

template <typename Vec>
std::string vector_to_string(size_t m, Vec T) {
    std::ostringstream ostream;
    ostream << "[";
    for (size_t i = 0; i < m; ++i) {
        ostream << T[i];
        if (i != m - 1) {
            ostream << ", ";
        }
    }
    ostream << "]";

    return ostream.str();
}

template <typename Vec>
Vec calculate_results(size_t m, size_t n, Vec A, Vec x, Vec y) {
    Vec res = Vec(m * n);

    for (size_t i = 0; i < m; ++i) {
        double sum = 0;

        // Calculate matrix multiplication of current row
        for (size_t j = 0; j < n; ++j) {
            sum += A[i * n + j] * x[j];
        }

        // Store results
        res[i] = y[i] + sum;
    }

    return res;
}

template <typename Vec>
bool verify_results(std::ostream& ostream, size_t m, size_t n, Vec t,  Vec A, Vec x, Vec y) {
    Vec ref = calculate_results(m, n, A, x, y);
    std::string ref_str = vector_to_string(m, ref);
    std::string t_str = vector_to_string(m, t);

    for (size_t i = 0; i < m; ++i) {
        double err = abs((t[i] - ref[i]) / ref[i]);
        if (err > 0.01) {
            ostream << "ERROR: results mismatch, " << t[i] << " != " << ref[i] << " (err: " << err << ")" <<  std::endl;
            if (std::getenv("VERBOSE")) {
                ostream << "t = y + A * x = " << t_str << std::endl;
                ostream << "reference = " << ref_str << std::endl;
            }
            return false;
        }
    }

    ostream << "Results correctly verified" << std::endl;
    if (std::getenv("VERBOSE")) {
        ostream << "t = y + A * x = " << t_str << std::endl;
    }

    return true;
}

template <typename Vec>
void initialize_xilinx_mem_buffer(cl_mem_ext_ptr_t *ext, size_t id, Vec data) {
    ext->flags = id | XCL_MEM_TOPOLOGY; // Use memory bank `id`
    ext->obj = data;
    ext->param = 0;
}

int main(int argc, char **argv) {
    auto prepare = std::chrono::steady_clock::now();
    if (argc < 4) {
        std::cerr << "Too few arguments" << std::endl;
        return EXIT_FAILURE;
    }
    const size_t cu_count = std::strtoul(argv[1], NULL, 10);
    if (cu_count > TOTAL_CU_COUNT) {
        std::cerr << "More CUs than supported (" << cu_count << " > " << TOTAL_CU_COUNT << ")" << std::endl;
        return EXIT_FAILURE;
    }
    const size_t m = std::strtoul(argv[2], NULL, 10);
    const size_t n = std::strtoul(argv[3], NULL, 10);

    const size_t total_mem_count = MIN(cu_count, MEM_BANKS);

    // Initialize the OpenCL environment
    cl_int err;
    std::string binaryFile = "affinetrans.xclbin";
    std::vector<cl::Device> devices = xcl::get_xilinx_devices();
    const cl::Device device = devices.front();
    cl::Context context(device, NULL, NULL, NULL, &err);
    std::vector<unsigned char> fileBuf = xcl::read_binary_file(binaryFile);
    cl::Program::Binaries bins{{reinterpret_cast<char*>(fileBuf.data()), fileBuf.size()}};
    cl::Program program(context, devices, bins, NULL, &err);
    cl::CommandQueue q(context, device, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE | CL_QUEUE_PROFILING_ENABLE, &err);
    std::string kernelnames[cu_count];
    cl::Kernel krnls_affine_transform[cu_count];

    for (size_t i = 0; i < cu_count; ++i) {
        kernelnames[i] = "affinetrans:{affinetrans_" + std::to_string(i + 1) + "}";
        krnls_affine_transform[i] = cl::Kernel(program, kernelnames[i].c_str(), &err);
    }

    std::vector<double, aligned_allocator<double>> a, x, y;

    generate_input(m, n, a, x, y);

    std::vector<double, aligned_allocator<double>> t(m);

    std::cerr << "Read input" << std::endl;

    if (std::getenv("VERBOSE")) {
        print_input(std::cerr, m, n, x, y, a);
    }

    auto start = std::chrono::steady_clock::now();

    size_t block_size = BLOCK_ALIGN(double);
    size_t block_count = m / block_size;
    size_t block_distribution[cu_count];

    for (size_t i = 0; i < cu_count; ++i) {
        block_distribution[i] = block_count / cu_count;
        if (i < block_count % cu_count) {
            ++block_distribution[i];
        }
    }

    // Create the buffers and allocate memory
    cl::Buffer A[cu_count];
    cl_mem_ext_ptr_t A_ext[cu_count];
    cl::Buffer Y[cu_count];
    cl_mem_ext_ptr_t Y_ext[cu_count];
    cl::Buffer T[cu_count];
    cl_mem_ext_ptr_t T_ext[cu_count];
    cl::Buffer X[total_mem_count];
    cl_mem_ext_ptr_t X_ext[total_mem_count];

    // X stays the same for all CUs
    for (size_t i = 0; i < total_mem_count; ++i) {
        initialize_xilinx_mem_buffer(&X_ext[i], i, x.data());
        X[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(double) * n, &X_ext[i], &err);
    }

    size_t pos = 0;

    for (size_t i = 0; i < cu_count; ++i) {
        size_t cur_part = block_distribution[i] * block_size;

        if (i + 1 == cu_count) {
            cur_part += m % block_size;
        }

        if (cur_part == 0) {
            continue;
        }

        initialize_xilinx_mem_buffer(&A_ext[i], i % MEM_BANKS, a.data() + n * pos);
        A[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX,
                          sizeof(double) * n * cur_part, &A_ext[i], &err);
        initialize_xilinx_mem_buffer(&Y_ext[i], i % MEM_BANKS, y.data() + pos);
        Y[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX,
                          sizeof(double) * cur_part, &Y_ext[i], &err);
        initialize_xilinx_mem_buffer(&T_ext[i], i % MEM_BANKS, t.data() + pos);
        T[i] = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX,
                          sizeof(double) * cur_part, &T_ext[i], &err);

        pos += cur_part;
    }

    // Events
    cl::Event transfer[cu_count];
    cl::Event execute[cu_count];
    cl::vector<cl::Event> execute_wait[cu_count];
    cl::vector<cl::Event> transfer_wait[cu_count];

    // Execute kernels
    for (size_t i = 0; i < cu_count; ++i) {
        size_t cur_part = block_distribution[i] * block_size;

        if (i + 1 == cu_count) {
            cur_part += m % block_size;
        }

        if (cur_part == 0) {
            continue;
        }

        // Set changing kernel arguments
        krnls_affine_transform[i].setArg(0, (int) cur_part);
        krnls_affine_transform[i].setArg(1, T[i]);
        krnls_affine_transform[i].setArg(2, Y[i]);
        krnls_affine_transform[i].setArg(3, A[i]);
        krnls_affine_transform[i].setArg(4, X[i % MEM_BANKS]);
        krnls_affine_transform[i].setArg(5, 1);
        krnls_affine_transform[i].setArg(6, 0);
        krnls_affine_transform[i].setArg(7, 1);
        krnls_affine_transform[i].setArg(8, (int) n);
        krnls_affine_transform[i].setArg(9, 0);
        krnls_affine_transform[i].setArg(10, (int) m);
        krnls_affine_transform[i].setArg(11, (int) n);
        krnls_affine_transform[i].setArg(12, (int) n);
        krnls_affine_transform[i].setArg(13, 1);

        // Schedule transfer of inputs to device memory, execution of kernel, and transfer of outputs back to host memory
        if (i < MEM_BANKS) {
            q.enqueueMigrateMemObjects({Y[i], A[i], X[i]}, 0, NULL, &transfer[i]);
        } else { // X is already transferred
            q.enqueueMigrateMemObjects({Y[i], A[i]}, 0, NULL, &transfer[i]);
        }
        execute_wait[i] = {transfer[i]};

        q.enqueueTask(krnls_affine_transform[i], &execute_wait[i], &execute[i]);
        transfer_wait[i] = {execute[i]};

        q.enqueueMigrateMemObjects({T[i]}, CL_MIGRATE_MEM_OBJECT_HOST, &transfer_wait[i]);
    }

    // Wait for all scheduled operations to finish
    q.finish();

    auto end = std::chrono::steady_clock::now();

    int status = EXIT_SUCCESS;

    if (!verify_results(std::cerr, m, n, t, a, x, y)) {
        status = EXIT_FAILURE;
    }

    auto verify = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    std::ofstream time;
    time.open("time.txt", std::ios::out | std::ios::trunc);
    time << elapsed.count() << std::endl;
    elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(start - prepare);
    time << elapsed.count() << std::endl;
    elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(verify - end);
    time << elapsed.count() << std::endl;
    time.close();

    return status;
}
