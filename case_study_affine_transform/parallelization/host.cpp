#include <iostream>
#include <sstream>
#include <vector>
#include <nlohmann/json.hpp>

#if __GNUC__ > 8 || (__GNUC__ == 8 && __GNUC_MINOR__ >= 1)
    #include <filesystem>
    namespace fs = std::filesystem;
#else
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#endif

using json = nlohmann::json;

#include "xcl2.hpp"

#ifndef CU_COUNT
#define CU_COUNT 4
#endif

#define MEM_BANKS 4

#define BLOCK_ALIGN(type) (4096 / sizeof(type))

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

template <typename T>
int get_size(T array) {
    int total = 1;
    for (auto &e : array) {
        total *= e;
    }
    return total;
}

template <typename Vec>
void read_input(char *filename, Vec &a, Vec &x, Vec &y) {
    fs::path json_file(filename);
    std::error_code ec;

    if (!fs::exists(json_file, ec) || ec) {
        std::cerr << "ERROR: " << json_file << " is not a valid file" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::ifstream i(filename);
    json js;
    i >> js;

    size_t m = js.at("A").at("size")[0];
    size_t n = js.at("A").at("size")[1];

    a.resize(m * n);

    for (size_t i = 0; i < m; ++i) {
        for (size_t j = 0; j < n; ++j) {
            a[i * n + j] = js.at("A").at("data")[i][j];
        }
    }

    if (js.at("x").at("size")[0] != n) {
        std::cerr << "ERROR: " << "A and x size mismatch" << std::endl;
        exit(EXIT_FAILURE);
    }

    x.resize(n);

    for (size_t i = 0; i < n; ++i) {
        x[i] = js.at("x").at("data")[i];
    }

    if (js.at("y").at("size")[0] != m) {
        std::cerr << "ERROR: " << "A and y size mismatch" << std::endl;
        exit(EXIT_FAILURE);
    }

    y.resize(m);

    for (size_t i = 0; i < m; ++i) {
        y[i] = js.at("y").at("data")[i];
    }
}

template <typename Vec>
void generate_input(Vec &a, Vec &x, Vec &y) {
    size_t m = 10000;
    size_t n = 10000;
    a.resize(m * n);
    x.resize(n);
    y.resize(m);

    for (size_t i = 0; i < n; ++i) {
        x[i] = (double) ((n - i) % 64);
    }

    for (size_t i = 0; i < m; ++i) {
        y[i] = (double) (i % 64);
    }

    for (size_t i = 0; i < m; ++i) {
        for (size_t j = 0; j < n; ++j) {
            if (i == j) {
                a[i * n + j] = 1.0;
            } else {
                a[i * n + j] = ((i > j) ? 1.0 : -1.0) * (((i - j) % 64) / 10.0);
            }
        }
    }
}

template <typename Vec>
void print_input(std::ostream& ostream, size_t m, size_t n, Vec X, Vec Y,
                 Vec A) {
    ostream << "X = [";
    for (size_t i = 0; i < n; ++i) {
        ostream << X[i];
        if (i != n - 1) {
            ostream << ", ";
        }
    }
    ostream << "]" << std::endl;

    ostream << "Y = [";
    for (size_t i = 0; i < m; ++i) {
        ostream << Y[i];
        if (i != m - 1) {
            ostream << ", ";
        }
    }
    ostream << "]" << std::endl;

    ostream << "A = [";
    for (size_t i = 0; i < m; ++i) {
        ostream << "[";
        for (size_t j = 0; j < n; ++j) {
            ostream << A[i * n + j];
            if (j != n - 1) {
                ostream << ", ";
            }
        }
        ostream << "]";
        if (i != m - 1) {
            ostream << "," << std::endl << "     ";
        }
    }
    ostream << "]" << std::endl;
}

template <typename Vec>
std::string vector_to_string(size_t m, Vec T) {
    std::ostringstream ostream;
    ostream << "[";
    for (size_t i = 0; i < m; ++i) {
        ostream << T[i];
        if (i != m - 1) {
            ostream << ", ";
        }
    }
    ostream << "]";

    return ostream.str();
}

template <typename Vec>
Vec calculate_results(size_t m, size_t n, Vec A, Vec x, Vec y) {
    Vec res = Vec(m * n);

    for (size_t i = 0; i < m; ++i) {
        double sum = 0;

        // Calculate matrix multiplication of current row
        for (size_t j = 0; j < n; ++j) {
            sum += A[i * n + j] * x[j];
        }

        // Store results
        res[i] = y[i] + sum;
    }

    return res;
}

template <typename Vec>
bool verify_results(std::ostream& ostream, size_t m, size_t n, Vec t,  Vec A, Vec x, Vec y) {
    Vec ref = calculate_results(m, n, A, x, y);
    std::string ref_str = vector_to_string(m, ref);
    std::string t_str = vector_to_string(m, t);

    for (size_t i = 0; i < m; ++i) {
        double err = abs((t[i] - ref[i]) / ref[i]);
        if (err > 0.01) {
            ostream << "ERROR: results mismatch, " << t[i] << " != " << ref[i] << " (err: " << err << ")" <<  std::endl;
            if (std::getenv("VERBOSE")) {
                ostream << "t = y + A * x = " << t_str << std::endl;
                ostream << "reference = " << ref_str << std::endl;
            }
            return false;
        }
    }

    ostream << "Results correctly verified" << std::endl;
    if (std::getenv("VERBOSE")) {
        ostream << "t = y + A * x = " << t_str << std::endl;
    }

    return true;
}

template <typename Vec>
void initialize_xilinx_mem_buffer(cl_mem_ext_ptr_t *ext, size_t id, Vec data) {
    ext->flags = id | XCL_MEM_TOPOLOGY; // Use memory bank `id`
    ext->obj = data;
    ext->param = 0;
}

int main(int argc, char **argv) {
    size_t total_mem_count = MIN(CU_COUNT, MEM_BANKS);

    // Initialize the OpenCL environment
    cl_int err;
    std::string binaryFile = "affinetrans.xclbin";
    std::vector<cl::Device> devices = xcl::get_xilinx_devices();
    const cl::Device device = devices.front();
    cl::Context context(device, NULL, NULL, NULL, &err);
    std::vector<unsigned char> fileBuf = xcl::read_binary_file(binaryFile);
    cl::Program::Binaries bins{{reinterpret_cast<char*>(fileBuf.data()), fileBuf.size()}};
    cl::Program program(context, devices, bins, NULL, &err);
    cl::CommandQueue q(context, device, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE | CL_QUEUE_PROFILING_ENABLE, &err);
    std::string kernelnames[CU_COUNT];
    cl::Kernel krnls_affine_transform[CU_COUNT];
    for (size_t i = 0; i < CU_COUNT; ++i) {
        kernelnames[i] = "affinetrans:{affinetrans_" + std::to_string(i + 1) + "}";
        krnls_affine_transform[i] = cl::Kernel(program, kernelnames[i].c_str(), &err);
    }

    std::vector<double, aligned_allocator<double>> a, x, y;

    if (argc > 1) {
        read_input(argv[1], a, x, y);
    } else {
        generate_input(a, x, y);
    }

    size_t m = y.size();
    size_t n = x.size();

    std::vector<double, aligned_allocator<double>> t(m);

    std::cerr << "Read input" << std::endl;

    if (std::getenv("VERBOSE")) {
        print_input(std::cerr, m, n, x, y, a);
    }

    auto start = std::chrono::steady_clock::now();

    size_t block_size = BLOCK_ALIGN(double);
    size_t block_count = m / block_size;
    size_t block_distribution[CU_COUNT];

    for (size_t i = 0; i < CU_COUNT; ++i) {
        block_distribution[i] = block_count / CU_COUNT;
        if (i < block_count % CU_COUNT) {
            ++block_distribution[i];
        }
    }

    // Create the buffers and allocate memory
    cl::Buffer A[CU_COUNT];
    cl_mem_ext_ptr_t A_ext[CU_COUNT];
    cl::Buffer Y[CU_COUNT];
    cl_mem_ext_ptr_t Y_ext[CU_COUNT];
    cl::Buffer T[CU_COUNT];
    cl_mem_ext_ptr_t T_ext[CU_COUNT];
    cl::Buffer X[total_mem_count];
    cl_mem_ext_ptr_t X_ext[total_mem_count];

    // X stays the same for all CUs
    for (size_t i = 0; i < total_mem_count; ++i) {
        initialize_xilinx_mem_buffer(&X_ext[i], i, x.data());
        X[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(double) * n, &X_ext[i], &err);
    }

    size_t pos = 0;

    for (size_t i = 0; i < CU_COUNT; ++i) {
        size_t cur_part = block_distribution[i] * block_size;

        if (i + 1 == CU_COUNT) {
            cur_part += m % block_size;
        }

        if (std::getenv("VERBOSE")) {
            std::cerr << "Block size " << i << ": " << cur_part << std::endl;
        }

        if (cur_part == 0) {
            continue;
        }

        initialize_xilinx_mem_buffer(&A_ext[i], i % MEM_BANKS, a.data() + n * pos);
        A[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX,
                          sizeof(double) * n * cur_part, &A_ext[i], &err);
        initialize_xilinx_mem_buffer(&Y_ext[i], i % MEM_BANKS, y.data() + pos);
        Y[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX,
                          sizeof(double) * cur_part, &Y_ext[i], &err);
        initialize_xilinx_mem_buffer(&T_ext[i], i % MEM_BANKS, t.data() + pos);
        T[i] = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX,
                          sizeof(double) * cur_part, &T_ext[i], &err);

        pos += cur_part;
    }

    std::cerr << "Created buffers!" << std::endl;

    // Events
    cl::Event transfer[CU_COUNT];
    cl::Event execute[CU_COUNT];
    cl::vector<cl::Event> execute_wait[CU_COUNT];
    cl::vector<cl::Event> transfer_wait[CU_COUNT];

    // Execute kernels
    for (size_t i = 0; i < CU_COUNT; ++i) {
        size_t cur_part = block_distribution[i] * block_size;

        if (i + 1 == CU_COUNT) {
            cur_part += m % block_size;
        }

        if (cur_part == 0) {
            continue;
        }

        // Set changing kernel arguments
        krnls_affine_transform[i].setArg(0, (int) cur_part);
        krnls_affine_transform[i].setArg(1, T[i]);
        krnls_affine_transform[i].setArg(2, Y[i]);
        krnls_affine_transform[i].setArg(3, A[i]);
        krnls_affine_transform[i].setArg(4, X[i % MEM_BANKS]);
        krnls_affine_transform[i].setArg(5, 1);
        krnls_affine_transform[i].setArg(6, 0);
        krnls_affine_transform[i].setArg(7, 1);
        krnls_affine_transform[i].setArg(8, (int) n);
        krnls_affine_transform[i].setArg(9, 0);
        krnls_affine_transform[i].setArg(10, (int) m);
        krnls_affine_transform[i].setArg(11, (int) n);
        krnls_affine_transform[i].setArg(12, (int) n);
        krnls_affine_transform[i].setArg(13, 1);

        // Schedule transfer of inputs to device memory, execution of kernel, and transfer of outputs back to host memory
        if (i < MEM_BANKS) {
            q.enqueueMigrateMemObjects({Y[i], A[i], X[i]}, 0, NULL, &transfer[i]);
        } else { // X is already transferred
            q.enqueueMigrateMemObjects({Y[i], A[i]}, 0, NULL, &transfer[i]);
        }
        execute_wait[i] = {transfer[i]};

        q.enqueueTask(krnls_affine_transform[i], &execute_wait[i], &execute[i]);
        transfer_wait[i] = {execute[i]};

        q.enqueueMigrateMemObjects({T[i]}, CL_MIGRATE_MEM_OBJECT_HOST, &transfer_wait[i]);
    }

    // Wait for all scheduled operations to finish
    q.finish();

    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    std::ofstream time;
    time.open("time.txt", std::ios::out | std::ios::trunc);
    time << elapsed.count() << std::endl;
    time.close();

    if (!verify_results(std::cerr, m, n, t, a, x, y)) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
