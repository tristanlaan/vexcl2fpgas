#!/usr/bin/python3
from pathlib import Path
import json
import re


def _calculate_wattage(data: list) -> float:
    return (int(data[1]) / 1000) * (int(data[2]) / 1000) \
        + (int(data[3]) / 1000) * (int(data[4]) / 1000)


def _get_power_profile_data(directory: Path) -> list:
    file = next(directory.glob('power_profile_*.csv'))
    data = []

    with file.open() as f:
        for i, line in enumerate(f):
            if i < 2:
                continue
            csv_data = line.split(',')
            data.append({'timestamp': float(csv_data[0]),
                         'power': _calculate_wattage(csv_data)})

    return data

def _get_power_txt_data(file: Path) -> list:
    data = []

    with file.open() as f:
        time = 0
        for line in f:
            # Shortened version for performance
            if line.startswith("        Power D"):
                numbers = re.findall("\d+\.\d+", line)
                data.append({'timestamp': time, 'power': float(numbers[0])})
                time += 20


    return data

def _is_mode(line: str):
    if line == 'OpenCL API Calls':
        return True
    if line == 'Kernel Execution':
        return True
    if line == 'Compute Unit Utilization':
        return True
    if line == 'Data Transfer: Host to Global Memory':
        return True
    if line == 'Data Transfer: Kernels to Global Memory':
        return True

    return False

def _get_profile_summary_data(file: Path) -> dict:
    data = dict()
    mode = None
    skip_line = False
    with file.open() as f:
        for line in f:
            line = line.strip()
            if skip_line:
                skip_line = False
                continue

            if _is_mode(line):
                mode = line
                data[mode] = []
                skip_line = True
                continue

            if line == '':
                mode = None

            if not mode:
                continue

            csv_data = line.split(',')

            if mode == 'OpenCL API Calls':
                data[mode].append({
                        'name': csv_data[0],
                        'calls': int(csv_data[1]),
                        'time': float(csv_data[2])
                    })

            if mode == 'Kernel Execution':
                data[mode].append({
                        'kernel': csv_data[0],
                        'enqueues': int(csv_data[1]),
                        'time': float(csv_data[2])
                    })

            if mode == 'Compute Unit Utilization':
                data[mode].append({
                        'cu': csv_data[1],
                        'kernel': csv_data[2],
                        'time': float(csv_data[9])
                    })

            if mode == 'Data Transfer: Host to Global Memory':
                data[mode].append({
                        'type': csv_data[1],
                        'transfers': int(csv_data[2]),
                        'speed': float(csv_data[3]),
                        'utilization': float(csv_data[4]),
                        'size': float(csv_data[5]),
                        'time': float(csv_data[6])
                    })


            if mode == 'Data Transfer: Kernels to Global Memory':
                data[mode].append({
                        'interface': csv_data[3],
                        'type': csv_data[4],
                        'transfers': int(csv_data[5]),
                        'speed': float(csv_data[6]),
                        'utilization': float(csv_data[7]),
                        'size': float(csv_data[8])
                    })

    return data


def _get_time_data(file: Path) -> float:
    data = dict()
    with file.open() as f:
        for key, time in zip(['compute', 'prepare', 'validate'], f):
            data[key] = float(time)

    return data


def read_data(directory: Path) -> dict:
    data = dict()
    profile = directory / 'profile_summary.csv'
    power_txt = directory / 'power.txt'
    time = directory / 'time.txt'
    try:
        data['power'] = _get_power_profile_data(directory)
    except StopIteration:
        pass
    if profile.exists():
        data.update(_get_profile_summary_data(profile))
    if power_txt.exists():
        data['power'] = _get_power_txt_data(power_txt)
    if time.exists():
        data['time'] = _get_time_data(time)
    return data


def write_data(data: dict, file: Path):
    with file.open('w') as f:
        json.dump(data, f)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description="Extract output of Xilinx profiler")
    parser.add_argument('-d', '--directory', type=lambda p: Path(p).absolute(),
                        default=Path.cwd(), metavar='DIR',
                        help="Directory containing the .csv files, "
                             "current directory by default")
    parser.add_argument('-o', '--output', type=lambda p: Path(p).absolute(),
                        default=Path.cwd() / 'data.json', metavar='FILE',
                        help="Output JSON file, ./data.json by default")

    args = parser.parse_args()

    data = read_data(args.directory)

    write_data(data, args.output)
