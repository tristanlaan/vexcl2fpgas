#include "datatypes.hpp"

extern "C" {
    #if defined(cl_khr_fp64)
    #  pragma OPENCL EXTENSION cl_khr_fp64: enable
    #elif defined(cl_amd_fp64)
    #  pragma OPENCL EXTENSION cl_amd_fp64: enable
    #endif

    void affinetrans
            (
            int n,
            lfix_t *prm_1,
            const fix_t *prm_2,
            const fix_t *prm_3_1,
            const fix_t *prm_3_2_expr_1,
            int prm_3_2_slice_1,
            int prm_3_2_slice_2,
            int prm_3_2_slice_3,
            int prm_3_2_slice_4,
            int prm_3_start,
            int prm_3_length0,
            int prm_3_stride0,
            int prm_3_length1,
            int prm_3_stride1
            )
    {
    #pragma HLS INTERFACE m_axi port=prm_1 bundle=aximm1
    #pragma HLS INTERFACE m_axi port=prm_2 bundle=aximm2
    #pragma HLS INTERFACE m_axi port=prm_3_1 bundle=aximm2
    #pragma HLS INTERFACE m_axi port=prm_3_2_expr_1 bundle=aximm3

        for(int idx = 0; idx < n; ++idx)
        {
            lfix_t prm_3_sum = 0;
            {
                int pos = idx;
                int ptr1 = prm_3_start + (pos % prm_3_length0) * prm_3_stride0;
                for(int i1 = 0, ptr2 = ptr1; i1 < prm_3_length1; ++i1, ptr2 += prm_3_stride1)
                {
                    int idx = ptr2;
                    prm_3_sum = prm_3_sum + (prm_3_1[idx] * prm_3_2_expr_1[( prm_3_2_slice_1 * (((prm_3_2_slice_2 + idx) / prm_3_2_slice_3) % prm_3_2_slice_4 ))]);
                }
            }
            prm_1[idx] = (prm_2[idx] + prm_3_sum);
        }
    }
}
