#!/usr/bin/python3
import os
import subprocess
from pathlib import Path
from extract_data import read_data, write_data


def compile_input_data(root: Path, input_data: Path, cwd: Path):
    extern_vars = root / 'input_data' / 'extern_vars.hpp'

    subprocess.run(['g++', '-shared', '-g', '-std=c++17', input_data, '-o',
                   cwd / 'input_data.so', f'-I{extern_vars.parent}'], cwd=cwd,
                   env=os.environ, check=True)

    return cwd / 'input_data.so'


def compile_vexcl(root: Path, program: Path, input_data: Path, cwd: Path) -> Path:
    extern_vars = root / 'input_data' / 'extern_vars.hpp'
    vexcl = cwd / 'vexcl'

    subprocess.run(['g++', '-w', '-g', '-std=c++17', program,
                   '-o', cwd / 'exp.exe', f'-L{input_data.parent}',
                   f'-l:{input_data.name}', f'-I{extern_vars.parent}',
                   f'-I{vexcl}', '-DBOOST_ALL_NO_LIB',
                   '-DBOOST_ATOMIC_DYN_LINK', '-DBOOST_FILESYSTEM_DYN_LINK',
                   '-DBOOST_SYSTEM_DYN_LINK', '-DBOOST_THREAD_DYN_LINK',
                   '-DVEXCL_BACKEND_OPENCL', '-rdynamic', '-lOpenCL',
                   '-lboost_system'], cwd=cwd, env=os.environ, check=True)

    return cwd / 'exp.exe'


def compile_host(root: Path, host: Path, input_data: Path, cwd: Path) -> Path:
    extern_vars = root / 'input_data' / 'extern_vars.hpp'
    xcl2 = host.parent / 'xcl2'
    xrt = os.environ['XILINX_XRT']
    hls = os.environ['XILINX_HLS']

    subprocess.run(['g++', '-w', '-g', '-std=c++17', host, xcl2 / 'xcl2.cpp',
                   '-o', cwd / 'exp.exe', f'-L{input_data.parent}',
                   f'-l:{input_data.name}', f'-I{xcl2}',
                   f'-I{extern_vars.parent}', f'-I{xrt}/include/',
                   f'-I{hls}/include/', f'-L{xrt}/lib/', '-lOpenCL',
                   '-lpthread', '-lrt', '-lstdc++fs'], cwd=cwd, env=os.environ,
                   check=True)

    return cwd / 'exp.exe'


def run_vexcl_kernel(executable: Path, cwd: Path) -> dict:
    powerlog = cwd / 'power.txt'
    with subprocess.Popen(['nvidia-smi', '-q', '-d', 'POWER', '-lms', '-f', f'{powerlog}'], cwd=cwd, env=os.environ) as proc:
        output = subprocess.run(
            [str(executable)], cwd=cwd, env=os.environ, stdout=subprocess.DEVNULL)
        proc.terminate()
        proc.wait()
    data = read_data(cwd)
    data['returncode'] = output.returncode

    return data


def run_kernel(executable: Path, cwd: Path) -> dict:
    output = subprocess.run([str(executable)], cwd=cwd, env=os.environ)
    data = read_data(cwd)
    data['returncode'] = output.returncode

    return data


def gen_data(m: int, n: int, program: Path, filename: Path) -> None:
    with filename.open('w') as f:
        subprocess.run([program, '-m', str(m), '-n', str(n)],
                       stdout=f, check=True)


def run_experiments(root: Path, data_folder: Path):
    versions = [root / 'vexcl', root / 'vitis', root / 'vitis_opt1',
                root / 'vitis_opt2', root / 'vitis_opt3',
                root / 'vitis_opt4', root / 'vitis_opt5']

    sizes = [(32, 32), (256, 256), (1024, 1024), (5000, 5000),
             (10000, 10000), (1000000, 2), (2, 1000000)]

    runs = 10

    data_generator = root / 'input_data' / 'gen_input_cpp.py'

    cleanup = set()
    executables = []
    os.environ['LD_LIBRARY_PATH'] = f"{root / 'input_data'}:{os.environ['LD_LIBRARY_PATH']}"

    try:
        print("Compiling host code...")
        for version in versions:
            # Generate small file so shared object exists
            input_data = root / 'input_data' / 'input_1x1.cpp'
            gen_data(1, 1, data_generator, input_data)
            cleanup.add(input_data)
            compiled_input_data = compile_input_data(root, input_data,
                                                        input_data.parent)

            if version.name == 'vexcl':
                cwd = version
                host = version / 'affinetrans_experiments.cpp'
            else:
                cwd = version / 'hw'
                host = version / 'host_experiments.cpp'

            if version.name == 'vexcl':
                exe = compile_vexcl(root, host, compiled_input_data,
                                    cwd)
            else:
                exe = compile_host(
                    root, host, compiled_input_data, cwd)

            cleanup.add(exe)
            executables.append(exe)
        print("Compiled host code")

        for m, n in sizes:
            input_data = root / 'input_data' / f'input_{m}x{n}.cpp'
            for run in range(runs):
                print('#' * 80)
                print(f'##### SIZE ({m}x{n}) RUN {run}')
                print('#' * 80)

                print("Generating input data...")
                gen_data(m, n, data_generator, input_data)
                cleanup.add(input_data)
                compile_input_data(root, input_data, input_data.parent)
                print("Generated input data")

                for version, exe in zip(versions, executables):
                    print('*' * 80)
                    print(f'***** VERSION "{version.name}"')
                    print('*' * 80)

                    output_file = data_folder \
                        / f'{version.name}___{m}x{n}___{run}.json'

                    if version.name == 'vexcl':
                        data = run_vexcl_kernel(exe, version)
                    else:
                        data = run_kernel(exe, version / 'hw')
                    write_data(data, output_file)

            input_data.unlink()
    finally:
        for file in cleanup:
            try:
                file.unlink()  # Remove file
            except FileNotFoundError:
                pass


if __name__ == '__main__':
    from datetime import datetime

    root = Path(__file__).resolve().parent
    data_folder = root / 'experiment_data'
    data_folder.mkdir(exist_ok=True)

    start = datetime.now()
    run_experiments(root, data_folder)
    end = datetime.now()

    delta = end - start
    print(f"Took {delta} hours to complete experiments")
