#if defined(cl_khr_fp64)
#  pragma OPENCL EXTENSION cl_khr_fp64: enable
#elif defined(cl_amd_fp64)
#  pragma OPENCL EXTENSION cl_amd_fp64: enable
#endif

kernel void spmat_kernel
(
  ulong n,
  double scale,
  ulong ell_w,
  ulong ell_pitch,
  global const ulong * ell_col,
  global const double * ell_val,
  global const ulong * csr_row,
  global const ulong * csr_col,
  global const double * csr_val,
  global const double * in,
  global double * out
)
{
  for(ulong i = get_global_id(0); i < n; i += get_global_size(0))
  {
    double sum = 0;
    for(size_t j = 0; j < ell_w; ++j)
    {
      ulong c = ell_col[i + j * ell_pitch];
      if (c != (ulong)(-1))
      {
        sum += ell_val[i + j * ell_pitch] * in[c];
      }
    }
    if (csr_row)
    {
      for(size_t j = csr_row[i], e = csr_row[i + 1]; j < e; ++j)
      {
        sum += csr_val[j] * in[csr_col[j]];
      }
    }
    out[i] = scale * sum;
  }
}
