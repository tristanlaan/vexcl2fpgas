#!/usr/bin/env python3
from pathlib import Path

def generate_config_file(file: Path, cu_count_phi: int, cu_count_spmat: int):
    with file.open('w') as f:
        print("platform=xilinx_u250_gen3x16_xdma_3_1_202020_1", file=f)
        print("debug=1", file=f)
        print("save-temps=1", file=f)

        print(file=f)
        print("[connectivity]", file=f)
        print(f"nk=phi:{cu_count_phi}:{'.'.join([f'phi_{i}' for i in range(1, cu_count_phi + 1)])}", file=f)
        print(f"nk=spmat:{cu_count_spmat}:{'.'.join([f'spmat_{i}' for i in range(1, cu_count_spmat + 1)])}", file=f)


        for i in range(cu_count_phi):
            print(file=f)
            print(f"sp=phi_{i + 1}.prm_1:DDR[3]", file=f)
            print(f"sp=phi_{i + 1}.prm_1:DDR[{i % 3}]", file=f)
            print(f"sp=phi_{i + 1}.prm_tag_1_1:DDR[{i % 3}]", file=f)
            print(f"sp=phi_{i + 1}.prm_tag_2_1:DDR[{i % 3}]", file=f)
            print(f"sp=phi_{i + 1}.prm_tag_3_1:DDR[{i % 3}]", file=f)

        for i in range(cu_count_spmat):
            print(file=f)
            print(f"sp=spmat_{i + 1}.in:DDR[3]", file=f)
            print(f"sp=spmat_{i + 1}.ell_col:DDR[{i % 3}]", file=f)
            print(f"sp=spmat_{i + 1}.ell_val:DDR[{i % 3}]", file=f)
            print(f"sp=spmat_{i + 1}.csr_row:DDR[{i % 3}]", file=f)
            print(f"sp=spmat_{i + 1}.csr_col:DDR[{i % 3}]", file=f)
            print(f"sp=spmat_{i + 1}.csr_row:DDR[{i % 3}]", file=f)
            print(f"sp=spmat_{i + 1}.out:DDR[{i % 3}]", file=f)

        print(file=f)
        print("[profile]", file=f)
        print("data=all:all:all", file=f)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description="Generate affine transformation configuration file")
    parser.add_argument('-f', '--file', type=lambda f: Path(f).absolute(), metavar='FILE',
                        required=True, help="File to store configuration")
    parser.add_argument('cu_count_phi', type=int, metavar='CU_COUNT_PHI',
                        help="Amount of compute units to use for phi kernel")
    parser.add_argument('cu_count_spmat', type=int, metavar='CU_COUNT_SPMAT',
                    help="Amount of compute units to use for spmat kernel")

    args = parser.parse_args()

    generate_config_file(args.file, args.cu_count_phi, args.cu_count_spmat)
