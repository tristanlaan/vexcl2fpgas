#!/usr/bin/python3
from pathlib import Path
import json
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

VERSION_NAMES = {
    'vexcl': r'VexCL* (GPU)',
    'vitis': r'Vitis phi: {} (FPGA)',
}

DIMENSION_NAMES = {
    (100000, 100000, 0.01): r'$10^5 \times 10^5$, density: $0.01$',
}

API_NAMES = {
    'clFinish': 'clFinish',
    'clCreateBuffer': 'clCreateBuffer',
    'clCreateProgramWithBinary': 'clCreateProgramWithBinary',
    'Other': 'Other'
}

KERNEL_NAMES = {
    'phi': 'phi',
    'spmat': 'spmat'
}

NOTE_SIZE = 5

CHECK_TIME = False


def get_performance_data(directory: Path, version: str, cu_spmat: int, cu_phi: int, m: int, n: int, d: float) -> dict:
    walltime = []
    kerneltime_spmat = []
    kerneltime_phi = []
    power = []
    api_calls = dict()
    for api in API_NAMES.keys():
        api_calls[api] = []

    for file in directory.glob(f"{version}-{cu_spmat}-{cu_phi}___{m}x{n}_{d}___*.json"):
        with file.open() as f:
            data = json.load(f)
            if version == 'vexcl':
                if isinstance(data['time'], float):
                    power.append(np.mean([x for x in data['power'] if x > 1]))
                elif CHECK_TIME:
                    power.append(np.mean([x['power'] for x in data['power'] if x['time'] / 1000 > data['time']['prepare'] and x['time'] / 1000 < data['time']['prepare'] + data['time']['compute']]))
                else:
                    power.append(np.mean([x['power'] for x in data['power'] if x['power'] > 1]))
            else:
                power.append(np.mean([x['power'] for x in data['power']]))
            if isinstance(data['time'], float):
                walltime.append(data['time'])
            else:
                walltime.append(data['time']['compute'])
            if version != 'vexcl':
                kerneltime_spmat.append(np.max([x['time'] / 1000 for x in data['Kernel Execution'] if x['kernel'] == 'spmat']))
                kerneltime_phi.append(np.max([x['time'] / 1000 for x in data['Kernel Execution'] if x['kernel'] == 'phi']))
                other_time = 0
                for call in data['OpenCL API Calls']:
                    if call['name'] in API_NAMES:
                        api_calls[call['name']].append(call['time'] / 1000)
                    else:
                        other_time += call['time']
                api_calls['Other'] = other_time / 1000

    data = {
        'walltime': {'mean': np.mean(walltime), 'std': np.std(walltime)},
        'power': {'mean': np.mean(power), 'std': np.std(power)}
    }

    if version != 'vexcl':
        data['kerneltime'] = {
            'spmat': {'mean': np.mean(kerneltime_spmat), 'std': np.std(kerneltime_spmat)},
            'phi': {'mean': np.mean(kerneltime_phi), 'std': np.std(kerneltime_phi)}
            }
        data['apitime'] = dict()
        for api in API_NAMES.keys():
            data['apitime'][api] = {'mean': np.mean(api_calls[api]), 'std': np.std(api_calls[api])}

    return data


def get_all_performance_data(directory: Path, versions: list, compute_units_phi: list, compute_units_spmat: list, dimensions: list) -> dict:
    data = dict()
    for version in versions:
        if version == 'vexcl':
            data[version] = dict()
            for m, n, d in dimensions:
                data[version][(m, n, d)] = get_performance_data(directory, version, 0, 0, m, n, d)
        else:
            for cu_phi in compute_units_phi:
                data[(version, cu_phi)] = dict()
                for cu_spmat in compute_units_spmat:
                    data[(version, cu_phi)][cu_spmat] = dict()
                    for m, n, d in dimensions:
                        data[(version, cu_phi)][cu_spmat][(m, n, d)] = get_performance_data(directory, version, cu_phi, cu_spmat, m, n, d)

    return data


def calc_energy(pmean, pstd, tmean, tstd):
    pvar = pstd ** 2
    tmean = tmean / 3600
    tvar = (tstd / 3600) ** 2
    mean = pmean * tmean
    std = np.sqrt(pvar * tvar + pmean ** 2 * tvar + tmean ** 2 * pvar)
    return mean, std


def calc_energies(pmeans, pstds, tmeans, tstds):
    pvars = np.array([std ** 2 for std in pstds])
    tmeans = np.array([mean / 3600 for mean in tmeans])
    tvars = np.array([(std / 3600) ** 2 for std in tstds])
    pmeans = np.array(pmeans)
    means = pmeans * tmeans
    stds = np.sqrt(pvars * tvars + pmeans ** 2 * tvars + tmeans ** 2 * pvars)
    return means, stds


def plot_power_plot(versions: list, compute_units_phi: list, compute_units_spmat: list, dimensions: list, data: dict, directory: Path):
    width = 8.3 * (2/3)
    height = 5.8 * (2/3)

    f = plt.figure(frameon=False, figsize=(width, height))
    spec = gridspec.GridSpec(ncols=1, nrows=1, figure=f)
    subplots = [f.add_subplot(place) for place in [spec[0, 0]]]

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']

    for subplot, dimension in zip(subplots, dimensions):
        vexcl_x = np.array([compute_units_spmat[0] - 1, compute_units_spmat[-1] + 1])
        vexcl_err = data['vexcl'][dimension]['power']['std']
        vexcl_mean = np.array([data['vexcl'][dimension]['power']['mean'] for _ in range(2)])
        subplot.plot(vexcl_x, vexcl_mean, color=colors[0], linestyle='dashed', label=VERSION_NAMES['vexcl'])
        subplot.fill_between(vexcl_x, vexcl_mean - vexcl_err, vexcl_mean + vexcl_err, alpha=0.25, color=colors[0], edgecolor="none")

        for i, cu_phi in enumerate(compute_units_phi):
            vitis_means = [data[('vitis', cu_phi)][cu][dimension]['power']['mean'] for cu in compute_units_spmat]
            vitis_errs = [data[('vitis', cu_phi)][cu][dimension]['power']['std'] for cu in compute_units_spmat]
            subplot.errorbar(compute_units_spmat, vitis_means, yerr=vitis_errs, color=colors[i + 1], marker='.', markersize=4, ecolor='black', elinewidth=0.75, capsize=1, capthick=0.5, linestyle='dashed', linewidth=0.5, label=VERSION_NAMES['vitis'].format(cu_phi))

        subplot.set_ylabel('Power (W)')
        subplot.set_ylim((0, 55))
        subplot.set_xlim((compute_units_spmat[0] - 1, compute_units_spmat[-1] + 1))
        subplot.set_xlabel('Compute units spmat')
        subplot.legend()
        subplot.set_title(f'Matrix dimensions: {DIMENSION_NAMES[dimension]}')

    f.text(0.675, 0.01, r"\noindent *VexCL only shown as reference, since GPU doesn't use compute units.", fontsize=NOTE_SIZE)

    f.suptitle(r'\large Average power usage averaged over 10 runs')
    f.savefig(directory / "power_plot.pdf")


def plot_walltime_plot(versions: list, compute_units_phi: list, compute_units_spmat: list, dimensions: list, data: dict, directory: Path):
    width = 8.3 * (2/3)
    height = 5.8 * (2/3)

    f = plt.figure(frameon=False, figsize=(width, height))
    spec = gridspec.GridSpec(ncols=1, nrows=1, figure=f)
    subplots = [f.add_subplot(place) for place in [spec[0, 0]]]

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']

    for subplot, dimension in zip(subplots, dimensions):
        vexcl_x = np.array([compute_units_spmat[0] - 1, compute_units_spmat[-1] + 1])
        vexcl_err = data['vexcl'][dimension]['walltime']['std']
        vexcl_mean = np.array([data['vexcl'][dimension]['walltime']['mean'] for _ in range(2)])
        subplot.plot(vexcl_x, vexcl_mean, color=colors[0], linestyle='dashed', label=VERSION_NAMES['vexcl'])
        subplot.fill_between(vexcl_x, vexcl_mean - vexcl_err, vexcl_mean + vexcl_err, alpha=0.25, color=colors[0], edgecolor="none")

        for i, cu_phi in enumerate(compute_units_phi):
            vitis_means = [data[('vitis', cu_phi)][cu][dimension]['walltime']['mean'] for cu in compute_units_spmat]
            vitis_errs = [data[('vitis', cu_phi)][cu][dimension]['walltime']['std'] for cu in compute_units_spmat]
            subplot.errorbar(compute_units_spmat, vitis_means, yerr=vitis_errs, color=colors[i + 1], marker='.', markersize=4, ecolor='black', elinewidth=0.75, capsize=1, capthick=0.5, linestyle='dashed', linewidth=0.5, label=VERSION_NAMES['vitis'].format(cu_phi))


        subplot.set_ylabel('Execution time (s)')
        subplot.set_ylim(bottom=0)
        subplot.set_xlim((compute_units_spmat[0] - 1, compute_units_spmat[-1] + 1))
        subplot.set_xlabel('Compute units spmat')
        subplot.legend()
        subplot.set_title(f'Matrix dimensions: {DIMENSION_NAMES[dimension]}')

    f.text(0.675, 0.01, r"\noindent *VexCL only shown as reference, since GPU doesn't use compute units.", fontsize=NOTE_SIZE)

    f.suptitle(r'\large Execution time averaged over 10 runs')
    f.savefig(directory / "walltime_plot.pdf")


def plot_kerneltime_phi_plot(versions: list, compute_units_phi: list, compute_units_spmat, dimensions: list, data: dict, directory: Path):
    width = 8.3 * (2/3)
    height = 5.8 * (2/3)

    f = plt.figure(frameon=False, figsize=(width, height))
    spec = gridspec.GridSpec(ncols=1, nrows=1, figure=f)
    subplots = [f.add_subplot(place) for place in [spec[0, 0]]]

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']

    for subplot, dimension in zip(subplots, dimensions):
        for i, cu_phi in enumerate(compute_units_phi):
            vitis_means = [data[('vitis', cu_phi)][cu][dimension]['kerneltime']['phi']['mean'] for cu in compute_units_spmat]
            vitis_errs = [data[('vitis', cu_phi)][cu][dimension]['kerneltime']['phi']['std'] for cu in compute_units_spmat]
            subplot.errorbar(compute_units_spmat, vitis_means, yerr=vitis_errs, color=colors[i + 1], marker='.', markersize=4, ecolor='black', elinewidth=0.75, capsize=1, capthick=0.5, linestyle='dashed', linewidth=0.5, label=VERSION_NAMES['vitis'].format(cu_phi))

        subplot.set_ylabel('Execution time (s)')
        subplot.set_ylim(bottom=0)
        subplot.set_xlim((compute_units_spmat[0] - 1, compute_units_spmat[-1] + 1))
        subplot.set_xlabel('Compute units spmat')
        subplot.legend()
        subplot.set_title(f'Matrix dimensions: {DIMENSION_NAMES[dimension]}')

    f.suptitle(r'\large Longest phi kernel time averaged over 10 runs')
    f.savefig(directory / "kerneltime_phi_plot.pdf")


def plot_kerneltime_spmat_plot(versions: list, compute_units_phi: list, compute_units_spmat, dimensions: list, data: dict, directory: Path):
    width = 8.3 * (2/3)
    height = 5.8 * (2/3)

    f = plt.figure(frameon=False, figsize=(width, height))
    spec = gridspec.GridSpec(ncols=1, nrows=1, figure=f)
    subplots = [f.add_subplot(place) for place in [spec[0, 0]]]

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']

    for subplot, dimension in zip(subplots, dimensions):
        for i, cu_phi in enumerate(compute_units_phi):
            vitis_means = [data[('vitis', cu_phi)][cu][dimension]['kerneltime']['spmat']['mean'] for cu in compute_units_spmat]
            vitis_errs = [data[('vitis', cu_phi)][cu][dimension]['kerneltime']['spmat']['std'] for cu in compute_units_spmat]
            subplot.errorbar(compute_units_spmat, vitis_means, yerr=vitis_errs, color=colors[i + 1], marker='.', markersize=4, ecolor='black', elinewidth=0.75, capsize=1, capthick=0.5, linestyle='dashed', linewidth=0.5, label=VERSION_NAMES['vitis'].format(cu_phi))

        subplot.set_ylabel('Execution time (s)')
        subplot.set_ylim(bottom=0)
        subplot.set_xlim((compute_units_spmat[0] - 1, compute_units_spmat[-1] + 1))
        subplot.set_xlabel('Compute units spmat')
        subplot.legend()
        subplot.set_title(f'Matrix dimensions: {DIMENSION_NAMES[dimension]}')

    f.suptitle(r'\large Longest spmat kernel time averaged over 10 runs')
    f.savefig(directory / "kerneltime_spmat_plot.pdf")


def plot_energy_plot(versions: list, compute_units_phi: list, compute_units_spmat: list, dimensions: list, data: dict, directory: Path):
    width = 8.3 * (2/3)
    height = 5.8 * (2/3)

    f = plt.figure(frameon=False, figsize=(width, height))
    spec = gridspec.GridSpec(ncols=1, nrows=1, figure=f)
    subplots = [f.add_subplot(place) for place in [spec[0, 0]]]

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']

    for subplot, dimension in zip(subplots, dimensions):
        vexcl_x = np.array([compute_units_spmat[0] - 1, compute_units_spmat[-1] + 1])
        vexcl_perr = data['vexcl'][dimension]['power']['std']
        vexcl_pmean = data['vexcl'][dimension]['power']['mean']
        vexcl_terr = data['vexcl'][dimension]['walltime']['std']
        vexcl_tmean = data['vexcl'][dimension]['walltime']['mean']
        vexcl_mean, vexcl_err = calc_energy(vexcl_pmean, vexcl_perr, vexcl_tmean, vexcl_terr)
        vexcl_mean = np.array([vexcl_mean, vexcl_mean])

        subplot.plot(vexcl_x, vexcl_mean, color=colors[0], linestyle='dashed', label=VERSION_NAMES['vexcl'])
        subplot.fill_between(vexcl_x, vexcl_mean - vexcl_err, vexcl_mean + vexcl_err, alpha=0.25, color=colors[0], edgecolor="none")

        for i, cu_phi in enumerate(compute_units_phi):
            vitis_pmeans = [data[('vitis', cu_phi)][cu][dimension]['power']['mean'] for cu in compute_units_spmat]
            vitis_perrs = [data[('vitis', cu_phi)][cu][dimension]['power']['std'] for cu in compute_units_spmat]
            vitis_tmeans = [data[('vitis', cu_phi)][cu][dimension]['walltime']['mean'] for cu in compute_units_spmat]
            vitis_terrs = [data[('vitis', cu_phi)][cu][dimension]['walltime']['std'] for cu in compute_units_spmat]
            vitis_means, vitis_errs = calc_energies(vitis_pmeans, vitis_perrs, vitis_tmeans, vitis_terrs)
            subplot.errorbar(compute_units_spmat, vitis_means, yerr=vitis_errs, color=colors[i + 1], marker='.', markersize=4, ecolor='black', elinewidth=0.75, capsize=1, capthick=0.5, linestyle='dashed', linewidth=0.5, label=VERSION_NAMES['vitis'].format(cu_phi))

        subplot.set_ylabel('Energy usage (Wh)')
        subplot.set_ylim(bottom=0)
        subplot.set_xlim((compute_units_spmat[0] - 1, compute_units_spmat[-1] + 1))
        subplot.set_xlabel('Compute units spmat')
        subplot.legend()
        subplot.set_title(f'Matrix dimensions: {DIMENSION_NAMES[dimension]}')

    f.text(0.675, 0.01, r"\noindent *VexCL only shown as reference, since GPU doesn't use compute units.", fontsize=NOTE_SIZE)

    f.suptitle(r'\large Approximate energy usage averaged over 10 runs')
    f.savefig(directory / "energy_plot.pdf")

def plot_api_plot(compute_units_phi: list, compute_units_spmat: list, dimensions: list, data: dict, directory: Path):
    width = 8.3 * (2/3)
    height = 5.8 * (2/3)

    f = plt.figure(frameon=False, figsize=(width, height))
    spec = gridspec.GridSpec(ncols=2, nrows=1, figure=f)
    subplots = [f.add_subplot(place) for place in (spec[0, 0], spec[0, 1])]

    dimension = dimensions[0]

    for subplot, cu_phi in zip(subplots, compute_units_phi):
        for api in API_NAMES.keys():
            vitis_means = [data[('vitis', cu_phi)][cu][dimension]['apitime'][api]['mean'] for cu in compute_units_spmat]
            vitis_errs = [data[('vitis', cu_phi)][cu][dimension]['apitime'][api]['std'] for cu in compute_units_spmat]
            subplot.errorbar(compute_units_spmat, vitis_means, yerr=vitis_errs, marker='.', markersize=4, ecolor='black', elinewidth=0.75, capsize=1, capthick=0.5, linestyle='dashed', linewidth=0.5, label=API_NAMES[api])

        subplot.set_ylabel('Execution time (s)')
        subplot.set_ylim(bottom=0)
        subplot.set_xlim((compute_units_spmat[0] - 1, compute_units_spmat[-1] + 1))
        subplot.set_xlabel('Compute units spmat')
        subplot.legend()
        subplot.set_title(f'Compute units phi: {cu_phi}')

    f.suptitle(r'\large Total call time OpenCL API calls averaged over 10 runs')
    f.savefig(directory / "apitime_plot.pdf")


if __name__ == '__main__':
    from matplotlib import rc
    from matplotlib import rcParams
    font_size = 7
    # font_size = 4
    # rc('font', **{'family': 'serif', 'serif': ['Computer Modern'], 'size': font_size})
    rc('font', **{'size': font_size})
    NOTE_SIZE = 6
    rc('text', usetex=True)
    rc('hatch', linewidth=0.1)
    rc('text.latex', preamble=r'\renewcommand{\familydefault}{\sfdefault} \usepackage{sansmath} \sansmath \usepackage[math]{blindtext}')
    rcParams.update({'figure.autolayout': True})

    root = Path(__file__).resolve().parent
    output = root / 'experiment_data'
    input = output
    versions = ['vexcl', 'vitis']
    dimensions = [(100000, 100000, 0.01)]
    compute_units_phi = [1, 3]
    compute_units_spmat = [1, 3, 6]

    data = get_all_performance_data(input, versions, compute_units_phi, compute_units_spmat, dimensions)
    output.mkdir(exist_ok=True)

    plot_power_plot(versions, compute_units_phi, compute_units_spmat, dimensions, data, output)
    plot_walltime_plot(versions, compute_units_phi, compute_units_spmat, dimensions, data, output)
    plot_kerneltime_phi_plot(versions, compute_units_phi, compute_units_spmat, dimensions, data, output)
    plot_kerneltime_spmat_plot(versions, compute_units_phi, compute_units_spmat, dimensions, data, output)
    plot_energy_plot(versions, compute_units_phi, compute_units_spmat, dimensions, data, output)
    plot_api_plot(compute_units_phi, compute_units_spmat, dimensions, data, output)
