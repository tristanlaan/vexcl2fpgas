#pragma once

/*
The MIT License

Copyright (c) 2012-2018 Denis Demidov <dennis.demidov@gmail.com>
Copyright (c) 2021 Tristan Laan <tristan.laan@student.uva.nl>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <vector>
#include <set>
#include <unordered_map>
#include <string>
#include <memory>
#include <algorithm>
#include <iostream>
#include <type_traits>

inline size_t alignup(size_t n, size_t m = 16U) {
    return (n + m - 1) / m * m;
}

template <typename val_t, typename idx_t = size_t, typename Vec = std::vector<val_t>, typename IVec = std::vector<idx_t>>
struct sparse_matrix {
    size_t n, pitch;

    struct {
        struct {
            size_t width;
            IVec   col;
            Vec    val;
        } ell;

        struct {
            size_t nnz;
            IVec   row;
            IVec   col;
            Vec    val;
        } csr;
    } mat;

    sparse_matrix(
            const idx_t *row_begin, const idx_t *row_end,
            const idx_t *col, const val_t *val,
            size_t col_begin, size_t col_end
            )
        : n(row_end - row_begin), pitch( alignup(n, 16U) )
    {
        /* 1. Get optimal ELL widths. */
        {
            // Speed of ELL relative to CSR (e.g. 2.0 -> ELL is twice as fast):
            const double ell_vs_csr = 3.0;

            // Find maximum widths:
            mat.ell.width = 0;
            for(auto row = row_begin; row != row_end; ++row) {
                size_t w = 0;
                for(idx_t j = row[0]; j < row[1]; ++j) {
                    ++w;
                }

                mat.ell.width = std::max(mat.ell.width, w);
            }

            // Build histograms for width distribution.
            std::vector<size_t> hist(mat.ell.width + 1, 0);

            for(auto row = row_begin; row != row_end; ++row) {
                size_t w = 0;
                for(idx_t j = row[0]; j < row[1]; ++j) {
                    ++w;
                }

                ++hist[w];
            }

            auto optimal_width = [&](size_t max_width, const std::vector<size_t> &hist) -> size_t {
                for(size_t i = 0, rows = n; i < max_width; ++i) {
                    rows -= hist[i]; // Number of rows wider than i.
                    if (ell_vs_csr * rows < n) return i;
                }

                return max_width;
            };

            mat.ell.width = optimal_width(mat.ell.width, hist);
        }

        /* 2. Count nonzeros in CSR parts of the matrix. */
        mat.csr.nnz = 0;

        for(auto row = row_begin; row != row_end; ++row) {
            size_t w = 0;
            for(idx_t j = row[0]; j < row[1]; ++j) {
                ++w;
            }

            if (w > mat.ell.width) mat.csr.nnz += w - mat.ell.width;
        }

        // Prepare ELL and COO formats.
        const idx_t not_a_column = static_cast<idx_t>(-1);

        if (mat.ell.width) {
            mat.ell.col = IVec(pitch * mat.ell.width, not_a_column);
            mat.ell.val = Vec(pitch * mat.ell.width, val_t());
        }

        if (mat.csr.nnz) {
            mat.csr.row = IVec();
            mat.csr.col = IVec();
            mat.csr.val = Vec();
        }

        mat.csr.row.reserve(n + 1);
        mat.csr.col.reserve(mat.csr.nnz);
        mat.csr.val.reserve(mat.csr.nnz);

        mat.csr.row.push_back(0);

        size_t k = 0;
        for(auto row = row_begin; row != row_end; ++row, ++k) {
            size_t cnt = 0;
            for(idx_t j = row[0]; j < row[1]; ++j) {
                if (cnt < mat.ell.width) {
                    mat.ell.col[k + pitch * cnt] = static_cast<idx_t>(col[j] - col_begin);
                    mat.ell.val[k + pitch * cnt] = val[j];
                    ++cnt;
                } else {
                    mat.csr.col.push_back(static_cast<idx_t>(col[j] - col_begin));
                    mat.csr.val.push_back(val[j]);
                }
            }

            mat.csr.row.push_back(static_cast<idx_t>(mat.csr.col.size()));
        }

        if (mat.csr.col.empty()) {
            mat.csr.col.push_back(0);
            mat.csr.val.push_back((val_t) 0);
        }
    }
};

/// Sparse matrix in hybrid ELL-CSR format.
template <typename val_t, typename idx_t = size_t, typename Vec = std::vector<val_t>, typename IVec = std::vector<idx_t>>
class SpMat {
    public:
        /// Empty constructor.
        SpMat() : nrows(0), ncols(0), nnz(0) {
            mtx = {};
        }

        /// Constructor.
        /**
         * Constructs hybrid ELL-CSR representation of the \f$n \times m\f$matrix. Input
         * matrix is in CSR format, Output matrix utilizes ELL format.
         */
        SpMat(size_t m, size_t n, const idx_t *row, const idx_t *col, const val_t *val, size_t *block_distribution, size_t cu_count, size_t block_size)
            : nrows(m), ncols(n), nnz(row[m])
        {
            empty_mtx();

            size_t pos = 0;
            for (size_t i = 0; i < cu_count; ++i) {
                size_t cur_part = block_distribution[i] * block_size;

                if (i + 1 == cu_count) {
                    cur_part += m % block_size;
                }

                if (cur_part == 0) {
                    mtx.push_back(NULL);
                } else {
                    mtx.push_back(new sparse_matrix<val_t, idx_t, Vec, IVec>(row + pos, row + pos + cur_part, col, val, 0, nnz));
                }

                pos += cur_part;
            }
        }

        ~SpMat() {
            empty_mtx();
        }

        /// Number of rows.
        size_t rows() const { return nrows; }
        /// Number of columns.
        size_t cols() const { return ncols; }
        /// Number of non-zero entries.
        size_t nonzeros() const { return nnz; }
        // Hybrid ELL-CSR format matrix
        std::vector<struct sparse_matrix<val_t, idx_t, Vec, IVec> *>ell_mat() const { return mtx; }

    private:
        size_t nrows;
        size_t ncols;
        size_t nnz;

        void empty_mtx() {
            for (auto & m : mtx) {
                delete m;
            }

            mtx = {};
        }

        std::vector<struct sparse_matrix<val_t, idx_t, Vec, IVec> *> mtx;
};
