#include <iostream>
#include <iomanip>
#include <vector>
#include <random>
#include <algorithm>
#include <string>
#include <cmath>

#include "extern_vars.hpp"

#include "xcl2.hpp"
#include "spmat.hpp"

#ifndef TOTAL_PHI_CU_COUNT
#define TOTAL_PHI_CU_COUNT 4
#endif

#ifndef TOTAL_SPMAT_CU_COUNT
#define TOTAL_SPMAT_CU_COUNT 4
#endif

#define MEM_BANKS 4

#define BLOCK_ALIGN(type) (4096 / sizeof(double))

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

typedef struct {
    size_t phi_cu_count;
    size_t spmat_cu_count;
    size_t m;
    size_t n;
    double density;
} config;

typedef std::pair<size_t, size_t> coordinate;

cl::Platform get_platform(const std::string &vendor_name) {
    cl_int err;
    std::vector<cl::Platform> platforms;
    OCL_CHECK(err, err = cl::Platform::get(&platforms));

    for (auto &platform : platforms) {
        std::string platformName = platform.getInfo<CL_PLATFORM_NAME>();
        if (platformName == vendor_name)
        {
            std::cerr << "INFO: Found " << vendor_name << " Platform" << std::endl;
            return platform;
        }
    }

    std::cerr << "ERROR: Failed to find " << vendor_name << " platform" << std::endl;
    exit(EXIT_FAILURE);
}

std::vector<cl::Device> get_devices(const std::string &vendor_name) {
    cl_int err;
    cl::Platform platform = get_platform(vendor_name);
    //Getting ACCELERATOR Devices and selecting 1st such device
    std::vector<cl::Device> devices;
    OCL_CHECK(err,
              err = platform.getDevices(CL_DEVICE_TYPE_GPU, &devices));
    return devices;
}

std::vector<cl::Device> get_nvidia_devices() { return get_devices("NVIDIA CUDA"); }

std::string load_program_source(const char* filename, const char* preamble)
{
    // locals
    FILE* pFileStream = NULL;
    const char* cFilename = filename;
    const char* cPreamble = preamble;
    size_t szFinalLength;
    size_t szSourceLength;

    // open the OpenCL source code file
    #ifdef _WIN32   // Windows version
        if(fopen_s(&pFileStream, cFilename, "rb") != 0)
        {
            return NULL;
        }
    #else           // Linux version
        pFileStream = fopen(cFilename, "rb");
        if(pFileStream == 0)
        {
            return NULL;
        }
    #endif

    size_t szPreambleLength = strlen(cPreamble);

    // get the length of the source code
    fseek(pFileStream, 0, SEEK_END);
    szSourceLength = ftell(pFileStream);
    fseek(pFileStream, 0, SEEK_SET);

    // allocate a buffer for the source code string and read it in
    char* cSourceString = (char *)malloc(szSourceLength + szPreambleLength + 1);
    memcpy(cSourceString, cPreamble, szPreambleLength);
    if (fread((cSourceString) + szPreambleLength, szSourceLength, 1, pFileStream) != 1)
    {
        fclose(pFileStream);
        free(cSourceString);
        return 0;
    }

    // close the file and return the total length of the combined (preamble + source) string
    fclose(pFileStream);
    szFinalLength = szSourceLength + szPreambleLength;
    cSourceString[szFinalLength] = '\0';

    return std::string(cSourceString, cSourceString + szFinalLength);
}

template <typename Vec>
std::string vector_to_string(const Vec &T, const int width=5, const int precision=3) {
    std::ostringstream ostream;
    ostream << "[";
    for (auto iter = T.begin(); iter != T.end(); ++iter) {
        ostream << std::setw(width) << std::setprecision(precision) << *iter;
        if (iter + 1 != T.end()) {
            ostream << ", ";
        }
    }
    ostream << "]";

    return ostream.str();
}

template <typename IVec, typename Vec>
void print_input(std::ostream& ostream, const IVec &A_row, const IVec &A_col, const Vec &A_data, const Vec &u1, const Vec &u2, const Vec &u3) {
    ostream << "A_row  = " << vector_to_string(A_row) << std::endl;
    ostream << "A_col  = " << vector_to_string(A_col) << std::endl;
    ostream << "A_data = " << vector_to_string(A_data) << std::endl;
    ostream << "u1     = " << vector_to_string(u1) << std::endl;
    ostream << "u2     = " << vector_to_string(u2) << std::endl;
    ostream << "u3     = " << vector_to_string(u3) << std::endl;
}

double phi(double U1, double U2, double U3) {
    auto u1 = U1;
    auto u2 = U2;
    auto u3 = U3;

    return (u1 - u2 + std::log(u3) * std::log(u3) * std::sin(u1)) / (u1 * u2);
}

template <typename IVec, typename Vec>
Vec calculate_results(const config &cfg, const IVec &A_row, const IVec &A_col, const Vec &A_data, const Vec &u1, const Vec &u2, const Vec &u3) {
    Vec res = Vec(cfg.m);

    Vec tmp = Vec(cfg.n);

    for (size_t i = 0; i < cfg.n; ++i) {
        tmp[i] = phi(u1[i], u2[i], u3[i]);
    }

    for (size_t i = 0; i < cfg.m; ++i) {
        double sum = 0;

        // Calculate matrix multiplication of current row
        for (size_t j = A_row[i]; j < A_row[i + 1]; ++j) {
            sum += A_data[j] * tmp[A_col[j]];
        }

        // Store results
        res[i] = sum;
    }

    return res;
}

template <typename IVec, typename Vec>
bool verify_results(std::ostream& ostream, const config &cfg, const Vec &t, const IVec &A_row, const IVec &A_col, const Vec &A_data, const Vec &u1, const Vec &u2, const Vec &u3) {
    Vec ref = calculate_results(cfg, A_row, A_col, A_data, u1, u2, u3);
    std::string ref_str = vector_to_string(ref);
    std::string t_str = vector_to_string(t);

    for (size_t i = 0; i < cfg.m; ++i) {
        double err = abs((t[i] - ref[i]) / ref[i]);
        if (err > 0.01) {
            ostream << "ERROR: results mismatch, " << t[i] << " != " << ref[i] << " (err: " << err << ")" <<  std::endl;
            if (std::getenv("VERBOSE")) {
                ostream << "t = A * phi(u1, u2, u3) = " << t_str << std::endl;
                ostream << "reference               = " << ref_str << std::endl;
            }
            return false;
        }
    }

    ostream << "Results correctly verified" << std::endl;
    if (std::getenv("VERBOSE")) {
        ostream << "t = A * phi(u1, u2, u3) = " << t_str << std::endl;
    }

    return true;
}

void initialize_xilinx_mem_buffer(cl_mem_ext_ptr_t *ext, size_t id, void *data) {
    ext->flags = id | XCL_MEM_TOPOLOGY; // Use memory bank `id`
    ext->obj = data;
    ext->param = 0;
}

int main(int argc, char **argv) {
    config cfg;
    cfg.m = m;
    cfg.n = n;
    if (argc < 3) {
        std::cerr << "Too few arguments" << std::endl;
        return EXIT_FAILURE;
    }

    cfg.phi_cu_count = std::strtoul(argv[1], NULL, 10);
    if (cfg.phi_cu_count > TOTAL_PHI_CU_COUNT) {
        std::cerr << "More CUs for phi than supported (" << cfg.phi_cu_count << " > " << TOTAL_PHI_CU_COUNT << ")" << std::endl;
        return EXIT_FAILURE;
    }
    cfg.spmat_cu_count = std::strtoul(argv[2], NULL, 10);
    if (cfg.spmat_cu_count > TOTAL_SPMAT_CU_COUNT) {
        std::cerr << "More CUs for spmat than supported (" << cfg.spmat_cu_count << " > " << TOTAL_SPMAT_CU_COUNT << ")" << std::endl;
        return EXIT_FAILURE;
    }

    // Initialize the OpenCL environment
    cl_int err;
    std::vector<cl::Device> gpu_devices = get_nvidia_devices();
    const cl::Device gpu_device = gpu_devices.front();
    std::cerr << "INFO: Using GPU: " << gpu_device.getInfo<CL_DEVICE_NAME>() << std::endl;

    std::string binaryFile = "spmat.xclbin";
    std::vector<cl::Device> devices = xcl::get_xilinx_devices();
    const cl::Device device = devices.front();
    std::vector<cl::Device> context_devices = {gpu_device, device};

    cl::Context gpu_context(gpu_device, NULL, NULL, NULL, &err);
    std::string phi_source = load_program_source("phi.cl", "");
    cl::Program gpu_program(gpu_context, phi_source);
    err = gpu_program.build(gpu_devices);
    if (err == CL_BUILD_PROGRAM_FAILURE) {
        std::cerr << "phi build failed! Log:" << std::endl << gpu_program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(gpu_device) << std::endl;
        return EXIT_FAILURE;
    }
    cl::Kernel krnl_phi(gpu_program, "phi_kernel");
    cl::CommandQueue qgpu(gpu_context, gpu_device, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE | CL_QUEUE_PROFILING_ENABLE, &err);

    cl::Context context(device, NULL, NULL, NULL, &err);
    std::vector<unsigned char> fileBuf = xcl::read_binary_file(binaryFile);
    cl::Program::Binaries bins{{reinterpret_cast<char*>(fileBuf.data()), fileBuf.size()}};
    cl::Program program(context, devices, bins, NULL, &err);
    cl::CommandQueue q(context, device, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE | CL_QUEUE_PROFILING_ENABLE, &err);

    std::string kernelnames_spmat[cfg.spmat_cu_count];
    cl::Kernel krnls_spmat[cfg.spmat_cu_count];
    for (size_t i = 0; i < cfg.spmat_cu_count; ++i) {
        kernelnames_spmat[i] = "spmat:{spmat_" + std::to_string(i + 1) + "}";
        krnls_spmat[i] = cl::Kernel(program, kernelnames_spmat[i].c_str(), &err);
    }

    std::vector<size_t, aligned_allocator<size_t>> A_row(A_row_data, A_row_data + m + 1), A_col(A_col_data, A_col_data + A_row_data[m]);
    std::vector<double, aligned_allocator<double>> A_data(A_data_data, A_data_data + A_row_data[m]), u1(u1_data, u1_data + n), u2(u2_data, u2_data + n), u3(u3_data, u3_data + n), temp, t;

    if (std::getenv("VERBOSE")) {
        std::cerr << std::endl;
        std::cerr << "-------------------" << std::endl;
        std::cerr << "-  CONFIGURATION  -" << std::endl;
        std::cerr << "-------------------" << std::endl;
        std::cerr << "m:        " << cfg.m << std::endl;
        std::cerr << "n:        " << cfg.n << std::endl;
        std::cerr << "function: phi(u1, u2, u3) = (u1 - u2 + log(u3) × sin(u1)) / (u1 × u2)" << std::endl;
        std::cerr << std::endl;
    }

    temp.resize(cfg.n);
    t.resize(cfg.m);
    std::cerr << "Read input" << std::endl;

    if (std::getenv("VERBOSE")) {
        std::cerr << std::endl;
        std::cerr << "-------------------" << std::endl;
        std::cerr << "-      INPUT      -" << std::endl;
        std::cerr << "-------------------" << std::endl;
        print_input(std::cerr, A_row, A_col, A_data, u1, u2, u3);
        std::cerr << std::endl;
    }

    auto start = std::chrono::steady_clock::now();

    /********************************************************
     *                                                      *
     *                         PHI                          *
     *                                                      *
     ********************************************************/

    cl::Buffer tmp_res(gpu_context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, sizeof(double) * temp.size(), temp.data(), &err);
    cl::Buffer U1(gpu_context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(double) * u1.size(), u1.data(), &err);
    cl::Buffer U2(gpu_context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(double) * u2.size(), u2.data(), &err);
    cl::Buffer U3(gpu_context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(double) * u3.size(), u3.data(), &err);

    // Events
    cl::Event transfer_phi;
    cl::Event execute_phi;
    cl::vector<cl::Event> execute_wait_phi;
    cl::vector<cl::Event> transfer_wait_phi;

    // Set kernel arguments
    krnl_phi.setArg(0, cfg.n);
    krnl_phi.setArg(1, tmp_res);
    krnl_phi.setArg(2, U1);
    krnl_phi.setArg(3, U2);
    krnl_phi.setArg(4, U3);

    // Schedule transfer of inputs to device memory, execution of kernel, and transfer of outputs back to host memory
    qgpu.enqueueMigrateMemObjects({U1, U2, U3}, 0, NULL, &transfer_phi);
    execute_wait_phi = {transfer_phi};
    qgpu.enqueueTask(krnl_phi, &execute_wait_phi, &execute_phi);
    transfer_wait_phi = {execute_phi};
    qgpu.enqueueMigrateMemObjects({tmp_res}, CL_MIGRATE_MEM_OBJECT_HOST, &transfer_wait_phi);


    /********************************************************
     *                                                      *
     *                        SPMAT                         *
     *                                                      *
     ********************************************************/
    size_t block_size = BLOCK_ALIGN(double);
    size_t block_count_spmat = cfg.m / block_size;
    size_t block_distribution_spmat[cfg.spmat_cu_count];

    for (size_t i = 0; i < cfg.spmat_cu_count; ++i) {
        block_distribution_spmat[i] = block_count_spmat / cfg.spmat_cu_count;
        if (i < block_count_spmat % cfg.spmat_cu_count) {
            ++block_distribution_spmat[i];
        }
    }


    SpMat A(cfg.m, cfg.n, A_row.data(), A_col.data(), A_data.data(), block_distribution_spmat, cfg.spmat_cu_count, block_size);

    auto A_ell = A.ell_mat();

    cl::Buffer tmp_res_fpga;
    cl_mem_ext_ptr_t tmp_res_ext;
    initialize_xilinx_mem_buffer(&tmp_res_ext, 3, temp.data());
    tmp_res_fpga = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(double) * temp.size(), &tmp_res_ext, &err);

    cl::Buffer A_ell_col[cfg.spmat_cu_count];
    cl_mem_ext_ptr_t A_ell_col_ext[cfg.spmat_cu_count];
    cl::Buffer A_ell_val[cfg.spmat_cu_count];
    cl_mem_ext_ptr_t A_ell_val_ext[cfg.spmat_cu_count];
    cl::Buffer A_csr_row[cfg.spmat_cu_count];
    cl_mem_ext_ptr_t A_csr_row_ext[cfg.spmat_cu_count];
    cl::Buffer A_csr_col[cfg.spmat_cu_count];
    cl_mem_ext_ptr_t A_csr_col_ext[cfg.spmat_cu_count];
    cl::Buffer A_csr_val[cfg.spmat_cu_count];
    cl_mem_ext_ptr_t A_csr_val_ext[cfg.spmat_cu_count];
    cl::Buffer T[cfg.spmat_cu_count];
    cl_mem_ext_ptr_t T_ext[cfg.spmat_cu_count];

    size_t pos_spmat = 0;
    for (size_t i = 0; i < cfg.spmat_cu_count; ++i) {
        size_t cur_part = block_distribution_spmat[i] * block_size;

        if (i + 1 == cfg.spmat_cu_count) {
            cur_part += cfg.m % block_size;
        }

        if (cur_part == 0) {
            continue;
        }

        initialize_xilinx_mem_buffer(&A_ell_col_ext[i], i % (MEM_BANKS - 1), A_ell[i]->mat.ell.col.data());
        A_ell_col[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(size_t) * A_ell[i]->mat.ell.col.size(), &A_ell_col_ext[i], &err);
        initialize_xilinx_mem_buffer(&A_ell_val_ext[i], i % (MEM_BANKS - 1), A_ell[i]->mat.ell.val.data());
        A_ell_val[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(double) * A_ell[i]->mat.ell.val.size(), &A_ell_val_ext[i], &err);
        initialize_xilinx_mem_buffer(&A_csr_row_ext[i], i % (MEM_BANKS - 1), A_ell[i]->mat.csr.row.data());
        A_csr_row[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(size_t) * A_ell[i]->mat.csr.row.size(), &A_csr_row_ext[i], &err);
        initialize_xilinx_mem_buffer(&A_csr_col_ext[i], i % (MEM_BANKS - 1), A_ell[i]->mat.csr.col.data());
        A_csr_col[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(size_t) * A_ell[i]->mat.csr.col.size(), &A_csr_col_ext[i], &err);
        initialize_xilinx_mem_buffer(&A_csr_val_ext[i], i % (MEM_BANKS - 1), A_ell[i]->mat.csr.val.data());
        A_csr_val[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(double) * A_ell[i]->mat.csr.val.size(), &A_csr_val_ext[i], &err);

        initialize_xilinx_mem_buffer(&T_ext[i], i % (MEM_BANKS - 1), t.data() + pos_spmat);
        T[i] = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(double) * cur_part, &T_ext[i], &err);

        pos_spmat += cur_part;
    }

    // Events
    cl::Event transfer_tmp_spmat;
    cl::Event transfer_spmat[cfg.spmat_cu_count];
    cl::Event execute_spmat[cfg.spmat_cu_count];
    cl::vector<cl::Event> execute_wait_spmat[cfg.spmat_cu_count];
    cl::vector<cl::Event> transfer_wait_spmat[cfg.spmat_cu_count];

    // Wait on GPU
    qgpu.finish();
    q.enqueueMigrateMemObjects({tmp_res_fpga}, 0, NULL, &transfer_tmp_spmat);
    cl::vector<cl::Event> transfer_tmp_wait_spmat = {transfer_tmp_spmat};

    for (size_t i = 0; i < cfg.spmat_cu_count; ++i) {
        size_t cur_part = block_distribution_spmat[i] * block_size;

        if (i + 1 == cfg.spmat_cu_count) {
            cur_part += cfg.m % block_size;
        }

        if (cur_part == 0) {
            continue;
        }

        krnls_spmat[i].setArg(0, cur_part);
        krnls_spmat[i].setArg(1, 1.0);
        krnls_spmat[i].setArg(2, A_ell[i]->mat.ell.width);
        krnls_spmat[i].setArg(3, A_ell[i]->pitch);
        krnls_spmat[i].setArg(4, A_ell_col[i]);
        krnls_spmat[i].setArg(5, A_ell_val[i]);
        krnls_spmat[i].setArg(6, A_csr_row[i]);
        krnls_spmat[i].setArg(7, A_csr_col[i]);
        krnls_spmat[i].setArg(8, A_csr_val[i]);
        krnls_spmat[i].setArg(9, tmp_res_fpga);
        krnls_spmat[i].setArg(10, T[i]);

        q.enqueueMigrateMemObjects({A_ell_col[i], A_ell_val[i], A_csr_row[i], A_csr_col[i], A_csr_val[i]}, 0, &transfer_tmp_wait_spmat, &transfer_spmat[i]);
        execute_wait_spmat[i] = {transfer_spmat[i]};
        q.enqueueTask(krnls_spmat[i], &execute_wait_spmat[i], &execute_spmat[i]);
        transfer_wait_spmat[i] = {execute_spmat[i]};
        q.enqueueMigrateMemObjects({T[i]}, CL_MIGRATE_MEM_OBJECT_HOST, &transfer_wait_spmat[i]);
    }

    // Wait for all scheduled operations to finish
    q.finish();

    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    std::ofstream time;
    time.open("time.txt", std::ios::out | std::ios::trunc);
    time << elapsed.count() << std::endl;
    time.close();

    if (!verify_results(std::cerr, cfg, t, A_row, A_col, A_data, u1, u2, u3)) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
