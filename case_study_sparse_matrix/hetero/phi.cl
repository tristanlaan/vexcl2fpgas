#if defined(cl_khr_fp64)
#  pragma OPENCL EXTENSION cl_khr_fp64: enable
#elif defined(cl_amd_fp64)
#  pragma OPENCL EXTENSION cl_amd_fp64: enable
#endif

kernel void phi_kernel
(
  ulong n,
  global double * prm_1,
  global double * prm_tag_1_1,
  global double * prm_tag_2_1,
  global double * prm_tag_3_1
)
{
  for(ulong idx = get_global_id(0); idx < n; idx += get_global_size(0))
  {
    prm_1[idx] = ( ( ( prm_tag_1_1[idx] - prm_tag_2_1[idx] ) + ( ( log( prm_tag_3_1[idx] ) * log( prm_tag_3_1[idx] ) ) * sin( prm_tag_1_1[idx] ) ) ) / ( prm_tag_1_1[idx] * prm_tag_2_1[idx] ) );
  }
}
