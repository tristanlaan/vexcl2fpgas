#!/usr/bin/python3
import os
import time
import subprocess
from pathlib import Path
from typing import Tuple
from extract_data import read_data, write_data


def compile_input_data(root: Path, input_data: Path, cwd: Path):
    extern_vars = root / 'input_data' / 'extern_vars.hpp'

    subprocess.run(['g++', '-shared', '-g', '-std=c++17', input_data, '-o',
                   cwd / 'input_data.so', f'-I{extern_vars.parent}'], cwd=cwd,
                   env=os.environ, check=True)

    return cwd / 'input_data.so'


def compile_vexcl(root: Path, program: Path, input_data: Path, cwd: Path) -> Path:
    extern_vars = root / 'input_data' / 'extern_vars.hpp'
    vexcl = root / 'vexcl'

    subprocess.run(['g++', '-w', '-g', '-std=c++17', program,
                   '-o', cwd / 'exp.exe', f'-L{input_data.parent}',
                   f'-l:{input_data.name}', f'-I{extern_vars.parent}',
                   f'-I{vexcl}', '-DBOOST_ALL_NO_LIB',
                   '-DBOOST_ATOMIC_DYN_LINK', '-DBOOST_FILESYSTEM_DYN_LINK',
                   '-DBOOST_SYSTEM_DYN_LINK', '-DBOOST_THREAD_DYN_LINK',
                   '-DVEXCL_BACKEND_OPENCL', '-rdynamic', '-lOpenCL',
                   '-lboost_system'], cwd=cwd, env=os.environ, check=True)

    return cwd / 'exp.exe'


def compile_host(root: Path, host: Path, input_data: Path,
                 max_cu: Tuple[int, int], cwd: Path) -> Path:
    extern_vars = root / 'input_data' / 'extern_vars.hpp'
    xcl2 = host.parent / 'xcl2'
    xrt = os.environ['XILINX_XRT']
    hls = os.environ['XILINX_HLS']

    subprocess.run(['g++', '-w', '-g', '-std=c++17', host, xcl2 / 'xcl2.cpp',
                   '-o', cwd / 'exp.exe', f'-L{input_data.parent}',
                   f'-l:{input_data.name}', f'-I{xcl2}', f'-I{host.parent}',
                   f'-I{extern_vars.parent}', f'-I{xrt}/include/',
                   f'-I{hls}/include/', f'-L{xrt}/lib/', '-lOpenCL',
                   '-lpthread', '-lrt', '-lstdc++fs',
                   f'-DTOTAL_PHI_CU_COUNT={max_cu[0]}',
                   f'-DTOTAL_SPMAT_CU_COUNT={max_cu[1]}'],
                   cwd=cwd, env=os.environ, check=True)

    return cwd / 'exp.exe'


def run_vexcl_kernel(executable: Path, cwd: Path) -> dict:
    powerlog = cwd / 'power.txt'
    with subprocess.Popen(['nvidia-smi', '-q', '-d', 'POWER', '-lms', '-f', f'{powerlog}'], cwd=cwd, env=os.environ) as proc:
        output = subprocess.run(
            [str(executable)], cwd=cwd, env=os.environ, stdout=subprocess.DEVNULL)
        proc.terminate()
        proc.wait()
    data = read_data(cwd)
    data['returncode'] = output.returncode

    return data


def run_kernel(executable: Path, compute_units: Tuple[int, int], cwd: Path) -> dict:
    output = subprocess.run([str(executable), str(compute_units[0]), str(compute_units[1])], cwd=cwd, env=os.environ)
    data = read_data(cwd)
    data['returncode'] = output.returncode

    return data


def gen_data(m: int, n: int, d: float, program: Path, filename: Path) -> None:
    with filename.open('w') as f:
        subprocess.run([program, '-m', str(m), '-n', str(n), '-d', str(d)],
                       stdout=f, check=True)


def run_experiments(root: Path, data_folder: Path):
    versions = [('vexcl', (0, 0)), *[('vitis', (i, j)) for j in (1, 3, 6) for i in (1, 3)]]

    sizes = [(100000, 100000, 0.01)]

    experiments = root / 'experiments'
    experiments.mkdir(exist_ok=True)

    runs = 10

    data_generator = root / 'input_data' / 'gen_input.py'

    cleanup = set()
    executables = []
    os.environ['LD_LIBRARY_PATH'] = f"{root / 'input_data'}:{os.environ['LD_LIBRARY_PATH']}"

    try:
        print("Compiling host code...")
        # Generate small file so shared object exists
        input_data = root / 'input_data' / 'input_1x1_0.01.cpp'
        gen_data(1, 1, 0.01, data_generator, input_data)
        cleanup.add(input_data)
        compiled_input_data = compile_input_data(root, input_data,
                                                    input_data.parent)
        for version, max_cu in versions:
            if version == 'vexcl':
                cwd = experiments / 'vexcl'
                host = root / 'vexcl_experiments.cpp'
            else:
                cwd = experiments / f'{version}_{max_cu[0]}_{max_cu[1]}'
                host = root / 'host_experiments.cpp'

            cwd.mkdir(exist_ok=True)

            if version == 'vexcl':
                exe = compile_vexcl(root, host, compiled_input_data,
                                    cwd)
            else:
                exe = compile_host(
                    root, host, compiled_input_data, max_cu, cwd)
                bin = cwd / 'spmat.xclbin'
                log_ini = cwd / 'xrt.ini'
                if bin.is_symlink():
                    bin.unlink()
                if log_ini.is_symlink():
                    log_ini.unlink()
                bin.symlink_to(root / 'kernels' / f'spmat{max_cu[0]}_{max_cu[1]}.xclbin')
                log_ini.symlink_to(root / 'hw' / 'xrt.ini')

            cleanup.add(exe)
            executables.append(exe)
        print("Compiled host code")

        for m, n, d in sizes:
            input_data = root / 'input_data' / f'input_{m}x{n}_{d}.cpp'
            for run in range(runs):
                print('#' * 80)
                print(f'##### SIZE ({m}x{n}_{d}) RUN {run}')
                print('#' * 80)

                print("Generating input data...")
                gen_data(m, n, d, data_generator, input_data)
                cleanup.add(input_data)
                compile_input_data(root, input_data, input_data.parent)
                print("Generated input data")

                for v, exe in zip(versions, executables):
                    version, max_cu = v
                    print('*' * 80)
                    if max_cu[0] == 0:
                        print(f'***** VERSION "{version}"')
                        local_compute_units = [(0, 0)]
                    else:
                        print(f'***** VERSION "{version}"/{max_cu[0]}:{max_cu[1]}')
                        local_compute_units = [max_cu]
                    print('*' * 80)

                    for cu in local_compute_units:
                        if version != 'vexcl':
                            print('·' * 80)
                            print(f'····· {cu[0]}:{cu[1]} CUs')
                            print('·' * 80)

                        output_file = data_folder \
                            / f'{version}-{max_cu[0]}-{max_cu[1]}___{m}x{n}_{d}___{run}.json'

                        if version == 'vexcl':
                            data = run_vexcl_kernel(exe, experiments / 'vexcl')
                        else:
                            data = run_kernel(exe, cu, experiments / f'{version}_{max_cu[0]}_{max_cu[1]}')
                        write_data(data, output_file)
                        time.sleep(10)

            input_data.unlink()
    finally:
        for file in cleanup:
            # try:
            #     file.unlink()  # Remove file
            # except FileNotFoundError:
            #     pass
            pass


if __name__ == '__main__':
    from datetime import datetime

    root = Path(__file__).resolve().parent
    data_folder = root / 'experiment_data'
    data_folder.mkdir(exist_ok=True)

    start = datetime.now()
    run_experiments(root, data_folder)
    end = datetime.now()

    delta = end - start
    print(f"Took {delta} hours to complete experiments")
