#include <iostream>
#include <iomanip>
#include <vector>
#include <random>
#include <algorithm>
#include <string>
#include <cmath>

#include "xcl2.hpp"
#include "spmat.hpp"

#ifndef TOTAL_PHI_CU_COUNT
#define TOTAL_PHI_CU_COUNT 4
#endif

#ifndef TOTAL_SPMAT_CU_COUNT
#define TOTAL_SPMAT_CU_COUNT 4
#endif

#define MEM_BANKS 4

#define BLOCK_ALIGN(type) (4096 / sizeof(double))

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

typedef struct {
    size_t phi_cu_count;
    size_t spmat_cu_count;
    size_t m;
    size_t n;
    double density;
    std::default_random_engine *gen;
} config;

typedef std::pair<size_t, size_t> coordinate;

template <typename Vec>
std::string vector_to_string(const Vec &T, const int width=5, const int precision=3) {
    std::ostringstream ostream;
    ostream << "[";
    for (auto iter = T.begin(); iter != T.end(); ++iter) {
        ostream << std::setw(width) << std::setprecision(precision) << *iter;
        if (iter + 1 != T.end()) {
            ostream << ", ";
        }
    }
    ostream << "]";

    return ostream.str();
}

std::vector<coordinate> generate_coords(const config &cfg) {
    size_t elements = cfg.density * cfg.m * cfg.n;

    std::uniform_int_distribution<size_t> coor_dist(0, cfg.m * cfg.n - 1);
    std::set<size_t> generated_numbers;
    auto coords = std::vector<coordinate>();

    while (generated_numbers.size() < elements) {
        size_t c = coor_dist(*cfg.gen);

        if (generated_numbers.find(c) == generated_numbers.end()) {
            generated_numbers.insert(c);
        }
    }

    for (const size_t &c : generated_numbers) {
        coords.push_back(coordinate(c / cfg.n, c % cfg.n));
    }

    return coords;
}

template <typename Vec>
std::vector<size_t> get_coord_row(const Vec& coords, const size_t row) {
    std::vector<size_t> cols;
    struct compare {
        size_t row;
        compare(size_t const &k): row(k) {}

        bool operator()(coordinate const &i) {
            return (row == std::get<0>(i));
        }
    };

    auto iter = coords.begin();

    while ((iter = std::find_if(iter, coords.end(), compare(row))) != coords.end()) {
        cols.push_back(std::get<1>(*iter));
        ++iter;
    }

    std::sort(cols.begin(), cols.end());

    return cols;
}

template <typename Vec, typename Dist, typename Gen>
void generate_vector(Vec &v, size_t n, Dist &dist, Gen &gen) {
    v.resize(n);
    for (size_t i = 0; i < n; ++i) {
        v[i] = dist(gen);
    }
}

template <typename IVec, typename Vec>
void generate_input(const config &cfg, IVec &A_row, IVec &A_col, Vec &A_data, Vec &u1, Vec &u2, Vec &u3) {
    std::uniform_real_distribution data_dist(.01, 64.0);

    size_t elements = cfg.density * cfg.m * cfg.n;
    A_row.resize(cfg.m + 1);
    A_col.resize(elements);
    A_data.resize(elements);
    std::vector<coordinate> coords = generate_coords(cfg);

    generate_vector(u1, cfg.n, data_dist, *cfg.gen);
    generate_vector(u2, cfg.n, data_dist, *cfg.gen);
    generate_vector(u3, cfg.n, data_dist, *cfg.gen);

    for (size_t i = 0; i < elements; ++i) {
        A_data[i] = data_dist((*cfg.gen));
    }


    A_row[0] = 0;
    auto iter = A_col.begin();

    for (size_t i = 0; i < cfg.m; ++i) {
        auto cols = get_coord_row(coords, i);

        if (cols.size() > 0) {
            std::copy(cols.begin(), cols.end(), iter);
            iter += cols.size();
        }

        A_row[i + 1] = A_row[i] + cols.size();
    }
}

template <typename IVec, typename Vec>
void print_input(std::ostream& ostream, const IVec &A_row, const IVec &A_col, const Vec &A_data, const Vec &u1, const Vec &u2, const Vec &u3) {
    ostream << "A_row  = " << vector_to_string(A_row) << std::endl;
    ostream << "A_col  = " << vector_to_string(A_col) << std::endl;
    ostream << "A_data = " << vector_to_string(A_data) << std::endl;
    ostream << "u1     = " << vector_to_string(u1) << std::endl;
    ostream << "u2     = " << vector_to_string(u2) << std::endl;
    ostream << "u3     = " << vector_to_string(u3) << std::endl;
}

double phi(double U1, double U2, double U3) {
    auto u1 = U1;
    auto u2 = U2;
    auto u3 = U3;

    return (u1 - u2 + std::log(u3) * std::log(u3) * std::sin(u1)) / (u1 * u2);
}

template <typename IVec, typename Vec>
Vec calculate_results(const config &cfg, const IVec &A_row, const IVec &A_col, const Vec &A_data, const Vec &u1, const Vec &u2, const Vec &u3) {
    Vec res = Vec(cfg.m);

    Vec tmp = Vec(cfg.n);

    for (size_t i = 0; i < cfg.n; ++i) {
        tmp[i] = phi(u1[i], u2[i], u3[i]);
    }

    for (size_t i = 0; i < cfg.m; ++i) {
        double sum = 0;

        // Calculate matrix multiplication of current row
        for (size_t j = A_row[i]; j < A_row[i + 1]; ++j) {
            sum += A_data[j] * tmp[A_col[j]];
        }

        // Store results
        res[i] = sum;
    }

    return res;
}

template <typename IVec, typename Vec>
bool verify_results(std::ostream& ostream, const config &cfg, const Vec &t, const IVec &A_row, const IVec &A_col, const Vec &A_data, const Vec &u1, const Vec &u2, const Vec &u3) {
    Vec ref = calculate_results(cfg, A_row, A_col, A_data, u1, u2, u3);
    std::string ref_str = vector_to_string(ref);
    std::string t_str = vector_to_string(t);

    for (size_t i = 0; i < cfg.m; ++i) {
        double err = abs((t[i] - ref[i]) / ref[i]);
        if (err > 0.01) {
            ostream << "ERROR: results mismatch, " << t[i] << " != " << ref[i] << " (err: " << err << ")" <<  std::endl;
            if (std::getenv("VERBOSE")) {
                ostream << "t = A * phi(u1, u2, u3) = " << t_str << std::endl;
                ostream << "reference               = " << ref_str << std::endl;
            }
            return false;
        }
    }

    ostream << "Results correctly verified" << std::endl;
    if (std::getenv("VERBOSE")) {
        ostream << "t = A * phi(u1, u2, u3) = " << t_str << std::endl;
    }

    return true;
}

void initialize_xilinx_mem_buffer(cl_mem_ext_ptr_t *ext, size_t id, void *data) {
    ext->flags = id | XCL_MEM_TOPOLOGY; // Use memory bank `id`
    ext->obj = data;
    ext->param = 0;
}

int main(int argc, char **argv) {
    config cfg;
    if (argc < 6) {
        std::cerr << "Too few arguments" << std::endl;
        return EXIT_FAILURE;
    }

    cfg.phi_cu_count = std::strtoul(argv[1], NULL, 10);
    if (cfg.phi_cu_count > TOTAL_PHI_CU_COUNT) {
        std::cerr << "More CUs for phi than supported (" << cfg.phi_cu_count << " > " << TOTAL_PHI_CU_COUNT << ")" << std::endl;
        return EXIT_FAILURE;
    }
    cfg.spmat_cu_count = std::strtoul(argv[2], NULL, 10);
    if (cfg.spmat_cu_count > TOTAL_SPMAT_CU_COUNT) {
        std::cerr << "More CUs for spmat than supported (" << cfg.spmat_cu_count << " > " << TOTAL_SPMAT_CU_COUNT << ")" << std::endl;
        return EXIT_FAILURE;
    }

    cfg.m = std::strtoul(argv[3], NULL, 10);
    cfg.n = std::strtoul(argv[4], NULL, 10);
    cfg.density = std::min(1.0, std::max(0.0, std::stod(argv[5])));

    // Initialize the OpenCL environment
    cl_int err;
    std::string binaryFile = "spmat.xclbin";
    std::vector<cl::Device> devices = xcl::get_xilinx_devices();
    const cl::Device device = devices.front();
    cl::Context context(device, NULL, NULL, NULL, &err);
    std::vector<unsigned char> fileBuf = xcl::read_binary_file(binaryFile);
    cl::Program::Binaries bins{{reinterpret_cast<char*>(fileBuf.data()), fileBuf.size()}};
    cl::Program program(context, devices, bins, NULL, &err);
    cl::CommandQueue q(context, device, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE | CL_QUEUE_PROFILING_ENABLE, &err);

    std::string kernelnames_phi[cfg.phi_cu_count];
    cl::Kernel krnls_phi[cfg.phi_cu_count];
    for (size_t i = 0; i < cfg.phi_cu_count; ++i) {
        kernelnames_phi[i] = "phi:{phi_" + std::to_string(i + 1) + "}";
        krnls_phi[i] = cl::Kernel(program, kernelnames_phi[i].c_str(), &err);
    }

    std::string kernelnames_spmat[cfg.spmat_cu_count];
    cl::Kernel krnls_spmat[cfg.spmat_cu_count];
    for (size_t i = 0; i < cfg.spmat_cu_count; ++i) {
        kernelnames_spmat[i] = "spmat:{spmat_" + std::to_string(i + 1) + "}";
        krnls_spmat[i] = cl::Kernel(program, kernelnames_spmat[i].c_str(), &err);
    }

    std::random_device r;
    std::vector<size_t, aligned_allocator<size_t>> A_row, A_col;
    std::vector<double, aligned_allocator<double>> A_data, u1, u2, u3, t;
    std::default_random_engine gen(r());
    cfg.gen = &gen;

    if (std::getenv("VERBOSE")) {
        std::cerr << std::endl;
        std::cerr << "-------------------" << std::endl;
        std::cerr << "-  CONFIGURATION  -" << std::endl;
        std::cerr << "-------------------" << std::endl;
        std::cerr << "m:        " << cfg.m << std::endl;
        std::cerr << "n:        " << cfg.n << std::endl;
        std::cerr << "density:  " << cfg.density << std::endl;
        std::cerr << "function: phi(u1, u2, u3) = (u1 - u2 + log(u3) × sin(u1)) / (u1 × u2)" << std::endl;
        std::cerr << std::endl;
    }

    generate_input(cfg, A_row, A_col, A_data, u1, u2, u3);
    t.resize(cfg.m);
    std::cerr << "Read input" << std::endl;

    if (std::getenv("VERBOSE")) {
        std::cerr << std::endl;
        std::cerr << "-------------------" << std::endl;
        std::cerr << "-      INPUT      -" << std::endl;
        std::cerr << "-------------------" << std::endl;
        print_input(std::cerr, A_row, A_col, A_data, u1, u2, u3);
        std::cerr << std::endl;
    }

    auto start = std::chrono::steady_clock::now();

    /********************************************************
     *                                                      *
     *                         PHI                          *
     *                                                      *
     ********************************************************/

    size_t block_size = BLOCK_ALIGN(double);
    size_t block_count_phi = cfg.n / block_size;
    size_t block_distribution_phi[cfg.phi_cu_count];

    for (size_t i = 0; i < cfg.phi_cu_count; ++i) {
        block_distribution_phi[i] = block_count_phi / cfg.phi_cu_count;
        if (i < block_count_phi % cfg.phi_cu_count) {
            ++block_distribution_phi[i];
        }
    }

    // Create the buffers and allocate memory
    cl::Buffer tmp_res;
    cl_mem_ext_ptr_t tmp_res_ext;
    cl::Buffer U1[cfg.phi_cu_count];
    cl_mem_ext_ptr_t U1_ext[cfg.phi_cu_count];
    cl::Buffer U2[cfg.phi_cu_count];
    cl_mem_ext_ptr_t U2_ext[cfg.phi_cu_count];
    cl::Buffer U3[cfg.phi_cu_count];
    cl_mem_ext_ptr_t U3_ext[cfg.phi_cu_count];

    initialize_xilinx_mem_buffer(&tmp_res_ext, 3, NULL);
    tmp_res = cl::Buffer(context, CL_MEM_HOST_NO_ACCESS | CL_MEM_EXT_PTR_XILINX, sizeof(double) * cfg.n, &tmp_res_ext, &err);

    size_t pos_phi = 0;
    for (size_t i = 0; i < cfg.phi_cu_count; ++i) {
        size_t cur_part = block_distribution_phi[i] * block_size;

        if (i + 1 == cfg.phi_cu_count) {
            cur_part += cfg.m % block_size;
        }

        if (cur_part == 0) {
            continue;
        }

        initialize_xilinx_mem_buffer(&U1_ext[i], i % (MEM_BANKS - 1), u1.data() + pos_phi);
        U1[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(double) * cur_part, &U1_ext[i], &err);
        initialize_xilinx_mem_buffer(&U2_ext[i], i % (MEM_BANKS - 1), u2.data() + pos_phi);
        U2[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(double) * cur_part, &U2_ext[i], &err);
        initialize_xilinx_mem_buffer(&U3_ext[i], i % (MEM_BANKS - 1), u3.data() + pos_phi);
        U3[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(double) * cur_part, &U3_ext[i], &err);

        pos_phi += cur_part;
    }

    // Events
    cl::Event transfer_phi[cfg.phi_cu_count];
    cl::Event execute_phi[cfg.phi_cu_count];
    cl::vector<cl::Event> execute_wait_phi[cfg.phi_cu_count];
    cl::vector<cl::Event> transfer_wait_phi = {};

    pos_phi = 0;
    for (size_t i = 0; i < cfg.phi_cu_count; ++i) {
        size_t cur_part = block_distribution_phi[i] * block_size;

        if (i + 1 == cfg.phi_cu_count) {
            cur_part += cfg.n % block_size;
        }

        if (cur_part == 0) {
            continue;
        }

        // Set kernel arguments
        krnls_phi[i].setArg(0, cur_part);
        krnls_phi[i].setArg(1, tmp_res);
        krnls_phi[i].setArg(2, U1[i]);
        krnls_phi[i].setArg(3, U2[i]);
        krnls_phi[i].setArg(4, U3[i]);
        krnls_phi[i].setArg(5, pos_phi);

        // Schedule transfer of inputs to device memory, execution of kernel, and transfer of outputs back to host memory
        q.enqueueMigrateMemObjects({U1[i], U2[i], U3[i]}, 0, NULL, &transfer_phi[i]);
        execute_wait_phi[i] = {transfer_phi[i]};
        q.enqueueTask(krnls_phi[i], &execute_wait_phi[i], &execute_phi[i]);
        transfer_wait_phi.push_back(execute_phi[i]);
        pos_phi += cur_part;
    }

    /********************************************************
     *                                                      *
     *                        SPMAT                         *
     *                                                      *
     ********************************************************/

    size_t block_count_spmat = cfg.m / block_size;
    size_t block_distribution_spmat[cfg.spmat_cu_count];

    for (size_t i = 0; i < cfg.spmat_cu_count; ++i) {
        block_distribution_spmat[i] = block_count_spmat / cfg.spmat_cu_count;
        if (i < block_count_spmat % cfg.spmat_cu_count) {
            ++block_distribution_spmat[i];
        }
    }


    SpMat A(cfg.m, cfg.n, A_row.data(), A_col.data(), A_data.data(), block_distribution_spmat, cfg.spmat_cu_count, block_size);

    auto A_ell = A.ell_mat();

    cl::Buffer A_ell_col[cfg.spmat_cu_count];
    cl_mem_ext_ptr_t A_ell_col_ext[cfg.spmat_cu_count];
    cl::Buffer A_ell_val[cfg.spmat_cu_count];
    cl_mem_ext_ptr_t A_ell_val_ext[cfg.spmat_cu_count];
    cl::Buffer A_csr_row[cfg.spmat_cu_count];
    cl_mem_ext_ptr_t A_csr_row_ext[cfg.spmat_cu_count];
    cl::Buffer A_csr_col[cfg.spmat_cu_count];
    cl_mem_ext_ptr_t A_csr_col_ext[cfg.spmat_cu_count];
    cl::Buffer A_csr_val[cfg.spmat_cu_count];
    cl_mem_ext_ptr_t A_csr_val_ext[cfg.spmat_cu_count];
    cl::Buffer T[cfg.spmat_cu_count];
    cl_mem_ext_ptr_t T_ext[cfg.spmat_cu_count];

    size_t pos_spmat = 0;
    for (size_t i = 0; i < cfg.spmat_cu_count; ++i) {
        size_t cur_part = block_distribution_spmat[i] * block_size;

        if (i + 1 == cfg.spmat_cu_count) {
            cur_part += cfg.m % block_size;
        }

        if (cur_part == 0) {
            continue;
        }

        initialize_xilinx_mem_buffer(&A_ell_col_ext[i], i % (MEM_BANKS - 1), A_ell[i]->mat.ell.col.data());
        A_ell_col[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(size_t) * A_ell[i]->mat.ell.col.size(), &A_ell_col_ext[i], &err);
        initialize_xilinx_mem_buffer(&A_ell_val_ext[i], i % (MEM_BANKS - 1), A_ell[i]->mat.ell.val.data());
        A_ell_val[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(double) * A_ell[i]->mat.ell.val.size(), &A_ell_val_ext[i], &err);
        initialize_xilinx_mem_buffer(&A_csr_row_ext[i], i % (MEM_BANKS - 1), A_ell[i]->mat.csr.row.data());
        A_csr_row[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(size_t) * A_ell[i]->mat.csr.row.size(), &A_csr_row_ext[i], &err);
        initialize_xilinx_mem_buffer(&A_csr_col_ext[i], i % (MEM_BANKS - 1), A_ell[i]->mat.csr.col.data());
        A_csr_col[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(size_t) * A_ell[i]->mat.csr.col.size(), &A_csr_col_ext[i], &err);
        initialize_xilinx_mem_buffer(&A_csr_val_ext[i], i % (MEM_BANKS - 1), A_ell[i]->mat.csr.val.data());
        A_csr_val[i] = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(double) * A_ell[i]->mat.csr.val.size(), &A_csr_val_ext[i], &err);

        initialize_xilinx_mem_buffer(&T_ext[i], i % (MEM_BANKS - 1), t.data() + pos_spmat);
        T[i] = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR | CL_MEM_EXT_PTR_XILINX, sizeof(double) * cur_part, &T_ext[i], &err);

        pos_spmat += cur_part;
    }

    // Events
    cl::Event transfer_spmat[cfg.spmat_cu_count];
    cl::Event execute_spmat[cfg.spmat_cu_count];
    cl::vector<cl::Event> execute_wait_spmat[cfg.spmat_cu_count];
    cl::vector<cl::Event> transfer_wait_spmat[cfg.spmat_cu_count];

    for (size_t i = 0; i < cfg.spmat_cu_count; ++i) {
        size_t cur_part = block_distribution_spmat[i] * block_size;

        if (i + 1 == cfg.spmat_cu_count) {
            cur_part += cfg.m % block_size;
        }

        if (cur_part == 0) {
            continue;
        }

        krnls_spmat[i].setArg(0, cur_part);
        krnls_spmat[i].setArg(1, 1.0);
        krnls_spmat[i].setArg(2, A_ell[i]->mat.ell.width);
        krnls_spmat[i].setArg(3, A_ell[i]->pitch);
        krnls_spmat[i].setArg(4, A_ell_col[i]);
        krnls_spmat[i].setArg(5, A_ell_val[i]);
        krnls_spmat[i].setArg(6, A_csr_row[i]);
        krnls_spmat[i].setArg(7, A_csr_col[i]);
        krnls_spmat[i].setArg(8, A_csr_val[i]);
        krnls_spmat[i].setArg(9, tmp_res);
        krnls_spmat[i].setArg(10, T[i]);

        q.enqueueMigrateMemObjects({A_ell_col[i], A_ell_val[i], A_csr_row[i], A_csr_col[i], A_csr_val[i]}, 0, NULL, &transfer_spmat[i]);
        execute_wait_spmat[i] = {transfer_spmat[i]};
        for (auto &c : execute_phi) {
            execute_wait_spmat[i].push_back(c);
        }
        q.enqueueTask(krnls_spmat[i], &execute_wait_spmat[i], &execute_spmat[i]);
        transfer_wait_spmat[i] = {execute_spmat[i]};
        q.enqueueMigrateMemObjects({T[i]}, CL_MIGRATE_MEM_OBJECT_HOST, &transfer_wait_spmat[i]);
    }


    // Wait for all scheduled operations to finish
    q.finish();

    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    std::ofstream time;
    time.open("time.txt", std::ios::out | std::ios::trunc);
    time << elapsed.count() << std::endl;
    time.close();

    if (!verify_results(std::cerr, cfg, t, A_row, A_col, A_data, u1, u2, u3)) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
