#include <stddef.h>
#include <math.h>
#include "datatypes.hpp"

extern "C" {
    #if defined(cl_khr_fp64)
    #pragma OPENCL EXTENSION cl_khr_fp64 : enable
    #elif defined(cl_amd_fp64)
    #pragma OPENCL EXTENSION cl_amd_fp64 : enable
    #endif

    void spmat(
        size_t n,
        fix_t scale,
        size_t ell_w,
        size_t ell_pitch,
        const size_t *ell_col,
        const fix_t *ell_val,
        const size_t *csr_row,
        const size_t *csr_col,
        const fix_t *csr_val,
        const lfix_t *in,
        lfix_t *out)
    {
    #pragma HLS INTERFACE m_axi port=ell_col bundle=aximm1
    #pragma HLS INTERFACE m_axi port=ell_val bundle=aximm2
    #pragma HLS INTERFACE m_axi port=csr_row bundle=aximm1
    #pragma HLS INTERFACE m_axi port=csr_col bundle=aximm2
    #pragma HLS INTERFACE m_axi port=csr_val bundle=aximm3
    #pragma HLS INTERFACE m_axi port=in bundle=aximm4
    #pragma HLS INTERFACE m_axi port=out bundle=aximm3
        for (size_t i = 0; i < n; ++i)
        {
            lfix_t sum = 0;
            for (size_t j = 0; j < ell_w; ++j)
            {
                size_t c = ell_col[i + j * ell_pitch];
                if (c != (size_t)(-1))
                {
                    sum += ell_val[i + j * ell_pitch] * in[c];
                }
            }
            if (csr_row)
            {
                for (size_t j = csr_row[i], e = csr_row[i + 1]; j < e; ++j)
                {
                    sum += csr_val[j] * in[csr_col[j]];
                }
            }
            out[i] = scale * sum;
        }
    }
}
