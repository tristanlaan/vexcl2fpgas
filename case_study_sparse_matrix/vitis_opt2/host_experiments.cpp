#include <iostream>
#include <iomanip>
#include <vector>
#include <random>
#include <algorithm>
#include <string>
#include <cmath>

#include "xcl2.hpp"
#include "spmat.hpp"
#include "datatypes.hpp"
#include "extern_vars.hpp"

typedef struct {
    size_t m;
    size_t n;
    double density;
    std::default_random_engine *gen;
} config;

typedef std::pair<size_t, size_t> coordinate;

template <typename Vec>
std::string vector_to_string(const Vec &T, const int width=5, const int precision=3) {
    std::ostringstream ostream;
    ostream << "[";
    for (auto iter = T.begin(); iter != T.end(); ++iter) {
        ostream << std::setw(width) << std::setprecision(precision) << *iter;
        if (iter + 1 != T.end()) {
            ostream << ", ";
        }
    }
    ostream << "]";

    return ostream.str();
}

template <typename IVec, typename Vec>
void print_input(std::ostream& ostream, const IVec &A_row, const IVec &A_col, const Vec &A_data, const Vec &u1, const Vec &u2, const Vec &u3) {
    ostream << "A_row  = " << vector_to_string(A_row) << std::endl;
    ostream << "A_col  = " << vector_to_string(A_col) << std::endl;
    ostream << "A_data = " << vector_to_string(A_data) << std::endl;
    ostream << "u1     = " << vector_to_string(u1) << std::endl;
    ostream << "u2     = " << vector_to_string(u2) << std::endl;
    ostream << "u3     = " << vector_to_string(u3) << std::endl;
}

double phi(double U1, double U2, double U3) {
    auto u1 = U1;
    auto u2 = U2;
    auto u3 = U3;

    return (u1 - u2 + std::log(u3) * std::log(u3) * std::sin(u1)) / (u1 * u2);
}

template <typename IVec, typename Vec>
std::vector<double> calculate_results(const config &cfg, const IVec &A_row, const IVec &A_col, const Vec &A_data, const Vec &u1, const Vec &u2, const Vec &u3) {
    std::vector<double> res(cfg.m);

    std::vector<double> tmp(cfg.n);

    for (size_t i = 0; i < cfg.n; ++i) {
        tmp[i] = phi(u1[i].to_double(), u2[i].to_double(), u3[i].to_double());
    }

    for (size_t i = 0; i < cfg.m; ++i) {
        double sum = 0;

        // Calculate matrix multiplication of current row
        for (size_t j = A_row[i]; j < A_row[i + 1]; ++j) {
            sum += A_data[j].to_double() * tmp[A_col[j]];
        }

        // Store results
        res[i] = sum;
    }

    return res;
}

template <typename IVec, typename Vec, typename LVec>
bool verify_results(std::ostream& ostream, const config &cfg, const LVec &t, const IVec &A_row, const IVec &A_col, const Vec &A_data, const Vec &u1, const Vec &u2, const Vec &u3) {
    auto ref = calculate_results(cfg, A_row, A_col, A_data, u1, u2, u3);
    std::string ref_str = vector_to_string(ref);
    std::string t_str = vector_to_string(t);

    for (size_t i = 0; i < cfg.m; ++i) {
        double err = abs((t[i].to_double() - ref[i]) / ref[i]);
        if (err > 1) {
            ostream << "ERROR: results mismatch, " << t[i] << " != " << ref[i] << " (err: " << err << ")" <<  std::endl;
            if (std::getenv("VERBOSE")) {
                ostream << "t = A * phi(u1, u2, u3) = " << t_str << std::endl;
                ostream << "reference               = " << ref_str << std::endl;
            }
            return false;
        }
    }

    ostream << "Results correctly verified" << std::endl;
    if (std::getenv("VERBOSE")) {
        ostream << "t = A * phi(u1, u2, u3) = " << t_str << std::endl;
    }

    return true;
}

int main(int argc, char **argv) {
    // Initialize the OpenCL environment
    cl_int err;
    std::string binaryFile = "spmat.xclbin";
    std::vector<cl::Device> devices = xcl::get_xilinx_devices();
    const cl::Device device = devices.front();
    cl::Context context(device, NULL, NULL, NULL, &err);
    std::vector<unsigned char> fileBuf = xcl::read_binary_file(binaryFile);
    cl::Program::Binaries bins{{reinterpret_cast<char*>(fileBuf.data()), fileBuf.size()}};
    cl::Program program(context, devices, bins, NULL, &err);
    cl::CommandQueue q(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
    cl::Kernel krnl_phi(program, "phi", &err);
    cl::Kernel krnl_sparse_matrix(program, "spmat", &err);

    config cfg;
    std::random_device r;
    cfg.m = m;
    cfg.n = n;
    std::default_random_engine gen(r());
    cfg.gen = &gen;
    std::vector<size_t, aligned_allocator<size_t>> A_row(A_row_data, A_row_data + m + 1), A_col(A_col_data, A_col_data + A_row_data[m]);
    std::vector<fix_t, aligned_allocator<fix_t>> A_data(A_data_data, A_data_data + A_row_data[m]), u1(u1_data, u1_data + n), u2(u2_data, u2_data + n), u3(u3_data, u3_data + n), t;

    if (std::getenv("VERBOSE")) {
        std::cerr << std::endl;
        std::cerr << "-------------------" << std::endl;
        std::cerr << "-  CONFIGURATION  -" << std::endl;
        std::cerr << "-------------------" << std::endl;
        std::cerr << "m:        " << cfg.m << std::endl;
        std::cerr << "n:        " << cfg.n << std::endl;
        std::cerr << "density:  " << cfg.density << std::endl;
        std::cerr << "function: phi(u1, u2, u3) = (u1 - u2 + log(u3) × sin(u1)) / (u1 × u2)" << std::endl;
        std::cerr << std::endl;
    }

    t.resize(cfg.m);
    std::cerr << "Read input" << std::endl;

    if (std::getenv("VERBOSE")) {
        std::cerr << std::endl;
        std::cerr << "-------------------" << std::endl;
        std::cerr << "-      INPUT      -" << std::endl;
        std::cerr << "-------------------" << std::endl;
        print_input(std::cerr, A_row, A_col, A_data, u1, u2, u3);
        std::cerr << std::endl;
    }

    auto start = std::chrono::steady_clock::now();

    SpMat<fix_t, size_t, std::vector<fix_t, aligned_allocator<fix_t>>, std::vector<size_t, aligned_allocator<size_t>>> A(cfg.m, cfg.n, A_row.data(), A_col.data(), A_data.data());

    auto A_ell = A.ell_mat();

    // Create the buffers and allocate memory
    cl::Buffer A_ell_col(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(size_t) * A_ell->mat.ell.col.size(), A_ell->mat.ell.col.data(), &err);
    cl::Buffer A_ell_val(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(fix_t) * A_ell->mat.ell.val.size(), A_ell->mat.ell.val.data(), &err);
    cl::Buffer A_csr_row(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(size_t) * A_ell->mat.csr.row.size(), A_ell->mat.csr.row.data(), &err);
    cl::Buffer A_csr_col(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(size_t) * A_ell->mat.csr.col.size(), A_ell->mat.csr.col.data(), &err);
    cl::Buffer A_csr_val(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(fix_t) * A_ell->mat.csr.val.size(), A_ell->mat.csr.val.data(), &err);
    cl::Buffer tmp_res(context, CL_MEM_HOST_NO_ACCESS, sizeof(lfix_t) * cfg.n, NULL, &err);
    cl::Buffer U1(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(fix_t) * u1.size(), u1.data(), &err);
    cl::Buffer U2(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(fix_t) * u2.size(), u2.data(), &err);
    cl::Buffer U3(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(fix_t) * u3.size(), u3.data(), &err);
    cl::Buffer T(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, sizeof(lfix_t) * t.size(), t.data(), &err);

    // Set kernel arguments
    krnl_phi.setArg(0, cfg.n);
    krnl_phi.setArg(1, tmp_res);
    krnl_phi.setArg(2, U1);
    krnl_phi.setArg(3, U2);
    krnl_phi.setArg(4, U3);

    krnl_sparse_matrix.setArg(0, cfg.m);
    krnl_sparse_matrix.setArg(1, (fix_t) 1.0);
    krnl_sparse_matrix.setArg(2, A_ell->mat.ell.width);
    krnl_sparse_matrix.setArg(3, A_ell->pitch);
    krnl_sparse_matrix.setArg(4, A_ell_col);
    krnl_sparse_matrix.setArg(5, A_ell_val);
    krnl_sparse_matrix.setArg(6, A_csr_row);
    krnl_sparse_matrix.setArg(7, A_csr_col);
    krnl_sparse_matrix.setArg(8, A_csr_val);
    krnl_sparse_matrix.setArg(9, tmp_res);
    krnl_sparse_matrix.setArg(10, T);

    // Schedule transfer of inputs to device memory, execution of kernel, and transfer of outputs back to host memory
    q.enqueueMigrateMemObjects({U1, U2, U3}, 0);
    q.enqueueTask(krnl_phi);
    q.enqueueMigrateMemObjects({A_ell_col, A_ell_val, A_csr_row, A_csr_col, A_csr_val}, 0);
    q.enqueueTask(krnl_sparse_matrix);
    q.enqueueMigrateMemObjects({T}, CL_MIGRATE_MEM_OBJECT_HOST);

    // Wait for all scheduled operations to finish
    q.finish();

    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    std::ofstream time;
    time.open("time.txt", std::ios::out | std::ios::trunc);
    time << elapsed.count() << std::endl;
    time.close();

    if (!verify_results(std::cerr, cfg, t, A_row, A_col, A_data, u1, u2, u3)) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
