#include <stddef.h>
#include "hls_math.h"
#include "datatypes.hpp"

extern "C" {
    #if defined(cl_khr_fp64)
    #pragma OPENCL EXTENSION cl_khr_fp64 : enable
    #elif defined(cl_amd_fp64)
    #pragma OPENCL EXTENSION cl_amd_fp64 : enable
    #endif

    void phi(
        size_t n,
        lfix_t *prm_1,
        const fix_t *prm_tag_1_1,
        const fix_t *prm_tag_2_1,
        const fix_t *prm_tag_3_1)
    {
    #pragma HLS INTERFACE m_axi port=prm_1 bundle=aximm4
    #pragma HLS INTERFACE m_axi port=prm_tag_1_1 bundle=aximm1
    #pragma HLS INTERFACE m_axi port=prm_tag_2_1 bundle=aximm2
    #pragma HLS INTERFACE m_axi port=prm_tag_3_1 bundle=aximm3
        for (size_t idx = 0; idx < n; ++idx)
        {
            prm_1[idx] = (((prm_tag_1_1[idx] - prm_tag_2_1[idx]) + ((hls::log(prm_tag_3_1[idx]) * hls::log(prm_tag_3_1[idx])) * hls::sin(prm_tag_1_1[idx]))) / (prm_tag_1_1[idx] * prm_tag_2_1[idx]));
        }
    }
}
