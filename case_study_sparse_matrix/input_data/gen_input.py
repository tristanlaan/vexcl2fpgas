#!/usr/bin/python3
import sys
import numpy as np
import scipy.sparse as sparse
import json


def gen_input(m, n, density=0.01, min=0.01, max=64.0):
    rng = np.random.default_rng()
    A = sparse.rand(m, n, density, format='csr', random_state=rng)
    A_data = min + A.data * (max - min)
    A_row = A.indptr
    A_col = A.indices
    u1 = min + rng.random(n) * (max - min)
    u2 = min + rng.random(n) * (max - min)
    u3 = min + rng.random(n) * (max - min)

    print("#include \"extern_vars.hpp\"")
    print()
    print(f"const unsigned long m = {m};")
    print(f"const unsigned long n = {n};")
    print()
    print(f"const unsigned long A_row_data_loc[] = {{{', '.join(str(t) for t in A_row)}}};")
    print(f"const unsigned long *A_row_data = A_row_data_loc;")
    print(f"const double A_data_data_loc[] = {{{', '.join(str(t) for t in A_data)}}};")
    print(f"const double *A_data_data = A_data_data_loc;")
    print(f"const unsigned long A_col_data_loc[] = {{{', '.join(str(t) for t in A_col)}}};")
    print(f"const unsigned long *A_col_data = A_col_data_loc;")
    print(f"const double u1_data_loc[] = {{{', '.join(str(t) for t in u1)}}};")
    print(f"const double *u1_data = u1_data_loc;")
    print(f"const double u2_data_loc[] = {{{', '.join(str(t) for t in u2)}}};")
    print(f"const double *u2_data = u2_data_loc;")
    print(f"const double u3_data_loc[] = {{{', '.join(str(t) for t in u3)}}};")
    print(f"const double *u3_data = u3_data_loc;")


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
        description="Generate matrix input for affine transform")
    parser.add_argument('-m', type=int, default=25, help="Width of matrix (defaults to 25)")
    parser.add_argument('-n', type=int, default=25, help="Height of matrix (defaults to 25)")
    parser.add_argument('--density', '-d', type=float, default=0.01, help="Density of matrix (defaults to 0.01)")
    parser.add_argument('--min', type=float, default=0.01,
                        help="Lowest number to be drawn from distribution (defaults to 0.01)")
    parser.add_argument('--max', type=float, default=64.0,
                        help="Highest number to be drawn from distribution (defaults to 64.0)")

    args = parser.parse_args()

    gen_input(args.m, args.n, density=args.density, min=args.min, max=args.max)
