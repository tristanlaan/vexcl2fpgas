#include <iostream>
#include <iomanip>
#include <vector>
#include <random>
#include <algorithm>
#include <string>
#include <cmath>

#define CL_TARGET_OPENCL_VERSION 120
#define VEXCL_SHOW_KERNELS

#include <vexcl/vexcl.hpp>

typedef struct {
    size_t m;
    size_t n;
    double density;
    std::default_random_engine *gen;
} config;

typedef std::pair<size_t, size_t> coordinate;

template <typename Vec>
std::string vector_to_string(const Vec &T, const int width=5, const int precision=3) {
    std::ostringstream ostream;
    ostream << "[";
    for (auto iter = T.begin(); iter != T.end(); ++iter) {
        ostream << std::setw(width) << std::setprecision(precision) << *iter;
        if (iter + 1 != T.end()) {
            ostream << ", ";
        }
    }
    ostream << "]";

    return ostream.str();
}

template <typename Vec>
coordinate generate_coord(const config &cfg, const Vec& coords) {
    std::uniform_int_distribution<size_t> row_dist(0, cfg.m - 1);
    std::uniform_int_distribution<size_t> col_dist(0, cfg.n - 1);

    struct compare {
        coordinate key;
        compare(coordinate const &k): key(k) {}

        bool operator()(coordinate const &i) {
            return (key == i);
        }
    };

    while (true) {
        coordinate c(row_dist(*cfg.gen), col_dist(*cfg.gen));

        if (std::none_of(coords.begin(), coords.end(), compare(c))) {
            return c;
        }

    }
}

template <typename Vec>
std::vector<size_t> get_coord_row(const Vec& coords, const size_t row) {
    std::vector<size_t> cols;
    struct compare {
        size_t row;
        compare(size_t const &k): row(k) {}

        bool operator()(coordinate const &i) {
            return (row == std::get<0>(i));
        }
    };

    auto iter = coords.begin();

    while ((iter = std::find_if(iter, coords.end(), compare(row))) != coords.end()) {
        cols.push_back(std::get<1>(*iter));
        ++iter;
    }

    std::sort(cols.begin(), cols.end());

    return cols;
}

template <typename Vec, typename Dist, typename Gen>
void generate_vector(Vec &v, size_t n, Dist &dist, Gen &gen) {
    v.resize(n);
    for (size_t i = 0; i < n; ++i) {
        v[i] = dist(gen);
    }
}

template <typename IVec, typename Vec>
void generate_input(const config &cfg, IVec &A_row, IVec &A_col, Vec &A_data, Vec &u1, Vec &u2, Vec &u3) {
    std::uniform_real_distribution data_dist(.01, 64.0);

    size_t elements = cfg.density * cfg.m * cfg.n;
    A_row.resize(cfg.m + 1);
    A_col.resize(elements);
    A_data.resize(elements);
    std::vector<coordinate> coords;

    generate_vector(u1, cfg.n, data_dist, *cfg.gen);
    generate_vector(u2, cfg.n, data_dist, *cfg.gen);
    generate_vector(u3, cfg.n, data_dist, *cfg.gen);

    for (size_t i = 0; i < elements; ++i) {
        coords.push_back(generate_coord(cfg, coords));
        A_data[i] = data_dist((*cfg.gen));
    }


    A_row[0] = 0;
    auto iter = A_col.begin();

    auto sorted = coords;
    std::sort(sorted.begin(), sorted.end());

    for (size_t i = 0; i < cfg.m; ++i) {
        auto cols = get_coord_row(coords, i);

        if (cols.size() > 0) {
            std::copy(cols.begin(), cols.end(), iter);
            iter += cols.size();
        }

        A_row[i + 1] = A_row[i] + cols.size();

    }
}

template <typename IVec, typename Vec>
void print_input(std::ostream& ostream, const IVec &A_row, const IVec &A_col, const Vec &A_data, const Vec &u1, const Vec &u2, const Vec &u3) {
    ostream << "A_row  = " << vector_to_string(A_row) << std::endl;
    ostream << "A_col  = " << vector_to_string(A_col) << std::endl;
    ostream << "A_data = " << vector_to_string(A_data) << std::endl;
    ostream << "u1     = " << vector_to_string(u1) << std::endl;
    ostream << "u2     = " << vector_to_string(u2) << std::endl;
    ostream << "u3     = " << vector_to_string(u3) << std::endl;
}

vex::vector<cl_double> phi(vex::vector<cl_double> U1, vex::vector<cl_double> U2, vex::vector<cl_double> U3) {
    auto u1 = vex::tag<1>(U1);
    auto u2 = vex::tag<2>(U2);
    auto u3 = vex::tag<3>(U3);

    return (u1 - u2 + vex::log(u3) * vex::log(u3) * vex::sin(u1)) / (u1 * u2);
}

double phi(double U1, double U2, double U3) {
    auto u1 = U1;
    auto u2 = U2;
    auto u3 = U3;

    return (u1 - u2 + std::log(u3) * std::log(u3) * std::sin(u1)) / (u1 * u2);
}

template <typename IVec, typename Vec>
Vec calculate_results(const config &cfg, const IVec &A_row, const IVec &A_col, const Vec &A_data, const Vec &u1, const Vec &u2, const Vec &u3) {
    Vec res = Vec(cfg.m);

    Vec tmp = Vec(cfg.n);

    for (size_t i = 0; i < cfg.n; ++i) {
        tmp[i] = phi(u1[i], u2[i], u3[i]);
    }

    for (size_t i = 0; i < cfg.m; ++i) {
        double sum = 0;

        // Calculate matrix multiplication of current row
        for (size_t j = A_row[i]; j < A_row[i + 1]; ++j) {
            sum += A_data[j] * tmp[A_col[j]];
        }

        // Store results
        res[i] = sum;
    }

    return res;
}

template <typename IVec, typename Vec>
bool verify_results(std::ostream& ostream, const config &cfg, const Vec &t, const IVec &A_row, const IVec &A_col, const Vec &A_data, const Vec &u1, const Vec &u2, const Vec &u3) {
    Vec ref = calculate_results(cfg, A_row, A_col, A_data, u1, u2, u3);
    std::string ref_str = vector_to_string(ref);
    std::string t_str = vector_to_string(t);

    for (size_t i = 0; i < cfg.m; ++i) {
        double err = abs((t[i] - ref[i]) / ref[i]);
        if (err > 1) {
            ostream << "ERROR: results mismatch, " << t[i] << " != " << ref[i] << " (err: " << err << ")" <<  std::endl;
            if (std::getenv("VERBOSE")) {
                ostream << "t = A * phi(u1, u2, u3) = " << t_str << std::endl;
                ostream << "reference               = " << ref_str << std::endl;
            }
            return false;
        }
    }

    ostream << "Results correctly verified" << std::endl;
    if (std::getenv("VERBOSE")) {
        ostream << "t = A * phi(u1, u2, u3) = " << t_str << std::endl;
    }

    return true;
}

int main(int argc, char **argv) {
    vex::Context ctx(vex::Filter::DoublePrecision);
    std::cerr << ctx << std::endl;

    config cfg;
    std::random_device r;
    std::vector<size_t> A_row, A_col;
    std::vector<double> A_data, u1, u2, u3, t;
    std::default_random_engine gen(r());
    cfg.gen = &gen;

    if (argc >= 4) {
        cfg.density = std::min(1.0, std::max(0.0, std::stod(argv[3])));
    } else {
        cfg.density = 0.05;
    }
    if (argc >= 3) {
        cfg.n = std::stoul(argv[2]);
    } else {
        cfg.n = 20;
    }
    if (argc >= 2) {
        cfg.m = std::stoul(argv[1]);
    } else {
        cfg.m = 20;
    }

    if (std::getenv("VERBOSE")) {
        std::cerr << std::endl;
        std::cerr << "-------------------" << std::endl;
        std::cerr << "-  CONFIGURATION  -" << std::endl;
        std::cerr << "-------------------" << std::endl;
        std::cerr << "m:        " << cfg.m << std::endl;
        std::cerr << "n:        " << cfg.n << std::endl;
        std::cerr << "density:  " << cfg.density << std::endl;
        std::cerr << "function: phi(u1, u2, u3) = (u1 - u2 + log(u3) × sin(u1)) / (u1 × u2)" << std::endl;
        std::cerr << std::endl;
    }

    generate_input(cfg, A_row, A_col, A_data, u1, u2, u3);
    t.resize(cfg.m);
    std::cerr << "Read input" << std::endl;

    if (std::getenv("VERBOSE")) {
        std::cerr << std::endl;
        std::cerr << "-------------------" << std::endl;
        std::cerr << "-      INPUT      -" << std::endl;
        std::cerr << "-------------------" << std::endl;
        print_input(std::cerr, A_row, A_col, A_data, u1, u2, u3);
        std::cerr << std::endl;
    }

    auto start = std::chrono::steady_clock::now();

    // Transfer host-side doubles into device-side cl_double vectors
    vex::SpMat<cl_double> A(ctx, cfg.m, cfg.n, A_row.data(), A_col.data(), reinterpret_cast<cl_double*>(A_data.data()));
    vex::vector<cl_double> U1(ctx, u1.size(), reinterpret_cast<cl_double*>(u1.data()));
    vex::vector<cl_double> U2(ctx, u2.size(), reinterpret_cast<cl_double*>(u2.data()));
    vex::vector<cl_double> U3(ctx, u3.size(), reinterpret_cast<cl_double*>(u3.data()));
    // Store result in T
    vex::vector<cl_double> T(ctx, t.size());

    T = A * phi(U1, U2, U3);

    vex::copy(T.begin(), T.end(), reinterpret_cast<cl_double*>(t.data()));

    auto end = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    std::ofstream time;
    time.open("time.txt", std::ios::out | std::ios::trunc);
    time << elapsed.count() << std::endl;
    time.close();

    if (!verify_results(std::cerr, cfg, t, A_row, A_col, A_data, u1, u2, u3)) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
