#include <stddef.h>
#include "hls_math.h"
#include "datatypes.hpp"

#define BURSTBUFFERSIZE 256

extern "C" {
    #if defined(cl_khr_fp64)
    #pragma OPENCL EXTENSION cl_khr_fp64 : enable
    #elif defined(cl_amd_fp64)
    #pragma OPENCL EXTENSION cl_amd_fp64 : enable
    #endif

    void phi(
        size_t n,
        lfix_t *prm_1,
        const fix_t *prm_tag_1_1,
        const fix_t *prm_tag_2_1,
        const fix_t *prm_tag_3_1)
    {
    #pragma HLS INTERFACE m_axi offset=slave port=prm_1 bundle=aximm4 max_write_burst_length = 256
    #pragma HLS INTERFACE m_axi offset=slave port=prm_tag_1_1 bundle=aximm1 max_read_burst_length = 256
    #pragma HLS INTERFACE m_axi offset=slave port=prm_tag_2_1 bundle=aximm2 max_read_burst_length = 256
    #pragma HLS INTERFACE m_axi offset=slave port=prm_tag_3_1 bundle=aximm3 max_read_burst_length = 256

        fix_t burstbuffer1[BURSTBUFFERSIZE];
        fix_t burstbuffer2[BURSTBUFFERSIZE];
        fix_t burstbuffer3[BURSTBUFFERSIZE];
        lfix_t burstbuffer4[BURSTBUFFERSIZE];

        for (size_t idx = 0; idx < n; idx += BURSTBUFFERSIZE)
        {
            int chunk_size = BURSTBUFFERSIZE;
            // boundary checks
            if ((idx + BURSTBUFFERSIZE) > n) {
                chunk_size = n - idx;
            }

            for (int j = 0; j < chunk_size; ++j) {
                burstbuffer1[j] = prm_tag_1_1[idx + j];
                burstbuffer2[j] = prm_tag_2_1[idx + j];
                burstbuffer3[j] = prm_tag_3_1[idx + j];
            }

            for (int j = 0; j < chunk_size; ++j) {
                burstbuffer4[j] = (((burstbuffer1[j] - burstbuffer2[j]) + ((hls::log(burstbuffer3[j]) * hls::log(burstbuffer3[j])) * hls::sin(burstbuffer1[j]))) / (burstbuffer1[j] * burstbuffer2[j]));
            }

            for (int j = 0; j < chunk_size; ++j) {
                prm_1[idx + j] = burstbuffer4[j];
            }
        }
    }
}
