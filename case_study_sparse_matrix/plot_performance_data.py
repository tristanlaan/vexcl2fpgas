#!/usr/bin/python3
from pathlib import Path
import json
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle

VERSION_NAMES = {
    'vexcl': r'VexCL',
    'vitis': r'Vitis (unopt.)',
    'vitis_opt1': r'Vitis (opt. 1)',
    'vitis_opt2': r'Vitis (opt. 2)',
    'vitis_opt3': r'Vitis (opt. 3)',
    'vitis_opt4': r'Vitis (opt. 4)',
    'vitis_opt5': r'Vitis (opt. 5)'
}

DIMENSION_NAMES = {
    (512, 512, 0.01): r'\begin{gather*}512 {\times} 512\\[-0.5em] d=0.01\end{gather*}',
    (5000, 5000, 0.01): r'\begin{gather*}5000 {\times} 5000\\[-0.5em] d=0.01\end{gather*}',
    (100000, 100000, 0.01): r'\begin{gather*}10^5 {\times} 10^5\\[-0.5em] d=0.01\end{gather*}',
    (12500, 12500, 0.64): r'\begin{gather*}12500 {\times} 12500\\[-0.5em] d=0.64\end{gather*}',
    (1000000, 1000000, 0.0001): r'\begin{gather*}10^6 {\times} 10^6\\[-0.5em] d=10^{-4}\end{gather*}'
}

DIMENSION_NAMES_ALT = {
    (512, 512, 0.01): r'$512$',
    (5000, 5000, 0.01): r'$5000$',
    (12500, 12500, 0.64): r'$12500$',
    (100000, 100000, 0.01): r'$10^4$',
    (1000000, 1000000, 0.0001): r'$10^5$',
}

DIMENSION_NAMES_SIMPLE = {
    (512, 512, 0.01): r'size: $512 {\times} 512$ density: $0.01$',
    (5000, 5000, 0.01): r'size: $5000 {\times} 5000$ density: $0.01$',
    (100000, 100000, 0.01): r'size: $10^5 {\times} 10^5$ density: $0.01$',
    (12500, 12500, 0.64): r'size: $12500 {\times} 12500$ density: $0.64$',
    (1000000, 1000000, 0.0001): r'size: $10^6 {\times} 10^6$ density: $10^{-4}$'
}

KERNEL_NAMES = {
    'phi': 'phi',
    'spmat': 'spmat'
}

TABLE_PREAMBLE = \
r'''% Required packages / defines:
% \usepackage{tabu}
% \usepackage{xcolor}
% \usepackage[utf8]{inputenc}
%
% \DeclareUnicodeCharacter{03BC}{\textmu}
% \definecolor{posgreen}{rgb}{0.13, 0.55, 0.13}
%
% Usage:
% \begin{table}
%     \centering
%     \input{<FILENAME>}
%     \caption{caption}
%     \label{tab:label}
% \end{table}

'''


def get_performance_data(directory: Path, version: str, m: int, n: int, d: float) -> dict:
    walltime = []
    kerneltime = dict()
    power = []

    for file in directory.glob(f"{version}___{m}x{n}_{d}__*.json"):
        with file.open() as f:
            data = json.load(f)
            if version == 'vexcl':
                power += [x for x in data['power'] if x > 1]
            else:
                power += [x['power'] for x in data['power']]
            walltime.append(data['time'])
            if version != 'vexcl':
                for kernel in data['Kernel Execution']:
                    kernel_name = kernel['kernel']
                    if kernel_name not in kerneltime:
                        kerneltime[kernel_name] = []
                    kerneltime[kernel_name].append(kernel['time'] / 1000)

    if len(walltime) < 1 or len(power) < 1:
        raise RuntimeError(f"No data found for version {VERSION_NAMES[version]} and dimension ({m}, {n}, {d})")

    data = {
        'walltime': {'mean': np.mean(walltime), 'std': np.std(walltime)},
        'power': {'mean': np.mean(power), 'std': np.std(power)}
    }



    if len(kerneltime) > 0:
        data['kerneltime'] = dict()
        for kernel, time in kerneltime.items():
            data['kerneltime'][kernel] = {'mean': np.mean(time), 'std': np.std(time)}

    return data


def get_all_performance_data(directory: Path, versions: list, dimensions: list) -> dict:
    data = dict()
    for version in versions:
        data[version] = dict()
        for dim in dimensions:
            data[version][dim] = get_performance_data(directory, version, *dim)

    return data


def calc_energy(pmean, pstd, tmean, tstd):
    pvar = pstd ** 2
    tmean = tmean / 3600
    tvar = (tstd / 3600) ** 2
    mean = pmean * tmean
    std = np.sqrt(pvar * tvar + pmean ** 2 * tvar + tmean ** 2 * pvar)
    return mean, std


def calc_energies(pmeans, pstds, tmeans, tstds):
    pvars = np.array([std ** 2 for std in pstds])
    tmeans = np.array([mean / 3600 for mean in tmeans])
    tvars = np.array([(std / 3600) ** 2 for std in tstds])
    pmeans = np.array(pmeans)
    means = pmeans * tmeans
    stds = np.sqrt(pvars * tvars + pmeans ** 2 * tvars + tmeans ** 2 * pvars)
    return means, stds


def plot_power_plot(versions: list, dimensions: list, data: dict, directory: Path):
    width = 5.7885
    # width = 3.3374
    height = 4.341375
    # height = 2.5

    f = plt.figure(frameon=False, figsize=(width, height))
    x = np.arange(len(dimensions))
    total_width = 0.7
    width = 0.7 / len(versions)
    for i, version in enumerate(versions):
        means = [data[version][dimension]['power']['mean'] for dimension in dimensions]
        stds = [data[version][dimension]['power']['std'] for dimension in dimensions]
        plt.bar(x - total_width / 2 + i * width + width / 2, means, width, yerr=stds, label=VERSION_NAMES[version])

    plt.ylabel('Power (W)')
    plt.ylim(bottom=0, top=65)
    plt.xticks(x, labels=[DIMENSION_NAMES[dimension] for dimension in dimensions])
    plt.xlabel('Matrix configuration')
    plt.title('Power consumption')
    plt.legend()
    f.savefig(directory / "power_plot.pdf")


def plot_walltime_plot(versions: list, dimensions: list, data: dict, v: int, directory: Path):
    width = 5.7885
    # width = 3.3374
    # width = 5.8
    height = 4.341375
    # height = 2.5
    # height = 3.5
    if v == 1:
        f = plt.figure(frameon=False, figsize=(width * .5, height * (4/6)))
    elif v == 0:
        f = plt.figure(frameon=False, figsize=(width * .45, height * (4/6)))

    x = np.arange(len(dimensions))
    total_width = 0.7
    width = 0.7 / len(versions)
    for i, version in enumerate(versions):
        means = [data[version][dimension]['walltime']['mean'] for dimension in dimensions]
        stds = [data[version][dimension]['walltime']['std'] for dimension in dimensions]
        plt.bar(x - total_width / 2 + i * width + width / 2, means, width, yerr=stds, label=VERSION_NAMES[version])

    plt.ylabel('Wall time (s)')
    plt.ylim(bottom=0)
    plt.xticks(x, labels=[DIMENSION_NAMES[dimension] for dimension in dimensions])
    plt.xlabel('Matrix configuration')
    plt.title('Wall time')
    plt.legend()
    f.savefig(directory / f"walltime_{v}_plot.pdf")


def plot_kerneltime_plot(versions: list, dimensions: list, data: dict, kernel: str, v: int, directory: Path):
    width = 5.7885
    # width = 3.3374
    height = 4.341375
    # height = 2.5
    if (v == 1 and kernel == 'spmat') or (v == 0 and kernel == 'phi'):
        f = plt.figure(frameon=False, figsize=(width * .55, height * (4/6)))
    else:
        f = plt.figure(frameon=False, figsize=(width * .45, height * (4/6)))

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = cycle(prop_cycle.by_key()['color'])
    # skip vexcl color
    next(colors)

    x = np.arange(len(dimensions))
    total_width = 0.7
    width = 0.7 / len(versions)
    for i, version in enumerate(versions):
        means = [data[version][dimension]['kerneltime'][kernel]['mean'] for dimension in dimensions]
        stds = [data[version][dimension]['kerneltime'][kernel]['std'] for dimension in dimensions]
        plt.bar(x - total_width / 2 + i * width + width / 2, means, width, yerr=stds, label=VERSION_NAMES[version], color=next(colors))

    plt.ylabel('Kernel time (s)')
    if kernel == 'spmat' and v == 1:
        plt.ylim(bottom=0, top=9.25)
    else:
        plt.ylim(bottom=0)
    if kernel == 'spmat':
        plt.xticks(x, labels=[DIMENSION_NAMES[dimension] for dimension in dimensions])
        plt.xlabel('Matrix configuration')
    else:
        plt.xticks(x, labels=[DIMENSION_NAMES_ALT[dimension] for dimension in dimensions])
        plt.xlabel('Vector size')

    plt.title(f'Kernel time {KERNEL_NAMES[kernel]}')
    plt.legend()
    f.savefig(directory / f"kerneltime_{kernel}_{v}_plot.pdf")


def plot_energy_plot(versions: list, dimensions: list, data: dict, v: int, directory: Path):
    width = 5.7885
    # width = 3.3374
    # width = 5.8
    height = 4.341375
    # height = 2.5
    # height = 3.5
    if v == 1:
        f = plt.figure(frameon=False, figsize=(width * .5, height * (4/6)))
    elif v == 0:
        f = plt.figure(frameon=False, figsize=(width * .45, height * (4/6)))

    x = np.arange(len(dimensions))
    total_width = 0.7
    width = 0.7 / len(versions)
    for i, version in enumerate(versions):
        pmeans = [data[version][dimension]['power']['mean'] for dimension in dimensions]
        pstds = [data[version][dimension]['power']['std'] for dimension in dimensions]
        tmeans = [data[version][dimension]['walltime']['mean'] for dimension in dimensions]
        tstds = [data[version][dimension]['walltime']['std']for dimension in dimensions]

        means, stds = calc_energies(pmeans, pstds, tmeans, tstds)

        plt.bar(x - total_width / 2 + i * width + width / 2, means, width, yerr=stds, label=VERSION_NAMES[version])
        # plt.bar(x - total_width / 2 + i * width + width / 2, means, width, yerr=stds, label=VERSION_NAMES[version], hatch='///', alpha=.99999999)

    plt.ylabel('Energy (Wh)')
    if v == 0:
        plt.ylim(bottom=0)
    elif v == 1:
        plt.ylim(bottom=0, top=0.17)
    plt.xticks(x, labels=[DIMENSION_NAMES[dimension] for dimension in dimensions])
    plt.xlabel('Matrix configuration')
    plt.title('Approximate energy consumption')
    plt.legend()
    f.savefig(directory / f"energy_{v}_plot.pdf")

def change(a, b):
    return ((a - b) / b) * 100

def format_measurement(m):
    if m < 10 ** -4:
        return rf'{m / 10 ** -6:.2f} \textmu '
    elif m < 10 ** -1:
        return f'{m / 10 ** -3:.2f} m'
    elif m > 10 ** 2:
        return f'{m / 10 ** 3:.2f} k'
    elif m > 10 ** 5:
        return f'{m / 10 ** 6:.2f} M'
    else:
        return f'{m:.2f} '

def gen_table(versions: list, dimensions: list, data: dict, compare='vitis'):
    table = ''
    newline = '\n' + ' ' * 4
    table += r'\begin{tabu}{|l|[1pt]' + 'r|r|r|r|r|r|r|r|r|r|' * len(dimensions) + '}' + newline
    table += r'\hline' + newline
    table += r'\multirow{3}{*}{Version} & ' + ' & '.join([r'\multicolumn{10}{c|}{Matrix configuration: ' + DIMENSION_NAMES_SIMPLE[d] + '}' for d in dimensions]) + r'\\ \cline{2-' + str(1 + 10 * len(dimensions)) + '}' + newline
    table += '& ' + ' & '.join([r'\multicolumn{2}{c|}{Wall time} & \multicolumn{2}{c|}{Kernel time (phi)} & \multicolumn{2}{c|}{Kernel time (spmat)} & \multicolumn{2}{c|}{Power usage} & \multicolumn{2}{c|}{Energy usage}' for _ in dimensions]) + r'\\ \cline{2-' + str(1 + 10 * len(dimensions)) + '}' + newline
    table += '& ' + ' & '.join(['& '.join([r'\multicolumn{1}{c|}{value} & \multicolumn{1}{c|}{change}' for _ in range(5)]) for _ in dimensions]) + r'\\ \tabucline[1pt]{-}' + newline
    for version in versions:
        table += f"{VERSION_NAMES[version]} & "
        values = []
        for dim in dimensions:
            baseline = data[compare][dim]
            current = data[version][dim]
            values.append(rf"{format_measurement(current['walltime']['mean'])}s $\pm$ {format_measurement(current['walltime']['std'])}s")
            percentage = change(current['walltime']['mean'], baseline['walltime']['mean'])
            if percentage == 0:
                    values.append(rf"0.0\%")
            elif percentage > 0:
                values.append(rf"\textcolor{{red}}{{+{percentage:.1f}\%}}")
            else:
                values.append(rf"\textcolor{{posgreen}}{{{percentage:.1f}\%}}")

            if 'kerneltime' in current:
                values.append(rf"{format_measurement(current['kerneltime']['phi']['mean'])}s $\pm$ {format_measurement(current['kerneltime']['phi']['std'])}s")
                percentage = change(current['kerneltime']['phi']['mean'], baseline['kerneltime']['phi']['mean'])
                if percentage == 0:
                    values.append(rf"0.0\%")
                elif percentage > 0:
                    values.append(rf"\textcolor{{red}}{{+{percentage:.1f}\%}}")
                else:
                    values.append(rf"\textcolor{{posgreen}}{{{percentage:.1f}\%}}")
                values.append(rf"{format_measurement(current['kerneltime']['spmat']['mean'])}s $\pm$ {format_measurement(current['kerneltime']['spmat']['std'])}s")
                percentage = change(current['kerneltime']['spmat']['mean'], baseline['kerneltime']['spmat']['mean'])
                if percentage == 0:
                    values.append(rf"0.0\%")
                elif percentage > 0:
                    values.append(rf"\textcolor{{red}}{{+{percentage:.1f}\%}}")
                else:
                    values.append(rf"\textcolor{{posgreen}}{{{percentage:.1f}\%}}")
            else:
                values.extend(['N/A', '-'])
                values.extend(['N/A', '-'])

            values.append(rf"{format_measurement(current['power']['mean'])}W $\pm$ {format_measurement(current['power']['std'])}W")
            percentage = change(current['power']['mean'], baseline['power']['mean'])
            if percentage == 0:
                    values.append(rf"0.0\%")
            elif percentage > 0:
                values.append(rf"\textcolor{{red}}{{+{percentage:.1f}\%}}")
            else:
                values.append(rf"\textcolor{{posgreen}}{{{percentage:.1f}\%}}")

            cmean, cstd = calc_energy(current['power']['mean'], current['power']['std'], current['walltime']['mean'], current['walltime']['std'])
            bmean, _bstd = calc_energy(baseline['power']['mean'], baseline['power']['std'], baseline['walltime']['mean'], baseline['walltime']['std'])

            values.append(rf"{format_measurement(cmean)}Wh $\pm$ {format_measurement(cstd)}Wh")
            percentage = change(cmean, bmean)
            if percentage == 0:
                    values.append(rf"0.0\%")
            elif percentage > 0:
                values.append(rf"\textcolor{{red}}{{+{percentage:.1f}\%}}")
            else:
                values.append(rf"\textcolor{{posgreen}}{{{percentage:.1f}\%}}")

        table += ' & '.join(values) + r'\\ \hline' + newline
    table = table[:-4]
    newline = '\n'
    table += r'\end{tabu}' + newline

    return table


if __name__ == '__main__':
    from matplotlib import rc
    from matplotlib import rcParams
    font_size = 7
    # font_size = 4
    rc('font', **{'family': 'serif', 'serif': ['Computer Modern'], 'size': font_size})
    # rc('font', **{'size': font_size})
    rc('text', usetex=True)
    rc('text.latex', preamble=r'\usepackage{amsmath}')
    # rc('hatch', linewidth=0.1)
    # rc('text.latex', preamble=r'\renewcommand{\familydefault}{\sfdefault} \usepackage{sansmath} \sansmath \usepackage[math]{blindtext}\usepackage{amsmath}')
    rcParams.update({'figure.autolayout': True})

    root = Path(__file__).resolve().parent
    output = root / 'experiment_data'
    versions = ['vexcl', 'vitis', 'vitis_opt1']
    dimensions = [(512, 512, 0.01), (5000, 5000, 0.01),
                  (100000, 100000, 0.01), (12500, 12500, 0.64),
                  (1000000, 1000000, 0.0001)]
    dims_1 = [(512, 512, 0.01), (5000, 5000, 0.01)]
    dims_2 = [(100000, 100000, 0.01), (12500, 12500, 0.64), (1000000, 1000000, 0.0001)]

    dims_1_alt = [(512, 512, 0.01), (5000, 5000, 0.01), (12500, 12500, 0.64)]
    dims_2_alt = [(100000, 100000, 0.01), (1000000, 1000000, 0.0001)]

    data = get_all_performance_data(root / 'experiment_data', versions, dimensions)
    output.mkdir(exist_ok=True)

    plot_power_plot(versions, dimensions, data, output)
    for i, dims in enumerate((dims_1, dims_2)):
        plot_walltime_plot(versions, dims, data, i, output)
        plot_kerneltime_plot([version for version in versions if version != 'vexcl'], dims, data, 'spmat', i, output)
        plot_energy_plot(versions, dims, data, i, output)

    for i, dims in enumerate((dims_1_alt, dims_2_alt)):
        plot_kerneltime_plot([version for version in versions if version != 'vexcl'], dims, data, 'phi', i, output)

    for dim in dimensions:
        table = gen_table(versions, [dim], data)
        with (output / f"spmat_{dim[0]}x{dim[1]}_{dim[2]}_table.tex").open('w') as f:
            print(TABLE_PREAMBLE, end='', file=f)
            print(table, end='', file=f)
