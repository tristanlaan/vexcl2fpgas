# From VexCL to FPGAs
This repository contains my BSc project for the BSc Informatica at the
University of Amsterdam, namely creating a systematic guide for porting VexCL
applications to Xilinx Vitis. Both the code and the thesis itself are included.

## Contents
- [projectproposal.pdf](projectproposal.pdf) \
  The proposal for the bachelor project.
- [thesis/thesis.pdf](thesis/thesis.pdf) \
  The bachelor thesis.
- [vadd/](vadd/) \
  Porting example of a simple vector add application.
- [case_study_affine_transform/](case_study_affine_transform/) \
  Code used for the first case study in the thesis, an affine transformation
  application.
- [case_study_sparse_matrix/](case_study_sparse_matrix/) \
  Code used for the second case study in the thesis, a SpMV application.
