#include <iostream>
#include <fstream>

#include "ap_fixed.h"

typedef ap_fixed<4, 2, AP_TRN, AP_WRAP, 0> fix_t;
typedef ap_fixed<4, 2, AP_TRN, AP_WRAP, 1> bfix_t;

int main() {
    std::ofstream file;

    file.open("data.txt", std::ios::out | std::ios::trunc);

    for (int i = -8 * 4; i <= 8 * 4; ++i) {
        double v = i / 4.0;
        fix_t t1 = v;
        bfix_t t2 = v;

        file << v << "; " << t1 << "; " << t2 << std::endl;
    }

    file.close();

    return 0;
}
