#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt

def plot(file):
    data = np.genfromtxt(file, delimiter=';')
    f = plt.figure(figsize=(5.7885, 4.341375))
    ax = plt.gca()
    plt.scatter(data[:, 0], data[:, 1], marker='_', label='\\texttt{ap\\_fixed<4, 2, AP\\_TRN, AP\\_WRAP, 0>}')
    plt.scatter(data[:, 0], data[:, 2], marker='|', label='\\texttt{ap\\_fixed<4, 2, AP\\_TRN, AP\\_WRAP, 1>}')
    ax.axhline(y=0, linewidth=0.5, alpha=.7, color='k')
    ax.axvline(x=0, linewidth=0.5, alpha=.7, color='k')
    plt.xlabel("Input value")
    plt.ylabel("Value representation")
    plt.ylim(-4, 4)
    plt.legend()
    f.savefig(f"fixed_point_plot.pdf", bbox_inches='tight')


if __name__ == '__main__':
    from matplotlib import rc
    rc('font', **{'family': 'serif', 'serif': ['Computer Modern'], 'size': 10})
    rc('text', usetex=True)
    plot('data.txt')
