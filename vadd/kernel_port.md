# Porting process kernel
<sup>[home](README.md) · [host porting process](host_port.md)</sup>

1. Copy OpenCL code.
2. Encapsulate code in `extern "C"` block.
3. Replace OpenCL types.
    - `kernel` → ε
    - `global` → `const` / ε[^1]
    - `ulong`/`long`/`size_t` → `int`
4. Replace OpenCL functions.
    - `get_global_id(0)` → 0
    - `get_global_size(0)` → 1
5. Add memory pragmas.
    - `#pragma HLS INTERFACE m_axi port=`_prm_` bundle=aximm`_\<memory interface No.\>_
6. Create configuration file.

[^1]: `const` if input buffer, else ε.
