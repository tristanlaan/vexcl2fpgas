#if defined(cl_khr_fp64)
#  pragma OPENCL EXTENSION cl_khr_fp64: enable
#elif defined(cl_amd_fp64)
#  pragma OPENCL EXTENSION cl_amd_fp64: enable
#endif

kernel void vexcl_vector_kernel
(
  ulong n,
  global double * prm_1,
  global double * prm_2,
  global double * prm_3
)
{
  for(ulong idx = get_global_id(0); idx < n; idx += get_global_size(0))
  {
    prm_1[idx] = ( prm_2[idx] + prm_3[idx] );
  }
}
