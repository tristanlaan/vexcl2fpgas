#include <iostream>
#include <iomanip>
#include <vector>

#define CL_TARGET_OPENCL_VERSION 120

#include <vexcl/vexcl.hpp>

template <typename Vec>
void initialize_input(Vec &a, Vec &b) {
    size_t n = a.size();

    for (size_t i = 0; i < n; ++i) {
        a[i] = (double) ((40 - i * (i - 1)) % 64);
    }

    for (size_t i = 0; i < n; ++i) {
        b[i] = (double) (i % 64);
    }
}

template <typename Vec>
void print_results(Vec &a, Vec &b, Vec &c) {
    size_t n = c.size();

    std::cerr << "a         = [";
    for (size_t i = 0; i < n; ++i) {
        std::cerr << std::setw(2) << a[i];
        if (i + 1 < n) std::cerr << ", ";
    }
    std::cerr << "]" << std::endl;

    std::cerr << "b         = [";
    for (size_t i = 0; i < n; ++i) {
        std::cerr << std::setw(2) << b[i];
        if (i + 1 < n) std::cerr << ", ";
    }
    std::cerr << "]" << std::endl;

    std::cerr << "c = a + b = [";
    for (size_t i = 0; i < n; ++i) {
        std::cerr << std::setw(2) << c[i];
        if (i + 1 < n) std::cerr << ", ";
    }
    std::cerr << "]" <<  std::endl;
}

int main(int argc, char **argv) {
    vex::Context ctx(vex::Filter::DoublePrecision);

    size_t n = 10;
    std::vector<double> a(n), b(n), c(n);
    initialize_input(a, b);

    vex::vector<cl_double> A(ctx, n, reinterpret_cast<cl_double*>(a.data()));
    vex::vector<cl_double> B(ctx, n, reinterpret_cast<cl_double*>(b.data()));
    vex::vector<cl_double> C(ctx, n);

    C = A + B;

    vex::copy(C.begin(), C.end(), reinterpret_cast<cl_double*>(c.data()));

    print_results(a, b, c);

    return EXIT_SUCCESS;
}
