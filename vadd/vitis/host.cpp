#include <iostream>
#include <iomanip>
#include <vector>

// #define CL_TARGET_OPENCL_VERSION 120
// #include <vexcl/vexcl.hpp>
#include "xcl2.hpp"

template <typename Vec>
void initialize_input(Vec &a, Vec &b) {
    size_t n = a.size();

    for (size_t i = 0; i < n; ++i) {
        a[i] = (double) ((40 - i * (i - 1)) % 64);
    }

    for (size_t i = 0; i < n; ++i) {
        b[i] = (double) (i % 64);
    }
}

template <typename Vec>
void print_results(Vec &a, Vec &b, Vec &c) {
    size_t n = c.size();

    std::cerr << "a         = [";
    for (size_t i = 0; i < n; ++i) {
        std::cerr << std::setw(2) << a[i];
        if (i + 1 < n) std::cerr << ", ";
    }
    std::cerr << "]" << std::endl;

    std::cerr << "b         = [";
    for (size_t i = 0; i < n; ++i) {
        std::cerr << std::setw(2) << b[i];
        if (i + 1 < n) std::cerr << ", ";
    }
    std::cerr << "]" << std::endl;

    std::cerr << "c = a + b = [";
    for (size_t i = 0; i < n; ++i) {
        std::cerr << std::setw(2) << c[i];
        if (i + 1 < n) std::cerr << ", ";
    }
    std::cerr << "]" <<  std::endl;
}

int main(int argc, char **argv) {
    // vex::Context ctx(vex::Filter::DoublePrecision);
    cl_int err;
    std::string binaryFile = "vadd.xclbin";
    std::vector<cl::Device> devices = xcl::get_xilinx_devices();
    const cl::Device device = devices.front();
    cl::Context context(device, NULL, NULL, NULL, &err);
    std::vector<unsigned char> fileBuf = xcl::read_binary_file(binaryFile);
    cl::Program::Binaries bins{{reinterpret_cast<char*>(fileBuf.data()), fileBuf.size()}};
    cl::Program program(context, devices, bins, NULL, &err);
    cl::CommandQueue q(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
    cl::Kernel krnl_vadd(program, "vadd", &err);

    size_t n = 10;
    // std::vector<double> a(n), b(n), c(n);
    std::vector<double, aligned_allocator<double>> a(n), b(n), c(n);
    initialize_input(a, b);

    // vex::vector<cl_double> A(ctx, n, reinterpret_cast<cl_double*>(a.data()));
    // vex::vector<cl_double> B(ctx, n, reinterpret_cast<cl_double*>(b.data()));
    // vex::vector<cl_double> C(ctx, n);
    cl::Buffer A(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(double) * n, a.data(), &err);
    cl::Buffer B(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(double) * n, b.data(), &err);
    cl::Buffer C(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, sizeof(double) * n, c.data(), &err);

    // C = A + B;
    krnl_vadd.setArg(0, n);
    krnl_vadd.setArg(1, C);
    krnl_vadd.setArg(2, A);
    krnl_vadd.setArg(3, B);

    q.enqueueMigrateMemObjects({A, B}, 0);
    q.enqueueTask(krnl_vadd);
    // vex::copy(C.begin(), C.end(), reinterpret_cast<cl_double*>(c.data()));
    q.enqueueMigrateMemObjects({C}, CL_MIGRATE_MEM_OBJECT_HOST);
    q.finish();

    print_results(a, b, c);

    return EXIT_SUCCESS;
}
