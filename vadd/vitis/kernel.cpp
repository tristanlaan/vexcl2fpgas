extern "C" {
    void vadd
    (
    unsigned long n,
    double * prm_1,
    const double * prm_2,
    const double * prm_3
    )
    {
    #pragma HLS INTERFACE m_axi port=prm_1 bundle=aximm1
    #pragma HLS INTERFACE m_axi port=prm_2 bundle=aximm2
    #pragma HLS INTERFACE m_axi port=prm_3 bundle=aximm3
    for(unsigned long idx = 0; idx < n; idx += 1)
    {
        prm_1[idx] = ( prm_2[idx] + prm_3[idx] );
    }
    }
}
