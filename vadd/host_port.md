# Porting process host
<sup>[home](README.md) · [kernel porting process](kernel_port.md)</sup>

Exact code changes with original code in comments are described in [vitis/host.cpp](vitis/host.cpp).

1. Copy VexCL code.
2. Replace includes.
3. Add device loading code.
4. Use aligned allocator.
5. Replace buffer creation.
6. Replace computation with kernel arguments.
7. Add execution code.
