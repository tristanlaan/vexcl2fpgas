# Vector add demo
Demo of porting a VexCL vector add application to Xilinx Vitis. The vexcl directory contains the VexCL implementation, and the vitis directory contain the Xilinx Vitis implementation.

To prepare the porting process we must first retrieve the OpenCL kernel from the VexCL implementation, this can be retrieved by running the application with the environment variable `VEXCL_SHOW_KERNELS` defined. The rest of the porting process is described here:
- [Porting process of kernel](kernel_port.md)
- [Porting process of host](host_port.md)
