\chapter{Affine transformation}
\section{Program description}
The program we will use for our case-study is a simple affine transformation implementation. It will calculate the expression given in equation~\ref{eq:affine_transform}.

\begin{equation}\label{eq:affine_transform}
    \vec{t} = \vec{y} + A\vec{x} =
    \begin{pmatrix}0 \\ 1 \\ 2 \\ 3 \\ 4 \\ 5 \\ 6\end{pmatrix} +
    \begin{pmatrix}
        1 & -1 & -2 & -3 & -4\\
        1 & 1 & -1 & -2 & -3\\
        2 & 1 & 1 & -1 & -2\\
        3 & 2 & 1 & 1 & -1\\
        4 & 3 & 2 & 1 & 1\\
        5 & 4 & 3 & 2 & 1\\
        6 & 5 & 4 & 3 & 2
    \end{pmatrix}
    \begin{pmatrix}5 \\ 4 \\ 3 \\ 2 \\ 1\end{pmatrix} =
    \begin{pmatrix}-15 \\ 0 \\ 15 \\ 30 \\ 45 \\ 60 \\ 76\end{pmatrix}
\end{equation}

\section{VexCL implementation}
To start writing a VexCL program we need some small boilerplate code first, as can be seen in listing~\ref{lst:vexcl_boilerplate}. The code simply includes the VexCL library and specifies that we want to use double precision math on our accelerator. We also set the \texttt{VEXCL\_SHOW\_KERNELS} flag because we want to see the OpenCL kernel that VexCL uses.

\begin{lstfloat}
\begin{cppcode}
#define CL_TARGET_OPENCL_VERSION 120
#define VEXCL_SHOW_KERNELS
#include <vexcl/vexcl.hpp>

int main(int argc, char **argv) {
    vex::Context ctx(vex::Filter::DoublePrecision);
    ...
    return 0;
}
\end{cppcode}
\caption{boilerplate VexCL code}\label{lst:vexcl_boilerplate}
\end{lstfloat}

Now we need to initialize the input data of our equation, which can be seen in listing~\ref{lst:vexcl_vector_initialization}. We first need to create the input data on the host device, and then the data can be copied to the accelerator device. For the host side vectors we can simply use the std::vector implementation of C++. We implement the matrix A as flattened vector, because VexCL doesn't support dense matrices without external libraries. Once the host vectors are initialized we can specify the device-side vectors using vex::vector and copy the host-side data into the vectors. We have to cast the doubles of the host-side vectors to cl\_doubles for the device-side vectors. Note that we leave the output vector $\vec{t}$  uninitialized on the accelerator, because the data in the vector will later be overwritten by the result of the equation.
\begin{lstfloat}
\begin{cppcode}
    size_t m = 7, n = 5;
    std::vector<double> a(m * n), x(n), y(m), t(m);

    // Initialize matrix + vectors
    ...

    // Transfer host-side doubles into device-side cl_double vectors
    vex::vector<cl_double> A(ctx, a.size(), reinterpret_cast<cl_double*>(a.data()));
    vex::vector<cl_double> X(ctx, x.size(), reinterpret_cast<cl_double*>(x.data()));
    vex::vector<cl_double> Y(ctx, y.size(), reinterpret_cast<cl_double*>(y.data()));
    vex::vector<cl_double> T(ctx, t.size();
\end{cppcode}
\caption{VexCL data initialization}\label{lst:vexcl_vector_initialization}
\end{lstfloat}

Once the vectors are initialized we can start calculating the equation. The relevant code can be seen in listing~\ref{lst:vexcl_calculation}. The addition is very simple as VexCL simply overloads the addition operator to support device-side vector addition. VexCL does not support matrix multiplication out of the box however, so we have to implement this ourselves with VexCL commands. To perform the matrix vector multiplication $A\vec{x}$, we extent the vector $\vec{x}$ to a $m\times n$ ($7 \times 5$) matrix $X$ by repeating $\vec{x}^\intercal$ for each row of $X$. Then we will perform an elementwise multiplication between $A$ and $X$ and reduce the result to a vector by summing the columns of the result together. Note that all the matrices are still implemented as flattened vectors, but the reshape and reduce functions of VexCL behave like the arguments are matrices and will handle the index conversion for us.

\begin{lstfloat}
\begin{cppcode}
template <class M, class V>
auto prod(size_t m, size_t n, M &&A, V &&x) {
    using namespace vex;
    // Specify M×N matrix shape.
    auto MxN = extents[m][n];
    // Reshape x to a matrix by copying x into each row of X.
    auto X = reshape(x, MxN, extents[1]);
    // Multiply A with X elementwise.
    auto E = A * X;
    // Reduce matrix E to a vector of size M by summing over dimension 1.
    return reduce<SUM>(MxN, E, 1);
}

int main(int argc, char **argv) {
    ...
    T = Y + prod(m, n, A, X);
    ...
}
\end{cppcode}
\caption{VexCL performing equation}\label{lst:vexcl_calculation}
\end{lstfloat}

When the calculations are finished, we can copy the results back to the host-device vector $\vec{t}$, as can be seen in listing~\ref{lst:vexcl_results}. Note that we have to cast the cl\_doubles back to normal doubles.

\begin{lstfloat}
\begin{cppcode}
    vex::copy(T.begin(), T.end(), reinterpret_cast<cl_double*>(t.data()));
\end{cppcode}
\caption{VexCL copying back results}\label{lst:vexcl_results}
\end{lstfloat}

\section{VexCL kernel}\label{sec:vexcl_kernel}
Because we set the \texttt{VEXCL\_SHOW\_KERNELS} flag in our program, VexCL will output the OpenCL kernels it produced, which we can use to port the application to Xilinx Vitis. The produced kernel can be seen in listing~\ref{lst:vexcl_kernel}. At the top of the file we have two pragmas that enable the double precision floating point numbers on supported hardware. Then we find an automatically generated function to sum two doubles. The actual kernel is generated as the kernel function vexcl\_vector\_kernel with 14 parameters. The first parameter n is the size of the output vector $\vec{t}$, which was equal to $m=7$. The other parameters all follow the same naming scheme prm\_?, which are named in which order they appear in the equation $\vec{t} = \vec{y} + A\vec{x}$. So the parameter prm\_1 corresponds to $\vec{t}$, the parameter prm\_2 corresponds to $\vec{y}$, the variables prm\_3\_* are part of the multiplication $A\vec{x}$, the parameter prm\_3\_1 corresponds to $A$ and the parameter prm\_3\_2 corresponds to $\vec{x}$.

Let us now take a look at the last five parameters, which are generated by the \texttt{vex::reduce} command, prm\_3\_start, prm\_3\_length0, prm\_3\_stride0, prm\_3\_length1, prm\_3\_stride1. The parameters prm\_3\_length0 and prm\_3\_stride0 are the size of the first and second dimension of the input matrix respectively, so in our case $\text{prm\_3\_length0} = m$ and  $\text{prm\_3\_length1} = n$. The parameter prm\_3\_length1 corresponds to how many values need to be summed together and the parameter prm\_3\_stride1 tells us how many elements we have to skip in the underlying array of the input matrix to get to the next value to be summed. Because we are summing over the columns, $\text{prm\_3\_length1} = n$ and $\text{prm\_3\_stride1} = 1$. Lastly prm\_3\_start is a global offset in the array, which we do not need so $\text{prm\_3\_start} = 0$.

We are now only left with the parameters prm\_3\_2\_slice\_1, prm\_3\_2\_slice\_2, prm\_3\_2\_slice\_3 and prm\_3\_2\_slice\_4, which are generated by the \texttt{vex::reshape} command. These parameters are used to convert a index into the flattened matrix $X$ to an index into the vector $\vec{x}$, and are easier to understand if we look to the code where they are used. In the expression \texttt{(prm\_3\_2\_slice\_1 * (((prm\_3\_2\_slice\_2 + idx) / prm\_3\_2\_slice\_3) \% prm\_3\_2\_slice\_4))}, idx is the index in the flattened matrix and the outcome of the expression is the index in the vector $\vec{x}$. If we start from idx we see that a global offset prm\_3\_2\_slice\_2 is added, this is only used if you slice an array, not when you reshape it, so in our case $\text{prm\_3\_2\_slice\_2} = 0$. The next operation in the expression is an integer division. This division is used to repeat the several index multiple times and is useful to replicate a vector along the columns of a matrix, but that is not necessary in our case, so $\text{prm\_3\_2\_slice\_1} = 1$. Then we see a modulo operation with prm\_3\_2\_slice\_4, which is used to wrap around the index. In our case we wanted to replicate the vector along the rows of a matrix, so at each row of the matrix we want to start at index 0 again. This means $\text{prm\_3\_2\_slice\_4} = |\vec{x}| = n$. Lastly we have a multiplication by prm\_3\_2\_slice\_1, which can be used to skip every $i$th index if set to $i$. That is not needed in our case so $\text{prm\_3\_2\_slice\_1} = 1$. The expression \texttt{(prm\_3\_2\_slice\_1 * (((prm\_3\_2\_slice\_2 + idx) / prm\_3\_2\_slice\_3) \% prm\_3\_2\_slice\_4))} thus simplifies to $\text{idx} \mod n$ in our case.

Now that we have defined all the parameters we can see that the kernel exists of a parallelized loop that runs over the elements of $\vec{t}$, which has an inner loop to calculate $A\vec{x}$, then adds the sum to $\vec{y}$ and stores the result in $\vec{t}$.

%VexCL can only show the produced kernel, and not how the kernel is called from the host-side, so we need to find the values corresponding to the parameters ourselves by looking at the generated code.

% The first thing we see is that the whole function exists within a for-loop. This for-loop will be parallelized on the accelerator, and runs from 0 to n. Note that this variable n does not correspond with our own variable $n$, and is in fact the size of the output vector $\vec{t}$, which was $m=7$. So the first parameter corresponds to $m$.

% Next up we have a more complicated code-block with a for-loop. This code-block calculates the matrix multiplication $A\vec{x}$. We can see that at the start of the code-block prm\_3\_sum is initialized. This stores the row idx of the result from the matrix multiplication, because we are still in a for loop that iterates over the rows of $\vec{t}$. Now we find the line \texttt{size\_t ptr1 = prm\_3\_start + (pos \% prm\_3\_length0) * prm\_3\_stride0}. This calculates at what index the row idx starts in the flattened matrix $A$. prm\_3\_start is a global offset in the array which is 0 in our case, pos is the current row which is equal to idx, prm\_3\_length0 is the height of the matrix which is $m$ and lastly prm\_3\_stride0 is the width of the matrix which is $n$. So this expression simplifies to $\text{ptr1} = \text{idx} \times n$.

% In the last line of code in the for-loop we find a familiar line of code: \texttt{prm\_1[idx] = (prm\_2[idx] + prm\_3\_sum)}. This is the addition of the vector $\vec{y}$ and the result of the multiplication $A\vec{x}$, which is assigned to the vector $\vec{t}$. Thus the second parameter prm\_1 is $\vec{t}$, the third parameter prm\_2 is $\vec{y}$. Now we have found all the corresponding parameters.

\begin{lstfloat}
\begin{cppcode}
#if defined(cl_khr_fp64)
#  pragma OPENCL EXTENSION cl_khr_fp64: enable
#elif defined(cl_amd_fp64)
#  pragma OPENCL EXTENSION cl_amd_fp64: enable
#endif

double SUM_double(double prm1, double prm2) {
  return prm1 + prm2;
}

kernel void vexcl_vector_kernel(ulong n, global double *prm_1,
  global double *prm_2, global double *prm_3_1, global double *prm_3_2_expr_1,
  ulong prm_3_2_slice_1, ulong prm_3_2_slice_2, ulong prm_3_2_slice_3,
  ulong prm_3_2_slice_4, ulong prm_3_start, ulong prm_3_length0,
  long prm_3_stride0, ulong prm_3_length1, long prm_3_stride1)
{
  for (ulong idx = get_global_id(0); idx < n; idx += get_global_size(0)) {
    double prm_3_sum = 0;
    {
      size_t pos = idx;
      size_t ptr1 = prm_3_start + (pos % prm_3_length0) * prm_3_stride0;
      for (size_t i1 = 0, ptr2 = ptr1; i1 < prm_3_length1; ++i1,
           ptr2 += prm_3_stride1) {
        size_t idx = ptr2;
        prm_3_sum = SUM_double(prm_3_sum,
          (prm_3_1[idx] * prm_3_2_expr_1[(prm_3_2_slice_1 * (
          ((prm_3_2_slice_2 + idx) / prm_3_2_slice_3) % prm_3_2_slice_4))]));
      }
    }
    prm_1[idx] = (prm_2[idx] + prm_3_sum);
  }
}
\end{cppcode}
\caption{OpenCL kernel produced by VexCL}\label{lst:vexcl_kernel}
\end{lstfloat}

\section{Xilinx Vitis implementation}
The Vitis implementation of the affine transformation needs two different files, where only one file is needed with VexCL, a file with the host code and a file with the device code.

\subsection{Device code}
For the device code we can mostly copy the kernel produced by VexCL with little changes, as is visible in listing~\ref{lst:vitis_kernel}. First we need to wrap the code in a \texttt{extern "C"} block to avoid name mangling issues between C and C++ \cite{Xilinxaccelarationmanual}. We also need to change some of the data types from OpenCL types to C types, so we remove the \texttt{kernel} and \texttt{global} keyword and replace the \texttt{ulong}, \texttt{long} and \texttt{size\_t} keywords with \texttt{int}. Then we specify the read-only buffers as \texttt{const} and rename the kernel to be more easily distinguishable. Lastly we need to add pragmas for the vectors to specify which memory interface the \ac{fpga} should use. We want to use different interfaces for data that is needed at the same time, so that the data can be loaded concurrently.

\begin{lstfloat}
\begin{cppcode}
extern "C" {
  // Unchanged defines
  ...

  double SUM_double( double prm1, double prm2) {
    return prm1 + prm2;
  }

  void affinetrans(int n, double *prm_1, const double *prm_2,
    const double *prm_3_1, const double *prm_3_2_expr_1, int prm_3_2_slice_1,
    int prm_3_2_slice_2, int prm_3_2_slice_3, int prm_3_2_slice_4,
    int prm_3_start, int prm_3_length0, int prm_3_stride0, int prm_3_length1,
    int prm_3_stride1)
  {
    #pragma HLS INTERFACE m_axi port=prm_1 bundle=aximm1
    #pragma HLS INTERFACE m_axi port=prm_2 bundle=aximm1
    #pragma HLS INTERFACE m_axi port=prm_3_1 bundle=aximm1
    #pragma HLS INTERFACE m_axi port=prm_3_2_expr_1 bundle=aximm2

    // Unchanged computations
    ...
}
\end{cppcode}
\caption{Shortened Xilinx Vitis kernel that can run on a \ac{fpga}}\label{lst:vitis_kernel}
\end{lstfloat}

\subsection{Host code}
The first step in creating the Vitis host code is to replace the boilerplate VexCL code with boilerplate code that enables a connection with the \ac{fpga}. As can be seen in listing~\ref{lst:vitis_boilerplate}, the defines change a bit and instead of including VexCL, we include OpenCL. Setting up the context is also a bit different, where we only needed a single command for VexCL, we now need to write our own code to connect to the \ac{fpga} and to load the OpenCL kernel.

\begin{lstfloat}
\begin{cppcode}
#define CL_HPP_CL_1_2_DEFAULT_BUILD
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#define CL_HPP_ENABLE_PROGRAM_CONSTRUCTION_FROM_ARRAY_COMPATIBILITY 1
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl2.hpp>
...
int main(int argc, char **argv) {
    // Get Xilinx device
    const cl::Device device = get_xilinx_devices().front();
    ...
    // Load OpenCL kernel
    cl::CommandQueue q(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
    cl::Kernel krnl_affine_transform(program, "affinetrans", &err);
    ...
    return 0;
}
\end{cppcode}
\caption{Shortened boilerplate Xilinx Vitis code}\label{lst:vitis_boilerplate}
\end{lstfloat}

The declaration and initialization of the host vectors is the same in the Vitis implementation as it was in the VexCL implementation, but copying the data to the accelerator does change a bit, as shown in listing~\ref{lst:vitis_buffer}. Instead of creating \texttt{vex::vector} objects, we need to create \texttt{cl::Buffer} objects and specify that we want to copy the data from a host pointer and if the data will be readable, writable or both.

\begin{lstfloat}
\begin{cppcode}
    std::vector<double> a(m * n), x(n), y(m), t(m);

    // Initialize matrix + vectors
    ...

    // Create the buffers and allocate memory
    cl::Buffer A(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
                 sizeof(double) * a.size(), a.data(), &err);
    cl::Buffer X(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
                 sizeof(double) * x.size(), x.data(), &err);
    cl::Buffer Y(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
                 sizeof(double) * y.size(), y.data(), &err);
    cl::Buffer T(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
                 sizeof(double) * t.size(), t.data(), &err);
\end{cppcode}
\caption{Data initialization in Xilinx Vitis}\label{lst:vitis_buffer}
\end{lstfloat}

We can now remove the VexCL vector computations, as that is already done in the \ac{fpga} kernel. We do however need to call the kernel ourself in the Vitis implementation. To do this we first need to set the arguments of the kernel. In section~\ref{sec:vexcl_kernel} we already specified which values corresponded to the arguments, so we can simply set the arguments to those values. In listing~\ref{lst:vitis_kernel_call} we set the arguments to the kernel and execute the kernel. We however first need to transfer the input buffers to the kernel and after the kernel is finished, we can transfer the output buffer back to the host device.

\begin{lstfloat}
\begin{cppcode}
    // Set kernel arguments
    krnl_affine_transform.setArg(0, (int) m);
    krnl_affine_transform.setArg(1, T);
    krnl_affine_transform.setArg(2, Y);
    krnl_affine_transform.setArg(3, A);
    krnl_affine_transform.setArg(4, X);
    krnl_affine_transform.setArg(5, 1);
    krnl_affine_transform.setArg(6, 0);
    krnl_affine_transform.setArg(7, 1);
    krnl_affine_transform.setArg(8, (int) n);
    krnl_affine_transform.setArg(9, 0);
    krnl_affine_transform.setArg(10, (int) m);
    krnl_affine_transform.setArg(11, (int) n);
    krnl_affine_transform.setArg(12, (int) n);
    krnl_affine_transform.setArg(13, 1);

    // Schedule transfer of inputs to device memory, execution of kernel,
    // and transfer of outputs back to host memory
    q.enqueueMigrateMemObjects({Y, A, X}, 0);
    q.enqueueTask(krnl_affine_transform);
    q.enqueueMigrateMemObjects({T}, CL_MIGRATE_MEM_OBJECT_HOST);

    // Wait for all scheduled operations to finish
    q.finish();
\end{cppcode}
\caption{Calling kernel in Xilinx Vitis}\label{lst:vitis_kernel_call}
\end{lstfloat}

\section{Summary}
Both programs give the correct output, so we have correctly ported a VexCL program to Xilinx Vitis. The steps needed to convert the VexCL code to Vitis code is shown in table~\ref{tab:vexcl_vitis_host} for the host code and table~\ref{tab:vexcl_vitis_kernel} for the kernel.

\begin{table}
    \centering
    \footnotesize
    \begin{tabularx}{\textwidth}{|L{0.75}|L{1.25}|}
        \hline
        \textbf{VexCL} & \textbf{Xilinx Vitis} \\
        \hline
        \multirow{5}{*}{ctx(vex::Filter::DoublePrecision);} & device = get\_xilinx\_devices().front(); \\
        & \dots \\
        & cl::CommandQueue q(context, device, CL\_QUEUE\_PROFILING\_ENABLE, \&err); \\
        & cl::Kernel \textit{krnl\_name}(program, \textit{kernel\_function\_name}, \&err); \\
        \hline
        vex::vector<cl\_type> \textit{A}(ctx, \textit{a}.size(), \textit{a}.data()); & cl::Buffer \textit{A}(context, CL\_MEM\_READ\_ONLY | CL\_MEM\_USE\_HOST\_PTR, sizeof(type) * \textit{a}.size(), \textit{a}.data(), \&err); \\
        vex::vector<cl\_type> \textit{T}(ctx, \textit{t}.size()); & cl::Buffer \textit{T}(context, CL\_MEM\_WRITE\_ONLY | CL\_MEM\_USE\_HOST\_PTR, sizeof(type) * \textit{t}.size(), \textit{t}.data(), \&err); \\
        ... & \\
        vex::copy(\textit{T}.begin(), \textit{T}.end(), \textit{t}.data()); & \\
        \hline
        \textit{T} = \textit{X} + \textit{Y}; & \textit{krnl\_name}.setArg(0, \textit{output\_vector\_size});\\
        & \textit{krnl\_name}.setArg(1, \textit{T});\\
        & \textit{krnl\_name}.setArg(2, \textit{X}); \\
        & \textit{krnl\_name}.setArg(3, \textit{Y}); \\
        \hline
        reshape(\textit{X}, extents[\textit{m}][\textit{n}], extents[1]) & \textit{krnl\_name}.setArg(0, \textit{output\_vector\_size});\\
        & \textit{krnl\_name}.setArg(1, \textit{X}); \\
        & \textit{krnl\_name}.setArg(2, 1); // skip indices \\
        & \textit{krnl\_name}.setArg(3, 0); // offset \\
        & \textit{krnl\_name}.setArg(4, 1); // repetitions \\
        & \textit{krnl\_name}.setArg(5, \textit{n}); // modulo \\
        \hline
        reduce<OP>(extents[\textit{m}][\textit{n}], \textit{X}, 1) & \textit{krnl\_name}.setArg(0, \textit{output\_vector\_size});\\
        & \textit{krnl\_name}.setArg(1, \textit{X}); \\
        & \textit{krnl\_name}.setArg(2, 0); // offset \\
        & \textit{krnl\_name}.setArg(3, \textit{m}); // first dimension matrix \\
        & \textit{krnl\_name}.setArg(4, \textit{n}); // second dimension matrix \\
        & \textit{krnl\_name}.setArg(5, \textit{n}); // amount of values to reduce each time \\
        & \textit{krnl\_name}.setArg(6, 1); // distance between values \\
        \hline
        // Finished computations & q.enqueueMigrateMemObjects(\textit{input\_buffers}, 0); \\
        & q.enqueueTask(\textit{krnl\_name}); \\
        & q.enqueueMigrateMemObjects(\textit{output\_buffers}, CL\_MIGRATE\_MEM\_OBJECT\_HOST); \\
        & q.finish(); \\
        \hline
    \end{tabularx}
    \caption{Host code changes from VexCL to Xilinx Vitis}
    \label{tab:vexcl_vitis_host}
\end{table}

\begin{table}
    \centering
    \footnotesize
    \begin{tabularx}{\textwidth}{|L{1}|L{1}|}
        \hline
        \textbf{VexCL} & \textbf{Xilinx Vitis} \\
        \hline
        \multirow{3}{*}{// Entire kernel} & extern "C" \{ \\
        & \quad// Entire kernel \\
        & \} \\
        \hline
        kernel void vexcl\_vector\_kernel(\dots) \{\dots\} & void \textit{kernel\_function\_name}(\dots) \{\dots\} \\
        \hline
        ulong / long / size\_t & int \\
        \hline
        \multirow{2}{*}{global \textit{type} *\textit{prm}} & const \textit{type} *\textit{prm} // if \textit{prm} is input buffer \\
        & \textit{type} *\textit{prm} // if \textit{prm} is output buffer \\
        & \dots \\
        & \#pragma HLS INTERFACE m\_axi port=\textit{prm} bundle=aximm\textit{<memory interface No.>} \\
        \hline
    \end{tabularx}
    \caption{Kernel code changes from VexCL to Xilinx Vitis}
    \label{tab:vexcl_vitis_kernel}
\end{table}
