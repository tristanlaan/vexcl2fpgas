\chapter{Porting a VexCL application}\label{ch:affinetrans}
\section{Program description}
The program we will use for our case-study implements a simple affine transformation: it calculates the expression given in equation~\ref{eq:affine_transform}, where $\vec{y}$ and $\vec{t}$ are vectors of length $m$, $\vec{x}$ is a vector of length $n$ and $A$ is a $m {\times} n$ matrix.

\begin{equation}\label{eq:affine_transform}
    \vec{t} = \vec{y} + A\vec{x}
%    =
%    \begin{pmatrix}0 \\ 1 \\ 2 \\ 3 \\ 4 \\ 5 \\ 6\end{pmatrix} +
%    \begin{pmatrix}
%        1 & -1 & -2 & -3 & -4\\
%        1 & 1 & -1 & -2 & -3\\
%        2 & 1 & 1 & -1 & -2\\
%        3 & 2 & 1 & 1 & -1\\
%        4 & 3 & 2 & 1 & 1\\
%        5 & 4 & 3 & 2 & 1\\
%        6 & 5 & 4 & 3 & 2
%    \end{pmatrix}
%    \begin{pmatrix}5 \\ 4 \\ 3 \\ 2 \\ 1\end{pmatrix} =
%    \begin{pmatrix}-15 \\ 0 \\ 15 \\ 30 \\ 45 \\ 60 \\ 76\end{pmatrix}
\end{equation}

\section{VexCL implementation}
To start writing a VexCL program, we need some boilerplate code first, as can be seen in listing~\ref{lst:vexcl_boilerplate}. The code simply includes the VexCL library and specifies that we want to use double precision math on our accelerator. We also set the \texttt{VEXCL\_SHOW\_KERNELS} flag because we want to see the OpenCL kernel that VexCL uses.

\begin{lstfloat}
\begin{cppcode}
#define CL_TARGET_OPENCL_VERSION 120
#define VEXCL_SHOW_KERNELS
#include <vexcl/vexcl.hpp>

int main(int argc, char **argv) {
    vex::Context ctx(vex::Filter::DoublePrecision);
    ...
    return 0;
}
\end{cppcode}
\caption{The boilerplate VexCL code.}\label{lst:vexcl_boilerplate}
\end{lstfloat}

We further need to initialize the input data of our equation, which can be seen in listing~\ref{lst:vexcl_vector_initialization}. We first need to create the input data on the host device, and then the data can be copied to the accelerator device. For the host side vectors we can simply use the std::vector implementation of C++. We implement the matrix A as a flattened vector, because VexCL does not support dense matrices without external libraries. Once the host vectors are initialized, we can specify the device-side vectors using vex::vector and copy the host-side data into the vectors. We have to cast the doubles of the host-side vectors to cl\_doubles for the device-side vectors. Note that we leave the output vector $\vec{t}$  uninitialized on the accelerator, because the data in the vector will later be overwritten by the result of the equation.
\begin{lstfloat}
\begin{cppcode}
    size_t m = 7, n = 5;
    std::vector<double> a(m * n), x(n), y(m), t(m);

    // Initialize matrix + vectors
    ...

    // Transfer host-side doubles into device-side cl_double vectors
    vex::vector<cl_double> A(ctx, a.size(), reinterpret_cast<cl_double*>(a.data()));
    vex::vector<cl_double> X(ctx, x.size(), reinterpret_cast<cl_double*>(x.data()));
    vex::vector<cl_double> Y(ctx, y.size(), reinterpret_cast<cl_double*>(y.data()));
    vex::vector<cl_double> T(ctx, t.size();
\end{cppcode}
\caption{The VexCL data initialization.}\label{lst:vexcl_vector_initialization}
\end{lstfloat}

Once the vectors are initialized we can start calculating the equation. The relevant code can be seen in listing~\ref{lst:vexcl_calculation}. The addition is very simple as VexCL simply overloads the addition operator to support device-side vector addition. VexCL does not support matrix multiplication out of the box however, so we have to implement this ourselves with VexCL commands. To perform the matrix vector multiplication $A\vec{x}$, we extent the vector $\vec{x}$ to a $m\times n$ ($7 \times 5$) matrix $X$ by repeating $\vec{x}^\intercal$ for each row of $X$. Then we will perform an element-wise multiplication between $A$ and $X$ and reduce the result to a vector by summing the columns of the result together. Note that all the matrices are still implemented as flattened vectors, but the \texttt{reshape} and \texttt{reduce} functions of VexCL behave like the arguments are matrices and will handle the index conversion for us.

\begin{lstfloat}
\begin{cppcode}
template <class M, class V>
auto prod(size_t m, size_t n, M &&A, V &&x) {
    using namespace vex;
    // Specify M×N matrix shape.
    auto MxN = extents[m][n];
    // Reshape x to a matrix by copying x into each row of X.
    auto X = reshape(x, MxN, extents[1]);
    // Multiply A with X element-wise.
    auto E = A * X;
    // Reduce matrix E to a vector of size M by summing over dimension 1.
    return reduce<SUM>(MxN, E, 1);
}

int main(int argc, char **argv) {
    ...
    T = Y + prod(m, n, A, X);
    ...
}
\end{cppcode}
\caption{Equation~\ref{eq:affine_transform} calculated in VexCL.}\label{lst:vexcl_calculation}
\end{lstfloat}

When the calculations are finished, we can copy the results back to the host-device vector $\vec{t}$, as can be seen in listing~\ref{lst:vexcl_results}. Note that we have to cast the cl\_doubles back to normal doubles.

\begin{lstfloat}
\begin{cppcode}
    vex::copy(T.begin(), T.end(), reinterpret_cast<cl_double*>(t.data()));
\end{cppcode}
\caption{VexCL copying back the results.}\label{lst:vexcl_results}
\end{lstfloat}

\section{VexCL kernel}\label{sec:vexcl_kernel}
\begin{table}
    \centering
    \begin{tabular}{l|l}
        \textbf{Original parameter name} & \textbf{Reference name} \\
        \hline
        \texttt{n} & parameter 0 \\
        \texttt{prm\_1} & buffer 1 \\
        \texttt{prm\_2} & buffer 2 \\
        \texttt{prm\_3\_1} & buffer 3 \\
        \texttt{prm\_3\_2\_expr\_1} & buffer 4 \\
        \texttt{prm\_3\_2\_slice\_1} & slice 1 \\
        \texttt{prm\_3\_2\_slice\_2} & slice 2 \\
        \texttt{prm\_3\_2\_slice\_3} & slice 3 \\
        \texttt{prm\_3\_2\_slice\_4} & slice 4 \\
        \texttt{prm\_3\_start} & reduce start \\
        \texttt{prm\_3\_length0} & reduce length 0 \\
        \texttt{prm\_3\_stride0} & reduce stride 0 \\
        \texttt{prm\_3\_length1} & reduce length 1 \\
        \texttt{prm\_3\_stride1} & reduce stride 1 \\
    \end{tabular}
    \caption{Reference naming of VexCL kernel parameters to improve the readability.}
    \label{tab:vexcl_param_naming}
\end{table}
Because we set the \texttt{VEXCL\_SHOW\_KERNELS} flag in our program, VexCL will output the OpenCL kernels it produced, which we can use to port the application to Xilinx Vitis. The produced kernel can be seen in listing~\ref{lst:vexcl_kernel}. At the top of the file we have two pragmas that enable the double precision floating point numbers on supported hardware. Then we find an automatically generated function to sum two doubles. The actual kernel is generated as the kernel function \texttt{vexcl\_vector\_kernel} with 14 parameters. To improve the readability of this section, we will reference the parameters with the names defined in table~\ref{tab:vexcl_param_naming}. The first parameter, \textit{parameter 0}, is the size of the output vector $\vec{t}$, which was equal to $m=7$. The following parameters are the input and output buffers, which are ordered based on where they appear in the equation $\vec{t} = \vec{y} + A\vec{x}$. So \textit{buffer 1} corresponds to $\vec{t}$, \textit{buffer 2} corresponds to $\vec{y}$, \textit{buffer 3} corresponds to $A$ and \textit{buffer 4} corresponds to $\vec{x}$.

Let us now take a look at the last five parameters, which are generated by the \texttt{vex::reduce} command, \textit{reduce start}, \textit{reduce length 0}, \textit{reduce stride 0}, \textit{reduce length 1} and \textit{reduce stride 1}. The parameters \textit{reduce length 0} and \textit{reduce stride 0} are the size of the first and second dimension of the input matrix respectively, so in our case $\text{reduce length 0} = m$ and  $\text{reduce length 1} = n$. The parameter \textit{reduce length 1} corresponds to how many values need to be summed together and the parameter \textit{reduce stride 1} indicates how many elements we have to skip in the underlying array of the input matrix to get to the next value to be summed. Because we are summing over the columns, $\text{reduce length 1} = n$ and $\text{reduce stride 1} = 1$. Lastly \textit{reduce start} is a global offset in the array, which we do not need so $\text{reduce start} = 0$.

We are now only left with the parameters \textit{slice 1}, \textit{slice 2}, \textit{slice 3} and \textit{slice 4}, which are generated by the \texttt{vex::reshape} command. These parameters are used to convert a index into the flattened matrix $X$ to an index into the vector $\vec{x}$, and are easier to understand if we look to the code where they are used. In the expression \texttt{(slice\_1 * (((slice\_2 + idx) / slice\_3) \% slice\_4))}, \texttt{idx} is the index in the flattened matrix and the outcome of the expression is the index in the vector $\vec{x}$. If we start from \texttt{idx} we see that a global offset \textit{slice 2} is added, this is only used if you slice an array, not when you reshape it, so in our case $\text{slice 2} = 0$. The next operation in the expression is an integer division by \textit{slice 1}. This division is used to repeat the several index multiple times and is useful to replicate a vector along the columns of a matrix, but that is not necessary in our case, so $\text{slice 1} = 1$. Then we see a modulo operation with \textit{slice 4}, which is used to wrap around the index. In our case we wanted to replicate the vector along the rows of a matrix, so at each row of the matrix we want to start at index 0 again. This means $\text{slice 4} = |\vec{x}| = n$. Lastly we have a multiplication by \textit{slice 1}, which can be used to skip every $i$th index if set to $i$. That is not needed in our case so $\text{slice 1} = 1$. The expression \texttt{(slice\_1 * (((slice\_2 + idx) / slice\_3) \% slice\_4))} thus simplifies to $(\text{idx} \mod n)$ in our case.

Now that we have defined all the parameters, we can see that the kernel consists of a parallelized loop that runs over the elements of $\vec{t}$, which has an inner loop to calculate $A\vec{x}$, then adds the sum to $\vec{y}$ and stores the result in $\vec{t}$.

\begin{lstfloat}
\begin{cppcode}
#if defined(cl_khr_fp64)
#  pragma OPENCL EXTENSION cl_khr_fp64: enable
#elif defined(cl_amd_fp64)
#  pragma OPENCL EXTENSION cl_amd_fp64: enable
#endif

double SUM_double(double prm1, double prm2) {
  return prm1 + prm2;
}

kernel void vexcl_vector_kernel(ulong n, global double *prm_1,
  global double *prm_2, global double *prm_3_1, global double *prm_3_2_expr_1,
  ulong prm_3_2_slice_1, ulong prm_3_2_slice_2, ulong prm_3_2_slice_3,
  ulong prm_3_2_slice_4, ulong prm_3_start, ulong prm_3_length0,
  long prm_3_stride0, ulong prm_3_length1, long prm_3_stride1)
{
  for (ulong idx = get_global_id(0); idx < n; idx += get_global_size(0)) {
    double prm_3_sum = 0;
    {
      size_t pos = idx;
      size_t ptr1 = prm_3_start + (pos % prm_3_length0) * prm_3_stride0;
      for (size_t i1 = 0, ptr2 = ptr1; i1 < prm_3_length1; ++i1,
           ptr2 += prm_3_stride1) {
        size_t idx = ptr2;
        prm_3_sum = SUM_double(prm_3_sum,
          (prm_3_1[idx] * prm_3_2_expr_1[(prm_3_2_slice_1 * (
          ((prm_3_2_slice_2 + idx) / prm_3_2_slice_3) % prm_3_2_slice_4))]));
      }
    }
    prm_1[idx] = (prm_2[idx] + prm_3_sum);
  }
}
\end{cppcode}
\caption{The OpenCL kernel produced by VexCL.}\label{lst:vexcl_kernel}
\end{lstfloat}

\section{Xilinx Vitis implementation}
The Vitis implementation of the affine transformation needs two different files, where only one file is needed with VexCL, a file with the host code and a file with the device code.

\subsection{Device code}
For the device code we port the OpenCL kernel, generated by VexCL, to a Xilinx Vitis C kernel. Note that we could have used a OpenCL kernel as well for the Xilinx Vitis implementation, but not all Vitis features are available in OpenCL~\cite{Xilinxaccelarationmanual}. To convert the OpenCL kernel to a C kernel, we can mostly copy the OpenCL kernel with little changes needed, as is visible in listing~\ref{lst:vitis_kernel}. First we need to wrap the code in a \texttt{extern "C"} block to avoid name mangling issues between C and C++ \cite{Xilinxaccelarationmanual}. We also need to change some of the data types from OpenCL types to C types, so we remove the \texttt{kernel} and \texttt{global} keyword and replace the \texttt{ulong}, \texttt{long} and \texttt{size\_t} keywords with \texttt{int}. There are two OpenCL specific functions too that make sure that the loop is parallelized in OpenCL, \texttt{get\_global\_id} and \texttt{get\_global\_size}. The Vitis compiler does not have such a construct, so we can replace the for loop to simply start at zero, and increment by one. We specify the read-only buffers as \texttt{const} and rename the kernel to be more easily distinguishable. We need to add pragmas for the vectors to specify which interface protocol the \ac{fpga} should use, we will use the \ac{axi4} protocol, as recommended by Xilinx \cite{Xilinxaxireference}. Lastly we also have to create a create a configuration file for the specific \ac{fpga} to specify the main function and which memory interfaces the ports should be connected to. We will use \acrshort{ddr} interface 1 for all the parameters in our case, meaning all data transfers will happen over the same memory interface. This configuration file can be seen in listing~\ref{lst:vitis_config}.

\begin{lstfloat}
\begin{cppcode}
extern "C" {
  // Unchanged defines
  ...

  double SUM_double( double prm1, double prm2) {
    return prm1 + prm2;
  }

  void affinetrans(int n, double *prm_1, const double *prm_2,
    const double *prm_3_1, const double *prm_3_2_expr_1, int prm_3_2_slice_1,
    int prm_3_2_slice_2, int prm_3_2_slice_3, int prm_3_2_slice_4,
    int prm_3_start, int prm_3_length0, int prm_3_stride0, int prm_3_length1,
    int prm_3_stride1)
  {
    #pragma HLS INTERFACE m_axi port=prm_1 bundle=aximm1
    #pragma HLS INTERFACE m_axi port=prm_2 bundle=aximm1
    #pragma HLS INTERFACE m_axi port=prm_3_1 bundle=aximm1
    #pragma HLS INTERFACE m_axi port=prm_3_2_expr_1 bundle=aximm1

    for(int idx = 0; idx < n; ++idx)
    {
        // Unchanged computations
        ...
    }
}
\end{cppcode}
\caption{A shortened version of the ported Xilinx Vitis kernel that can run on a \ac{fpga}.}\label{lst:vitis_kernel}
\end{lstfloat}

\begin{lstfloat}
\begin{minted}[bgcolor=bg, autogobble, linenos, frame=lines, framesep=10pt]{ini}
platform=xilinx_u250_gen3x16_xdma_3_1_202020_1
debug=1
save-temps=1

[connectivity]
nk=affinetrans:1:affinetrans_1
sp=affinetrans_1.prm_1:DDR[1]
sp=affinetrans_1.prm_2:DDR[1]
sp=affinetrans_1.prm_3_1:DDR[1]
sp=affinetrans_1.prm_3_2_expr_1:DDR[1]

[profile]
data=all:all:all
\end{minted}
\caption{A Xilinx configuration file for the affine transformation kernel on a U250 Xilinx \ac{fpga}.}\label{lst:vitis_config}
\end{lstfloat}

\subsection{Host code}
The first step in creating the Vitis host code is to replace the boilerplate VexCL code with boilerplate code that enables a connection with the \ac{fpga}. As can be seen in listing~\ref{lst:vitis_boilerplate}, the defines change, and, instead of including VexCL, we include OpenCL. Setting up the context is also a bit different, where we only needed a single command for VexCL, we now need to write our own code to connect to the \ac{fpga}, and to load the Vitis kernel to the \ac{fpga}.

\begin{lstfloat}
\begin{cppcode}
#define CL_HPP_CL_1_2_DEFAULT_BUILD
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#define CL_HPP_ENABLE_PROGRAM_CONSTRUCTION_FROM_ARRAY_COMPATIBILITY 1
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl2.hpp>
...
int main(int argc, char **argv) {
    // Get Xilinx device
    const cl::Device device = get_xilinx_devices().front();
    ...
    // Load OpenCL kernel
    cl::CommandQueue q(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
    cl::Kernel krnl_affine_transform(program, "affinetrans", &err);
    ...
    return 0;
}
\end{cppcode}
\caption{The shortened boilerplate Xilinx Vitis code.}\label{lst:vitis_boilerplate}
\end{lstfloat}

The declaration and initialization of the host vectors is the same in the Vitis implementation as it was in the VexCL implementation, but copying the data to the accelerator does change, as shown in listing~\ref{lst:vitis_buffer}. Instead of creating \texttt{vex::vector} objects, we need to create \texttt{cl::Buffer} objects and specify that we want to copy the data from a host pointer and if the data will be readable, writable or both.

\begin{lstfloat}
\begin{cppcode}
    std::vector<double> a(m * n), x(n), y(m), t(m);

    // Initialize matrix + vectors
    ...

    // Create the buffers and allocate memory
    cl::Buffer A(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
                 sizeof(double) * a.size(), a.data(), &err);
    cl::Buffer X(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
                 sizeof(double) * x.size(), x.data(), &err);
    cl::Buffer Y(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
                 sizeof(double) * y.size(), y.data(), &err);
    cl::Buffer T(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
                 sizeof(double) * t.size(), t.data(), &err);
\end{cppcode}
\caption{The data initialization in Xilinx Vitis.}\label{lst:vitis_buffer}
\end{lstfloat}

We can now remove the VexCL vector computations, as that is already done in the \ac{fpga} kernel. We do however need to call the kernel ourselves in the Vitis implementation. To do this, we first need to set the arguments of the kernel. In section~\ref{sec:vexcl_kernel} we already specified which values corresponded to the arguments, so we can simply set the arguments to those values. In listing~\ref{lst:vitis_kernel_call} we set the arguments to the kernel and execute the kernel. We however first need to transfer the input buffers to the kernel and after the kernel is finished, we can transfer the output buffer back to the host device.

\begin{lstfloat}
\begin{cppcode}
    // Set kernel arguments
    krnl_affine_transform.setArg(0, (int) m);
    krnl_affine_transform.setArg(1, T);
    krnl_affine_transform.setArg(2, Y);
    krnl_affine_transform.setArg(3, A);
    krnl_affine_transform.setArg(4, X);
    krnl_affine_transform.setArg(5, 1);
    krnl_affine_transform.setArg(6, 0);
    krnl_affine_transform.setArg(7, 1);
    krnl_affine_transform.setArg(8, (int) n);
    krnl_affine_transform.setArg(9, 0);
    krnl_affine_transform.setArg(10, (int) m);
    krnl_affine_transform.setArg(11, (int) n);
    krnl_affine_transform.setArg(12, (int) n);
    krnl_affine_transform.setArg(13, 1);

    // Schedule transfer of inputs to device memory, execution of kernel,
    // and transfer of outputs back to host memory
    q.enqueueMigrateMemObjects({Y, A, X}, 0);
    q.enqueueTask(krnl_affine_transform);
    q.enqueueMigrateMemObjects({T}, CL_MIGRATE_MEM_OBJECT_HOST);

    // Wait for all scheduled operations to finish
    q.finish();
\end{cppcode}
\caption{Calling a kernel in Xilinx Vitis.}\label{lst:vitis_kernel_call}
\end{lstfloat}

\section{Verifying correctness}
\begin{lstfloat}
\begin{cppcode}
template <typename Vec>
std::vector<double> calculate_results(size_t m, size_t n, Vec A, Vec x,
                                      Vec y) {
    auto res = std::vector<double>(m * n);

    for (size_t i = 0; i < m; ++i) {
        double sum = 0;

        // Calculate matrix multiplication of current row
        for (size_t j = 0; j < n; ++j) {
            sum += A[i * n + j] * x[j];
        }

        // Store results
        res[i] = y[i] + sum;
    }

    return res;
}
\end{cppcode}
\caption{A sequential implementation of the affine transformation.}\label{lst:seq_affine}
\end{lstfloat}
To test both the VexCL and the Xilinx Vitis implementation, listing~\ref{lst:seq_affine} presents a sequential version of the algorithm, to be used as reference implementation. We check if the output of the accelerated version matches the output of this sequential version. To account for floating point errors, we check if $|(acc[i] - ref[i]) / ref[i]| < 0.01$ holds for each element in the output vectors of the accelerated implementation ($acc$) and the reference implementation $(ref)$.

\begin{lstfloat}
\begin{jsoncode}
{
    "A": {
        "size": [2, 2],
        "data": [[0, 1], [2, 3]]
    },
    "x": {
        "size": [2],
        "data": [4, 5]
    },
    "y": {
        "size": [2],
        "data": [6, 7]
    }
}
\end{jsoncode}
\caption{\Acrshort{json} format to describe input data.}\label{lst:json_input}
\end{lstfloat}
To be able to use the same input data for both the VexCL implementation and Vitis implementation, we present a \ac{json} format that describes the size and data of the matrix and vectors, as can be seen in listing~\ref{lst:json_input}. This file can then be parsed by both implementations to read the same input data, and we can now change the input data without recompiling the program.

\section{Compiling and running the ported application}
\subsection{Compilation and execution time}
\begin{table}
    \centering
    \begin{tabular}{c|c|c}
         \textbf{Target} & \textbf{Compilation time (hours)} & \textbf{Execution time (minutes)} \\
         \hline
         Software emulation & 00:00:51 & 00:01.244 \\
         Hardware emulation & 00:06:38 & 08:25.781 \\
         Hardware execution & 01:31:11 & 00:01.376
    \end{tabular}
    \caption{Compilation and execution time of the Vitis kernel with different targets using a 12-core Intel Xeon Gold 6128 \ac{gpu} with 187 GiB of RAM capacity and a Xilinx Alveo U250 \ac{fpga}. A random $256 {\times} 256$ matrix and three random vectors with a length of $256$ are used as input data to measure the execution time.}
    \label{tab:vitis_compilation_time}
\end{table}

As described in chapter~\ref{ch:background}, there are three compilation targets for the Vitis kernel, software emulation, hardware emulation and hardware execution. To compile the kernel, we must first run the \ac{hls} compiler to turn the kernel into a \ac{xo} file, and then run the linker of the Vitis compiler to turn the \ac{xo} file into a binary for the \ac{fpga} device. In table \ref{tab:vitis_compilation_time} we show the time it takes to compile the kernel for these three targets using a 12-core Intel Xeon Gold 6128 \ac{cpu}, and how long it takes each version to compute the affine transformation of a random $256 {\times} 256$ matrix and three random vectors with a length of $256$. We can see that compiling for hardware execution indeed takes much longer than compiling for emulation. We can also see that running the hardware emulation is very slow in comparison to software emulation and hardware execution.

\subsection{Compiled FPGA kernel}
Once we compile the kernel, we can let the Vitis Analyser produce a system diagram. This diagram can be seen in figure~\ref{fig:unoptimized_vitis_diagram}. We can see that all the buffers are connected to the \acrshort{ddr} memory interface, and that the other parameters are directly fed by a control bus. We can also see that our design uses 7,356 \acp{lut}, 2 \acp{bram}, 10,022 registers and 17 \ac{dsp} slices.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/unoptimized_vitis_diagram.pdf}
    \caption{System diagram of the affine transformation implementation on Xilinx Vitis FPGA.}
    \label{fig:unoptimized_vitis_diagram}
\end{figure}

\subsection{Application timeline}
\begin{table}
    \centering
    \begin{tabular}{l|r|r}
        \textbf{Task} & \textbf{Time} & \textbf{Percentage of total time} \\
        \hline
        Loading kernel to \ac{fpga} &  122.8 ms & 64.0\% \\
        Generating input data &  0.7 ms & 0.4\% \\
        Memory buffer creation and setting kernel args &  1.6 ms & 0.8\% \\
        Data transfers and kernel execution & 4.4 ms & 2.3\% \\
        Verifying results & 62.2 ms & 32.5\% \\
        \hline
        Full application & 191.6 ms & 100.0\%
    \end{tabular}
    \caption{Analysis of the application timeline in figure~\ref{fig:vitis_timeline}, showing how long each part of the ported affine transformation application approximately takes. A random $256 {\times} 256$ matrix and three random vectors with a length of $256$ are used as input data for the application.}
    \label{tab:timeline_analysis}
\end{table}

\begin{figure}
    \begin{subfigure}[c]{\linewidth}
        \centering
        \includegraphics[width=\linewidth]{images/vitis_timeline.png}
        \caption{}
        \label{fig:vitis_timeline_full}
    \end{subfigure}
    \\[1em]
    \begin{subfigure}[c]{\linewidth}
        \centering
        \includegraphics[width=\linewidth]{images/vitis_timeline_zoomed.png}
        \caption{}
        \label{fig:vitis_timeline_zoomed}
    \end{subfigure}
    \caption{Application timeline of ported Vitis application to calculate an affine transformation, showing the OpenCL calls, data transfers and kernel executions. A random $256 {\times} 256$ matrix and three random vectors with a length of $256$ are used as input data for the application. Sub-figure~\subref{fig:vitis_timeline_full} shows the full timeline, while sub-figure~\subref{fig:vitis_timeline_zoomed} shows the timeline zoomed in on the kernel execution and data transfers.}
    \label{fig:vitis_timeline}
\end{figure}

Using the Xilinx profiler we can create an application timeline with information about OpenCL API calls, data transfers and kernel execution time from a run of our application. In figure~\ref{fig:vitis_timeline} we show a timeline of our ported Vitis application that is run using a randomly generated $256 {\times} 256$ matrix and three randomly generated vectors of size $256$. In table~\ref{tab:timeline_analysis}, we show how long each part of the application takes, as derived from the timeline. We can see that most time is spend loading the kernel to the \ac{fpga}, and verifying the results. That verifying the results using a sequential implementation of the algorithm is slow compared to computing the results on the \ac{fpga} is expected and shows that our implementation is faster than the sequential version. That loading the kernel takes the longest time in this case is also expected, since the input data is very small and is thus easy to compute. The time it takes to load the kernel to the \ac{fpga} is constant, while the time it takes to do the other tasks is dependent on the size of the input.

\section{Porting guidelines}
The steps needed to convert the VexCL code to Vitis code are shown in table~\ref{tab:vexcl_vitis_host} for the host code, and in table~\ref{tab:vexcl_vitis_kernel} for the kernel. Users can use these tables to match patterns in the VexCL and OpenCL code, and then replace them with the Xilinx Vitis counterpart. This simplifies the process of porting a VexCL application to Xilinx Vitis. The tables can also serve as a basis to describe which tasks are necessary to automate the process of converting a VexCL application to Xilinx Vitis.

\begin{table}
    \centering
    \footnotesize
    \begin{tabularx}{\textwidth}{|L{1}|L{1}|}
        \hline
        \textbf{VexCL} & \textbf{Xilinx Vitis} \\
        \hline
        \multirow{4}{*}{// Entire kernel} & \#include <stddef.h> \\
        & extern "C" \{ \\
        & \quad// Entire kernel \\
        & \} \\
        \hline
        kernel void vexcl\_vector\_kernel(\dots) \{\dots\} & void \textit{kernel\_function\_name}(\dots) \{\dots\} \\
        \hline
        ulong & size\_t \\
        \hline
        idx = get\_global\_id(0) & idx = 0 \\
        \hline
        idx += get\_global\_size(0) & ++idx \\
        \hline
        \multirow{2}{*}{global \textit{type} *\textit{prm}} & const \textit{type} *\textit{prm} // if \textit{prm} is input buffer \\
        & \textit{type} *\textit{prm} // if \textit{prm} is output buffer \\
        & \dots \\
        & \#pragma HLS INTERFACE m\_axi port=\textit{prm} bundle=aximm\textit{<memory interface No.>} \\
        \hline
    \end{tabularx}
    \caption{Kernel code changes from VexCL to Xilinx Vitis.}
    \label{tab:vexcl_vitis_kernel}
\end{table}

\begin{table}
    \centering
    \footnotesize
    \begin{tabularx}{\textwidth}{|L{0.75}|L{1.25}|}
        \hline
        \textbf{VexCL} & \textbf{Xilinx Vitis} \\
        \hline
        \multirow{5}{*}{ctx(vex::Filter::DoublePrecision);} & device = get\_xilinx\_devices().front(); \\
        & \dots \\
        & cl::CommandQueue q(context, device, CL\_QUEUE\_PROFILING\_ENABLE, \&err); \\
        & cl::Kernel \textit{krnl\_name}(program, \textit{kernel\_function\_name}, \&err); \\
        \hline
        vex::vector<cl\_type> \textit{A}(ctx, \textit{a}.size(), \textit{a}.data()); & cl::Buffer \textit{A}(context, CL\_MEM\_READ\_ONLY | CL\_MEM\_USE\_HOST\_PTR, sizeof(type) * \textit{a}.size(), \textit{a}.data(), \&err); \\
        vex::vector<cl\_type> \textit{T}(ctx, \textit{t}.size()); & cl::Buffer \textit{T}(context, CL\_MEM\_WRITE\_ONLY | CL\_MEM\_USE\_HOST\_PTR, sizeof(type) * \textit{t}.size(), \textit{t}.data(), \&err); \\
        ... & \\
        vex::copy(\textit{T}.begin(), \textit{T}.end(), \textit{t}.data()); & \\
        \hline
        \textit{T} = \textit{X} + \textit{Y}; & \textit{krnl\_name}.setArg(0, \textit{output\_vector\_size});\\
        & \textit{krnl\_name}.setArg(1, \textit{T});\\
        & \textit{krnl\_name}.setArg(2, \textit{X}); \\
        & \textit{krnl\_name}.setArg(3, \textit{Y}); \\
        \hline
        reshape(\textit{X}, extents[\textit{m}][\textit{n}], extents[1]) & \textit{krnl\_name}.setArg(0, \textit{output\_vector\_size});\\
        & \textit{krnl\_name}.setArg(1, \textit{X}); \\
        & \textit{krnl\_name}.setArg(2, 1); // skip indices \\
        & \textit{krnl\_name}.setArg(3, 0); // offset \\
        & \textit{krnl\_name}.setArg(4, 1); // repetitions \\
        & \textit{krnl\_name}.setArg(5, \textit{n}); // modulo \\
        \hline
        reduce<OP>(extents[\textit{m}][\textit{n}], \textit{X}, 1) & \textit{krnl\_name}.setArg(0, \textit{output\_vector\_size});\\
        & \textit{krnl\_name}.setArg(1, \textit{X}); \\
        & \textit{krnl\_name}.setArg(2, 0); // offset \\
        & \textit{krnl\_name}.setArg(3, \textit{m}); // first dimension matrix \\
        & \textit{krnl\_name}.setArg(4, \textit{n}); // second dimension matrix \\
        & \textit{krnl\_name}.setArg(5, \textit{n}); // amount of values to reduce each time \\
        & \textit{krnl\_name}.setArg(6, 1); // distance between values \\
        \hline
        // Finished computations & q.enqueueMigrateMemObjects(\textit{input\_buffers}, 0); \\
        & q.enqueueTask(\textit{krnl\_name}); \\
        & q.enqueueMigrateMemObjects(\textit{output\_buffers}, CL\_MIGRATE\_MEM\_OBJECT\_HOST); \\
        & q.finish(); \\
        \hline
    \end{tabularx}
    \caption{Host code changes from VexCL to Xilinx Vitis.}
    \label{tab:vexcl_vitis_host}
\end{table}
