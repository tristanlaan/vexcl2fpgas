\chapter{The effectiveness of the porting process}\label{ch:applications}
In this chapter we apply the porting guide proposed in chapter~\ref{ch:affinetrans} to a more advanced application, thus testing its effectiveness; we further improve the porting guide, as necessary. The application we use is \ac{spmv}. \Ac{spmv} uses sparse matrix representations to more efficiently store large matrices with many zero values, by only storing the non-zero values. Sparse matrices are very common in scientific computation, in applications from graph processing, like PageRank~\cite{pagerank}, to physics calculations~\cite{chen2010}. 

In this chaper, we specifically look at the calculation defined in equation~\ref{eq:spmv} and~\ref{eq:phi}, where $A$ is a sparse matrix of size $m {\times} n$, $\Vec{t}$ is a vector of size $m$, $\Vec{u_1}$, $\Vec{u_2}$, and $\Vec{u_3}$ are vectors of size $n$, $\odot$ is the element-wise multiplication, and $\oslash$ is the element-wise division.

\begin{align}
    \Vec{t} &= A \varphi(\Vec{u_1}, \Vec{u_2}, \Vec{u_3}) \label{eq:spmv}\\
    \varphi(\Vec{u_1}, \Vec{u_2}, \Vec{u_3}) &= \left(\Vec{u_1} - \Vec{u_2} + \log^2(\Vec{u_3}) \odot \sin(\Vec{u_1})\right) \oslash \left(\Vec{u_1} \odot \Vec{u_2}\right) \label{eq:phi}
\end{align}

\section{Sparse matrix representation}
There are several formats that can be used to represent a sparse matrix, all with their own advantages and disadvantages. VexCL only uses the \ac{csr} and \ac{hell} formats, so we will focus on those two.

\subsection{CSR format}
The \ac{csr} format stores the sparse matrix using three arrays, a row array ($R$), a column array ($C$), and a data array ($V$). $V$ stores all the non-zero values of the array, and has a length of $\mathit{nnz}$. $C$ has the same length as $V$, and specifies the column at which each value is located in matrix, i.e. $C[i]$ stores the column of $V[i]$. The row array has a length of $(m + 1)$, where $m$ is the amount of rows, and $(R[i] - R[i - 1])$ specifies the amount of elements in the $i$th row of the matrix. More specifically, $R[i - 1]$ specifies at which index the first element of row $i$ starts in the arrays $C$ and $V$, and $R[i]$ specifies what the last index of row $i$ is in $C$ and $V$. The arrays $V$ and $C$ must be grouped by row to allow this representation, and to improve cache performance, the arrays should be sorted by row and column. Note that because of this, the first value of $R$ will always be zero, and the last value $\mathit{nnz}$. In equation~\ref{eq:csr} an example is shown of how the matrix $A$ on the left is stored in the \ac{csr} format on the right.

\begin{align}\label{eq:csr}
    A=\begin{pmatrix}
        0 & 6 & 9 & 7 \\
        0 & 0 & 0 & 0 \\
        3 & 3 & 0 & 0
    \end{pmatrix} &&
    \begin{aligned}
        V &= [6, 9, 7, 3, 3] \\
        C &= [1, 2, 3, 0, 1] \\
        R &= [0, 3, 3, 5]
    \end{aligned}
\end{align}

\subsection{ELL format}
In the \ac{ell}, two matrices are used to store the sparse matrix: a a data matrix $V$, which stores the non-zero values, and a column matrix $C$, which stores the columns of the values in the sparse matrix. Each row of the two matrices represent a row in the sparse matrix, and the entry $C_{i,j}$ specifies in which column the entry $V_{i,j}$ is stored in the sparse matrix. The matrices $V$ and $C$ have the same size, the height is equal to the height of the sparse matrix, and the width is equal to highest amount of non-zero values in a row of the sparse matrix. When not all the rows in the sparse matrix contain the same amount amount of non-zero values, the smaller rows will be padded in the matrices $V$ and $C$. The \ac{ell} format requires more space than the \ac{csr} format, but the values can more easily be retrieved. Equation~\ref{eq:ell} shows an example of how the matrix $A$ on the left is stored in the \ac{ell} format on the right, with padded values represented as $\ast$.

\begin{align}\label{eq:ell}
    A=\begin{pmatrix}
        0 & 6 & 9 & 7 \\
        0 & 0 & 0 & 0 \\
        3 & 3 & 0 & 0
    \end{pmatrix} &&
    \begin{aligned}
        C = \begin{pmatrix}
            1 & 2 & 3 \\
            \ast & \ast & \ast \\
            0 & 1 & \ast
        \end{pmatrix} &&
        V = \begin{pmatrix}
            6 & 9 & 7 \\
            \ast & \ast & \ast \\
            3 & 3 & \ast
        \end{pmatrix}
    \end{aligned}
\end{align}

\subsection{Hybrid ELL--CSR format}
If a few rows are larger than the rest in the \ac{ell} format, the wasted space from padded value increases significantly. To mitigate this, the \ac{hell} format stores the bulk of the data using the \ac{ell} format, but uses the \ac{csr} format for the outliers. An example of this is shown in equation~\ref{eq:hell}, where the matrix $A$ on the left is stored using the \ac{hell} format on the right, where the first two columns are stored using the \ac{ell} format, while the last column is stored using the \ac{csr} format.

\begin{align}\label{eq:hell}
    A=\begin{pmatrix}
        0 & 6 & 9 & 7 \\
        0 & 0 & 0 & 0 \\
        3 & 3 & 0 & 0
    \end{pmatrix} &&
    \begin{aligned}
        C_{ell} = \begin{pmatrix}
            1 & 2 \\
            \ast & \ast \\
            0 & 1
        \end{pmatrix} &&
        V_{ell} = \begin{pmatrix}
            6 & 9 \\
            \ast & \ast \\
            3 & 3
        \end{pmatrix} \\
        \begin{aligned}
            V_{csr} &= [7] \\
            C_{csr} &= [3] \\
            R_{csr} &= [0, 1, 1, 1]
        \end{aligned}
    \end{aligned}
\end{align}

\section{VexCL implementation}\label{sec:spmv_vexcl}
The VexCL implementation of \acp{spmv} application is quite straightforward, since VexCL has native support for sparse matrices and \acp{spmv}. The sparse matrices must be initialized in the \ac{csr} format, and VexCL will then convert them to the \ac{hell} format, which is the format that will be used on the \ac{gpu}. The \acp{spmv} can simply be calculated using the standard multiplication operator in C++. The $\varphi$ function can also be easily implemented using the log and sin functions provided by VexCL. We do however use a new VexCL construct in the $\varphi$ function, the tag function. The \texttt{tag} function makes sure that if a vector is used multiple times in the same kernel, VexCL will not use duplicate parameters to send the same vector to the kernel multiple times, instead all the duplicate uses of the vector will use the same tag parameter. A shortened version of the VexCL implementation can be seen in listing~\ref{lst:vexcl_spmv}.

\begin{lstfloat}
\begin{cppcode}
template <typename Vec>
Vec phi(U1, U2, U3) {
    auto u1 = vex::tag<1>(U1);
    auto u2 = vex::tag<2>(U2);
    auto u3 = vex::tag<3>(U3);

    return (u1 - u2 + vex::log(u3) * vex::log(u3) * vex::sin(u1)) / (u1 * u2);
}

int main() {
    std::vector<double> A_row(m), A_col(nnz), A_data(nnz), u1(n), u2(n), u3(n);
    std::vector<double> t(m);

    // Initialize matrix + vectors
    ...

    // Transfer host-side doubles into device-side cl_double vectors
    vex::SpMat<cl_double> A(ctx, m, n, A_row.data(), A_col.data(),
        reinterpret_cast<cl_double*>(A_data.data()));
    vex::vector<cl_double> U1(ctx, u1.size(),
        reinterpret_cast<cl_double*>(u1.data()));
    vex::vector<cl_double> U2(ctx, u2.size(),
        reinterpret_cast<cl_double*>(u2.data()));
    vex::vector<cl_double> U3(ctx, u3.size(),
        reinterpret_cast<cl_double*>(u3.data()));
    vex::vector<cl_double> T(ctx, t.size();
    
    T = A * phi(U1, U2, U3);
    
    vex::copy(T.begin(), T.end(), reinterpret_cast<cl_double*>(t.data()));
    
    // Verify results
    ...
}
\end{cppcode}
\caption{A VexCL implementation of the \acrlong*{spmv} calculation.}\label{lst:vexcl_spmv}
\end{lstfloat}

\subsection{OpenCL kernel}\label{sec:spmv_opencl}
When we run the VexCL applicatio to retrieve the OpenCL kernel, we can see that three kernels are generated.

The first kernel, seen in listing~\ref{lst:opencl_spmv_copy}, simply copies the vector \texttt{prm\_2} to vector \texttt{prm\_1}. This kernel is called three times, for each of the calls to the tag function from VexCL. VexCL does this to make a copy of the vector to be used by the tag, so that if the vector passed to the tag function is changed, the data from the tag vector stays the same. Since we do not modify the vectors $\Vec{u_1}$, $\Vec{u_2}$, and $\Vec{u_3}$, these temporary copies of the vectors are unnecessary.

\begin{lstfloat}
\begin{cppcode}
kernel void vexcl_vector_kernel(ulong n, global double *prm_1,
  global double *prm_2) {
  for(ulong idx = get_global_id(0); idx < n; idx += get_global_size(0)) {
    prm_1[idx] = prm_2[idx];
  }
}
\end{cppcode}
\caption{The first OpenCL kernel generated by VexCL, which copies a vector.}\label{lst:opencl_spmv_copy}
\end{lstfloat}

The second kernel that is generated is the kernel that executes the $\varphi$ function, and can be seen in listing~\ref{lst:opencl_spmv_phi}. We can see that, by using the tag function, only three input buffers are passed to the kernel, i.e., the three buffers created by the first kernel. The main body of the code simply calculates the expression as specified in the $\varphi$ function, using the builtin $\log$ and $\sin$ functions from OpenCL.

\begin{lstfloat}
\begin{cppcode}
kernel void vexcl_vector_kernel(ulong n, global double *prm_1,
  global double *prm_tag_1_1, global double *prm_tag_2_1,
  global double *prm_tag_3_1) {
  for(ulong idx = get_global_id(0); idx < n; idx += get_global_size(0)) {
    prm_1[idx] = (prm_tag_1_1[idx] - prm_tag_2_1[idx] + log(prm_tag_3_1[idx]) \
      * log(prm_tag_3_1[idx]) * sin(prm_tag_1_1[idx])) / \
      (prm_tag_1_1[idx] * prm_tag_2_1[idx]);
  }
}
\end{cppcode}
\caption{The second OpenCL kernel generated by VexCL, which executes the $\varphi$ function.}\label{lst:opencl_spmv_phi}
\end{lstfloat}

\begin{table}
    \centering
    \begin{tabular}{c|c}
        \textbf{Parameter} & \textbf{Value} \\
        \hline
        \texttt{ell\_w} & height of the \ac{ell} matrices \\
        \texttt{ell\_pitch} & width of the \ac{ell} matrices \\
        \texttt{ell\_col} & $C_{ell}$ matrix \\
        \texttt{ell\_val} & $V_{ell}$ matrix \\
        \texttt{csr\_row} & $R_{csr}$ array \\
        \texttt{csr\_col} & $C_{csr}$ array \\
        \texttt{csr\_val} & $V_{csr}$ array \\
    \end{tabular}
    \caption{Values of the parameters that correspond to the \acrshort*{hell} sparse matrix representation in the third OpenCL kernel generated by VexCL.}
    \label{tab:opencl_spmv_params}
\end{table}

The last kernel generated by VexCL executes the actual \ac{spmv}, as can be seen in listing~\ref{lst:opencl_spmv_spmat}. We can see that several parameters are generated for the \ac{ell} and \ac{csr} part of the \ac{hell} sparse matrix representation. In table~\ref{tab:opencl_spmv_params} we show what value each of these parameters represent. A input vector is passed to the kernel for the vector of the multiplication, and an output vector is passed to store the result. Lastly there is a scale variable that can be used if the resulting vector of the \ac{spmv} is multiplied by a scalar. The outer loop of the kernel loops over the rows of the matrix, and is parallelized. The first inner loop in the kernel loops over the current row in the \ac{ell} matrices, and first retrieves the corresponding column from the $C_{ell}$ matrix, and then calculates the matrix vector multiplication on that element of the matrix. Note that the padding of the \ac{ell} matrix is represented by setting the entry in the $C_{ell}$ to the maximum value of an unsigned integer. Then a second for loop iterates over the $R_{csr}$ matrix to calculate the matrix vector multiplication of the \ac{csr} part. Here, again, the corresponding column is retrieved first from the $C_{csr}$ matrix, and then the matrix vector multiplication on that element of the matrix is computed. The sum of all those multiplication is then stored in the output vector to complete the \ac{spmv} calculation of that row.
\begin{lstfloat}
\begin{cppcode}
kernel void hybrid_ell_spmv(ulong n, double scale, ulong ell_w,
  ulong ell_pitch, global const ulong *ell_col,
  global const double *ell_val, global const ulong *csr_row,
  global const ulong *csr_col, global const double *csr_val,
  global const double *in, global double *out) {
  for(ulong i = get_global_id(0); i < n; i += get_global_size(0)) {
    double sum = 0;
    for(size_t j = 0; j < ell_w; ++j) {
      ulong c = ell_col[i + j * ell_pitch];
      if (c != (ulong)(-1)) {
        sum += ell_val[i + j * ell_pitch] * in[c];
      }
    }
    if (csr_row) {
      for(size_t j = csr_row[i], e = csr_row[i + 1]; j < e; ++j) {
        sum += csr_val[j] * in[csr_col[j]];
      }
    }
    out[i] = scale * sum;
  }
}
\end{cppcode}
\caption{Third OpenCL kernel generated by VexCL, which executes the \acrshort*{spmv} calculation.}\label{lst:opencl_spmv_spmat}
\end{lstfloat}


\section{Xilinx Vitis implementation}
\subsection{Porting process}
When we look at the VexCL and OpenCL code, we see that there are some new constructs, which we have not seen before in the porting process. This means that we have to adapt our porting guide, as described in chapter~\ref{ch:affinetrans}.

First of all, there are now three kernels instead of one. We can ignore the first kernel, since it only makes an unnecessary copy of the $\Vec{u_i}$ vectors, which we do not need as we use the $\Vec{u_i}$ vectors as read-only vectors, so we do not modify them. Even if we modified the $\Vec{u_i}$ vectors, we could simply copy them for the tag on the host-side. The other two kernels are however both necessary. We could combine the two kernels into one kernel, but that can have a big influence on the performance if the kernels are large, or if there are many kernels. Instead we will adapt the porting guide to support multiple kernels. On the kernel side of the porting process nothing changes, we simply create the kernels separately of each other, we will call the first kernel \texttt{phi}, and the second kernel \texttt{spmat}. We first compile both kernels seperately into \ac{xo} files, and then link the \ac{xo} files to the same binary using the Vitis compiler. The configuration file that we used to specify the connectivity of the kernel for the compiler can combine the configurations of both kernels in the same file. The compiler will then generate a design for the \ac{fpga} that contains both the kernels, meaning that both the kernels will be present on the \ac{fpga} when we load the binary, and no kernel swapping will be needed. On the host-side we only need to duplicate one line to load an extra kernel, the line \texttt{cl::Kernel }\textit{krnl\_name}\texttt{(program, }\textit{kernel\_function\_name}\texttt{, \&err)}. The only thing we need to change to this duplicated line is the \textit{krnl\_name} and \textit{kernel\_function\_name}. We can then set the kernel arguments for both kernels, and enqueue the kernels separately. The last thing that changes is that we can now not only have input and output buffers, but also buffers that are only used to communicate between kernels. To specify such a buffer, we can need to replace the memory flags from \texttt{CL\_MEM\_}\textit{READ/WRITE}\texttt{\_ONLY | CL\_MEM\_USE\_HOST\_PTR} to \texttt{CL\_MEM\_HOST\_NO\_ACCESS}, and set the host pointer argument of the function to \texttt{NULL}.

Another construct we have not encountered before is the \texttt{vex::SpMat} class. In section~\ref{sec:spmv_vexcl} we saw that the class changed a sparse matrix in \ac{csr} format to a sparse matrix in \ac{hell} format. We will need to do the same to be able to call the kernel. To accomplish this we have created our own \texttt{SpMat} class, based on the \texttt{vex::SpMat}, and changed the code so that it no longer depends on the VexCL code base. We did this by (1) replacing the VexCL vectors with standard vectors, which we will later use in the OpenCL buffers, (2) removing the support to split the sparse matrix over multiple kernels, (3) removing the OpenCL code generation, and (4) making the internal \ac{hell} format public, so that we can retrieve the data for the kernel arguments later on. We can then simply call this class with the same parameters as the VexCL class, except for the VexCL context parameter which must be left out, and than create buffers for the $C_{ell}$ and $V_{ell}$ matrices, and for the $R_{csr}$, $C_{csr}$, and $V_{csr}$ vectors.

For the first kernel, we have two functions we have not encountered yet, \texttt{log} and \texttt{sin}. Since C only provide those functions in the math library, we need to add \texttt{\#include <math.h>} add the top of the kernel. When calling this kernel, we can simply use the $\Vec{u_i}$ for the tag parameters. Note that the parameter format is \texttt{prm\_tag\_}\textit{n}\texttt{\_1}, where \textit{n} is the value we gave to the tag in VexCL.

For the last kernel that calculates the \ac{spmv}, we also identify some new parameters. Fortunately, they are all provided by the \texttt{spmat} class, except for the \texttt{scale} parameter, as mentioned in section~\ref{sec:spmv_opencl}, which must be set to one in this case, since we do not multiply the \ac{spmv} result with a scalar.

\subsection{Compiled application}
In figure~\ref{fig:vitis_diagram_spmv} we show the system diagram of the compiled kernels on a Xilinx U250 \ac{fpga}. Note that in this version of the kernels we also implemented the memory interface optimization from chapter~\ref{ch:optimizations}, and spread the parameters over the memory interfaces. We can see that the phi kernel uses more resources than the \texttt{spmat} kernel, this is expected, as the phi function consists of many calculations.

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{images/vitis_diagram_spmv.pdf}
    \caption{System diagram of ported \acrlong*{spmv} application from VexCL to Xilinx Vitis on a Xilinx U250 \acrshort*{fpga}.}
    \label{fig:vitis_diagram_spmv}
\end{figure}

\subsection{Verifying correctness}
To test the correctness of the application, we have again implemented a sequential version of the algorithm that runs on the \ac{cpu}, and verified the output of the application using random data of different sizes and densities. From this verification process, we conclude that the port was successful.

\section{Performance study}
To test how effective the optimizations of chapter~\ref{ch:optimizations} are, we also apply them to the \ac{spmv} application.

\subsection{Implementations}
In optimization 1, we applied the common optimizations of aligning the host memory and using more memory interfaces. We did have to use the same interfaces multiple times, since there are almost three times more data buffers than memory interfaces. We chose the interface mapping such that parameters that can be read at the same time use different interfaces.

In optimization 2, we replaced the floating-point numbers of optimization 1 with fixed-point numbers. This did create a problem with the \texttt{log} and \texttt{sin} functions, since the functions are not defined for fixed-point numbers in the standard math header. To fix this we used the hls\_math header, provided by Xilinx, which define the functions \texttt{hls::log} and \texttt{hls::sin} for fixed-point integers. They do however not work with custom quantization and overflow setting, so we had to redefine our fixed-point integers to use the default quantization method of truncating to minus infinity, and the default overflow setting of wrapping around.

In optimization 3, we added burst buffers to the phi kernel of optimization 2. Note that we do not use burst buffers in the spmat kernel, since the kernel does not access the memory in a sequential order.

We do not apply the data width saturation optimization, since the results from chapter~\ref{ch:optimizations} indicate that the Vitis compiler does not handle structs well.

Unfortunately we were not able to compile optimizations 2 and 3 for the hardware, since there were timing issues with the phi kernel during the compilation. This means that the kernel is too complicated to be run at the target frequency. This could be solved by lowering the target frequency, but the compilation report indicates that we would have to at least half the target frequency from 300 MHz to 150 MHz, which would have a large impact on the performance. So we choose to drop these two optimizations, as we suspect the performance impact would be too large to give useful results. We suspect the timing issues are caused by current inefficient implementations of the $\log$ and $\sin$ functions in the hls\_math library for fixed-point integers.

\subsection{Method}
We compare three implementations of the \ac{spmv} application, the VexCL implementation, the unoptimized Vitis implementation, and optimization 1. The VexCL implementation uses the \ac{gpu} as accelerator, and the Vitis implementations uses an \ac{fpga}. The hardware and profiling tools used for this experiment are the same as in chapter~\ref{ch:optimizations}.

\begin{table}
    \centering
    \begin{tabular}{c|cc|c}
        \textbf{No.} & $\mathbf{m {\times} n}$ & \textbf{density} & \textbf{total matrix elements}  \\
        \hline
        1 & $512 {\times} 512$ & $0.01$ & $2621$ \\
        2 & $5000 {\times} 5000$ & $0.01$ & $2.5 \cdot 10^5$ \\
        3 & $10^5 {\times} 10^5$ & $0.01$ & $10^8$ \\
        4 & $12500 {\times} 12500$ & $0.64$ & $10^8$ \\
        5 & $10^6 {\times} 10^6$ & $10^{-4}$ & $10^8$
    \end{tabular}
    \caption{Sparse matrix configurations used for the experiment.}
    \label{tab:spmv_matrices}
\end{table}

We generate random input data for the experiment using a Python script. To generate random sparse matrices with a given density, the sparse.random function is used from the SciPy library\footnote{scipy.sparse.random -- \url{https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.random.html}}. We use five different sparse matrix configurations, as specified in table~\ref{tab:spmv_matrices}. We choose the first three configurations to see how the matrix size influences the performance, and the last two configurations to see how the density of the sparse matrix influences the performance. All matrix configurations are run 10 times, all with new random input data.

\subsection{Results}

\subsubsection{Wall time}
In figure~\ref{fig:spmat_walltime_results} we show the wall time of the implementations. We can see that, in line with the results from the affine transformation application in chapter~\ref{ch:optimizations}, the VexCL implementation on the \ac{gpu} is always faster than the Vitis implementations. The difference between the VexCL implementation and the unoptimized Vitis implementation is smaller however for the \ac{spmv} application than for the affine transformation application. The VexCL implementation is generally only up to 50\% faster than the Vitis implementation for the \ac{spmv} application, in comparison to generally about 80\% faster for the affine transformation application. The optimized version of the Vitis implementation is always faster than the unoptimized version, generally between 5\% and 15\%.

We can see that the matrix with a density of $0.64$ takes the longest time of the matrices of our density experiment, while the matrix with the density of $0.01$ is generally slightly faster than the matrix with a density of $10^{-4}$.
\begin{figure}
    \begin{subfigure}{0.45\linewidth}
        \centering
        \includegraphics{images/spmat_results/walltime_0_plot.pdf}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.55\linewidth}
        \centering
        \includegraphics{images/spmat_results/walltime_1_plot.pdf}
        \caption{}
    \end{subfigure}

    \caption{The wall time of the different \acrshort*{spmv} implementations with different matrix configurations, averaged over ten runs.}
    \label{fig:spmat_walltime_results}
\end{figure}

\subsubsection{Kernel time}
In figure~\ref{fig:spmat_kerneltime_phi_results} we show the kernel time of the phi kernel on the \ac{fpga} for the different implementations, and in figure~\ref{fig:spmat_kerneltime_spmat_results} we show the kernel time of the spmat kernel. We can see that the optimized version of the phi kernel is much faster than the unoptimized version, especially as the vector size rises, reaching a speedup of up to 95\% in comparison to the unoptimized version. This is less the case for the spmat kernel, where the optimized version is up to 15\% slower for the smaller matrices, and is only about 25\% faster for the larger matrices.
\begin{figure}
    \begin{subfigure}{0.55\linewidth}
        \centering
        \includegraphics{images/spmat_results/kerneltime_phi_0_plot.pdf}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.45\linewidth}
        \centering
        \includegraphics{images/spmat_results/kerneltime_phi_1_plot.pdf}
        \caption{}
    \end{subfigure}

    \caption{The kernel time of the phi kernel in different \acrshort*{spmv} implementations with different matrix configurations, averaged over ten runs.}
    \label{fig:spmat_kerneltime_phi_results}
\end{figure}

\begin{figure}
    \begin{subfigure}{0.45\linewidth}
        \centering
        \includegraphics{images/spmat_results/kerneltime_spmat_0_plot.pdf}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.55\linewidth}
        \centering
        \includegraphics{images/spmat_results/kerneltime_spmat_1_plot.pdf}
        \caption{}
    \end{subfigure}

    \caption{The wall time of the spmat kernel in different \acrshort*{spmv} implementations with different matrix configurations, averaged over ten runs.}
    \label{fig:spmat_kerneltime_spmat_results}
\end{figure}

\FloatBarrier % Make sure the power usage section isn't spread over 3 pages
\subsubsection{Power usage}
In figure~\ref{fig:spmat_power_results} we show the power usage of the different implementations. We can see that the \ac{gpu} uses around 50 W of power with the VexCL implementation, while the \ac{fpga} uses between 27 W and 37 W of power with the Vitis implementations. We can see that the unoptimized Vitis implementation uses less power on average as the matrix size grows, while the optimized Vitis implementation uses more power as the matrix size grows.
\begin{figure}
    \centering
    \includegraphics{images/spmat_results/power_plot.pdf}
    \caption{The power usage of the different \acrshort*{spmv} implementations with different matrix configurations, averaged over ten runs.}
    \label{fig:spmat_power_results}
\end{figure}

\subsubsection{Energy usage}
In figure~\ref{fig:spmat_energy_results} we show the approximate energy usage of the implementations, calculated using the wall time and power usage. We can see that for the smallest matrix the VexCL and optimized Vitis version use the least energy, using about 31\% less energy than the unoptimized Vitis version. For the $5000{\times}5000$ matrix, the VexCL implementation uses the least amount of energy, using 34\% less energy than the optimized Vitis version, and 39\% less energy than the unoptimized Vitis version. For the larger matrices the unoptimized Vitis implementation uses the least amount of energy, using up to 16\% less energy than the VexCL implementation, and up to 22\% less energy than the optimized Vitis implementation. The only exemption to this is the largest matrix, where the VexCL implementation uses the same amount of energy as the unoptimized Vitis implementation.

In this application the difference between the energy usage of the Vitis implementations and the VexCL implementation is more favorable for the Vitis implementations than when we looked at the affine transformation application in chapter~\ref{ch:optimizations}, where the VexCL implementation generally used between 47\% and 73\% less energy than the Vitis implementation.
\begin{figure}
    \begin{subfigure}{0.45\linewidth}
        \centering
        \includegraphics{images/spmat_results/energy_0_plot.pdf}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.55\linewidth}
        \centering
        \includegraphics{images/spmat_results/energy_1_plot.pdf}
        \caption{}
    \end{subfigure}

    \caption{The approximate energy usage of the different \acrshort*{spmv} implementations with different matrix configurations, derived from the wall time and power usage results.}
    \label{fig:spmat_energy_results}
\end{figure}


\FloatBarrier % Again, make sure the summary section isn't spread over 3 pages
\section{Summary}
By extending the porting guide to allow more VexCL constructs, we have successfully ported a \ac{spmv} application from VexCL to Xilinx Vitis. The porting guide now supports running multiple kernels, which can be useful for many applications. We have also ported the VexCL sparse matrix library to be able to use sparse matrices in Xilinx Vitis. The new porting guidelines are shown in table~\ref{tab:vexcl_vitis_kernel_new} for the kernel code, and table~\ref{tab:vexcl_vitis_host_new} for the host code.

Our performance results indicate that this kind of applications is better suited for \acp{fpga} than the affine transformation kernel used in chapter~\ref{ch:optimizations}. We can see that the energy usage of the Vitis implementations is closer to the energy usage of the VexCL implementation for the \ac{spmv} application than for the affine transformation application. The energy usage of the Vitis implementations is sometimes even lower than the energy usage of the VexCL implementation.

The memory alignment optimization, and using more memory interfaces, make the Vitis implementation faster, like in chapter~\ref{ch:optimizations}. However, for larger matrices, this also increase the power usage in this application. The fixed-point integer optimization could not be implemented in the phi kernel, due to the complexity of the kernel.

\begin{table}
    \centering
    \footnotesize
    \begin{tabularx}{\textwidth}{|L{1}|L{1}|}
        \hline
        \textbf{VexCL} & \textbf{Xilinx Vitis} \\
        \hline
        \multirow{4}{*}{// Entire kernel} & \#include <stddef.h> \\
        & extern "C" \{ \\
        & \quad// Entire kernel \\
        & \} \\
        \hline
        kernel void vexcl\_vector\_kernel(\dots) \{\dots\} & void \textit{kernel\_function\_name}(\dots) \{\dots\} \\
        \hline
        ulong & size\_t \\
        \hline
        idx = get\_global\_id(0) & idx = 0 \\
        \hline
        idx += get\_global\_size(0) & ++idx \\
        \hline
        \multirow{3}{*}{log/sin/cos} & \#include <math.h> \\
        & \dots \\
        & log/sin/cos \\
        \hline
        \multirow{2}{*}{global \textit{type} *\textit{prm}} & const \textit{type} *\textit{prm} // if \textit{prm} is input buffer \\
        & \textit{type} *\textit{prm} // if \textit{prm} is output buffer \\
        & \dots \\
        & \#pragma HLS INTERFACE m\_axi port=\textit{prm} bundle=aximm\textit{<memory interface No.>} \\
        \hline
    \end{tabularx}
    \caption{Kernel code changes from VexCL to Xilinx Vitis.}
    \label{tab:vexcl_vitis_kernel_new}
\end{table}

\begin{table}
    \centering
    \footnotesize
    \begin{tabularx}{\textwidth}{|L{0.75}|L{1.25}|}
        \hline
        \textbf{VexCL} & \textbf{Xilinx Vitis} \\
        \hline
        \multirow{8}{*}{ctx(vex::Filter::DoublePrecision);} & device = get\_xilinx\_devices().front(); \\
        & \dots \\
        & cl::CommandQueue q(context, device, CL\_QUEUE\_PROFILING\_ENABLE, \&err); \\
        & cl::Kernel \textit{krnl\_name\_1}(program, \textit{kernel\_1\_function\_name}, \&err); \\
        & cl::Kernel \textit{krnl\_name\_2}(program, \textit{kernel\_2\_function\_name}, \&err); \\
        & \dots \\
        & cl::Kernel \textit{krnl\_name\_n}(program, \textit{kernel\_n\_function\_name}, \&err); \\
        \hline
        vex::vector<\textit{cl\_type}> \textit{A}(ctx, \textit{a}.size(), \textit{a}.data()); & cl::Buffer \textit{A}(context, CL\_MEM\_READ\_ONLY | CL\_MEM\_USE\_HOST\_PTR, sizeof(\textit{type}) * \textit{a}.size(), \textit{a}.data(), \&err); \\
        vex::vector<\textit{cl\_type}> \textit{T}(ctx, \textit{t}.size()); & cl::Buffer \textit{T}(context, CL\_MEM\_WRITE\_ONLY | CL\_MEM\_USE\_HOST\_PTR, sizeof(\textit{type}) * \textit{t}.size(), \textit{t}.data(), \&err); \\
        ... & \\
        vex::copy(\textit{T}.begin(), \textit{T}.end(), \textit{t}.data()); & \\
        \hline
        // Temporary value used between kernels & cl::Buffer \textit{tmp}(context, CL\_MEM\_HOST\_NO\_ACCESS, sizeof(\textit{type}) * \textit{a}.size(), \textit{a}.data(), \&err); \\
        \hline
        vex::spMat<\textit{cl\_type}> \textit{A}(ctx, \textit{m}, \textit{n}, & \#include "spmat.hpp" \\
        \textit{A\_row}.data(), \textit{A\_col}.data(), & \dots \\
        \textit{A\_data}.data()); & spMat<\textit{type}> \textit{A}(\textit{m}, \textit{n}, \textit{A\_row}.data(), \textit{A\_col}.data(), \textit{A\_data}.data()); \\
        & auto \textit{A\_ell} = \textit{A}.ell\_mat(); \\
        & // Create input buffers for: \textit{A\_ell}\texttt{->}mat.ell.col, \textit{A\_ell}\texttt{->}mat.ell.val, \textit{A\_ell}\texttt{->}mat.csr.row, \textit{A\_ell}\texttt{->}mat.csr.col, \textit{A\_ell}\texttt{->}mat.csr.val \\
        \hline
        \textit{T} = \textit{X} + \textit{Y}; & \textit{krnl\_name}.setArg(0, \textit{output\_vector\_size});\\
        & \textit{krnl\_name}.setArg(1, \textit{T});\\
        & \textit{krnl\_name}.setArg(2, \textit{X}); \\
        & \textit{krnl\_name}.setArg(3, \textit{Y}); \\
        \hline
        \textit{x} = vex::tag<\textit{n}>(\textit{X}) & \textit{krnl\_name}.setArg(0, \textit{output\_vector\_size});\\
        \textit{T} = \textit{x} + \textit{x}; & \textit{krnl\_name}.setArg(1, \textit{T});\\
        & \textit{krnl\_name}.setArg(2, \textit{X}); \\
        \hline
        reshape(\textit{X}, extents[\textit{m}][\textit{n}], extents[1]) & \textit{krnl\_name}.setArg(0, \textit{output\_vector\_size});\\
        & \textit{krnl\_name}.setArg(1, \textit{X}); \\
        & \textit{krnl\_name}.setArg(2, 1); // skip indices \\
        & \textit{krnl\_name}.setArg(3, 0); // offset \\
        & \textit{krnl\_name}.setArg(4, 1); // repetitions \\
        & \textit{krnl\_name}.setArg(5, \textit{n}); // modulo \\
        \hline
        reduce<OP>(extents[\textit{m}][\textit{n}], \textit{X}, 1) & \textit{krnl\_name}.setArg(0, \textit{output\_vector\_size});\\
        & \textit{krnl\_name}.setArg(1, \textit{X}); \\
        & \textit{krnl\_name}.setArg(2, 0); // offset \\
        & \textit{krnl\_name}.setArg(3, \textit{m}); // first dimension matrix \\
        & \textit{krnl\_name}.setArg(4, \textit{n}); // second dimension matrix \\
        & \textit{krnl\_name}.setArg(5, \textit{n}); // amount of values to reduce each time \\
        & \textit{krnl\_name}.setArg(6, 1); // distance between values \\
        \hline
        // spmat \textit{A}, scalar $\alpha$, and vector \textit{X} & \textit{krnl\_name}.setArg(0, \textit{output\_vector\_size});\\
        T = $\alpha$ * \textit{A} * \textit{X} & \textit{krnl\_name}.setArg(1, $\alpha$);\\
        & \textit{krnl\_name}.setArg(2, \textit{A\_ell}\texttt{->}mat.ell.width); \\
        & \textit{krnl\_name}.setArg(3, \textit{A\_ell}\texttt{->}mat.ell.pitch); \\
        & \textit{krnl\_name}.setArg(4, \textit{A\_ell\_col}); \\
        & \textit{krnl\_name}.setArg(5, \textit{A\_ell\_val}); \\
        & \textit{krnl\_name}.setArg(6, \textit{A\_csr\_row}); \\
        & \textit{krnl\_name}.setArg(7, \textit{A\_csr\_col}); \\
        & \textit{krnl\_name}.setArg(8, \textit{A\_csr\_val}); \\
        & \textit{krnl\_name}.setArg(9, \textit{X}); \\
        & \textit{krnl\_name}.setArg(10, \textit{T}); \\
        \hline
        // Finished computations & q.enqueueMigrateMemObjects(\textit{input\_buffers\_krnl\_1}, 0); \\
        & q.enqueueTask(\textit{krnl\_1\_name}); \\
        & \dots \\
        & q.enqueueMigrateMemObjects(\textit{input\_buffers\_krnl\_n}, 0); \\
        & q.enqueueTask(\textit{krnl\_n\_name}); \\
        & q.enqueueMigrateMemObjects(\textit{output\_buffers}, CL\_MIGRATE\_MEM\_OBJECT\_HOST); \\
        & q.finish(); \\
        \hline
    \end{tabularx}
    \caption{Host code changes from VexCL to Xilinx Vitis.}
    \label{tab:vexcl_vitis_host_new}
\end{table}