\chapter{Introduction}
\section{Context}
\Acp{fpga} are capturing the interest of the \ac{hpc} community to use as accelerator, because of their favourable energy consumption compared to other accelerators, like \acp{gpu}~\cite{Muslim2016, Paulino2020}. It is also becoming easier than ever to program \acp{fpga}, due to ongoing efforts to enable OpenCL\footnote{OpenCL -- \url{https://www.khronos.org/opencl/}}, C or C++ programs to target \acp{fpga}~\cite{Zohouri2016, Paulino2020}. Xilinx currently provides the Xilinx Vitis\footnote{Xilinx Vitis -- \url{https://www.xilinx.com/products/design-tools/vitis/vitis-platform.html}} platform to run C++ and OpenCL programs on Xilinx \acp{fpga}.

However, many frameworks in the \ac{hpc} community, like Halide\footnote{Halide -- \url{https://halide-lang.org/}} and PyTorch\footnote{PyTorch -- \url{https://pytorch.org/}}, aim to make it easier to write high-performance applications, and target GPU-like accelerators without explicitly writing OpenCL or CUDA. Yet most such frameworks only support GPUs. With the emergence of \acp{fpga} in the \ac{hpc} community, adding support for \acp{fpga} in such frameworks can be highly beneficial to understand whether \acp{fpga} can indeed become \ac{hpc} accelerators.

VexCL is a C++ library that adds support for vector arithmetic on a GPU using standard C++ operators~\cite{VexCL}. Just like Halide and PyTorch, VexCL only supports using GPUs as accelerator. In this project we will focus on (1) designing and developing a strategy to run VexCL code on Xilinx \acp{fpga}, using Xilinx Vitis as target language, (2) proposing optimizations to this strategy to improve the performance of the application on \acp{fpga}, and (3) evaluating this strategy using representative applications.

% In this project we specifically look at how we can transform code written using VexCL into \ac{fpga} compatible code. Designing a step-by-step porting guide for this task requires selecting the target platform (i.e. which \ac{fpga} to use), an intermediate representation (e.g. OpenCL or C++), and a set of translation rules from the input to the output. An additional challenge will also be to evaluate the porting guide using representative applications.

\section{Research question and approach}
Our main research question is: \\
\textbf{How can VexCL code be effectively compiled into code for \acp{fpga}?}

To determine how we can compile code from VexCL to Xilinx Vitis for \acp{fpga}, we first need to select which intermediate representation that VexCL can produce (e.g., OpenCL or OpenMP) to use. Thus, our first subquestion is: \\
\textbf{[SQ1] What language supported by VexCL is a convenient intermediate representation for compiling VexCL to Xilinx Vitis code for \acp{fpga}?} \\
To answer this question, we study which intermediate representations VexCL can produce, experiment with these intermediate representations, and study previous work to determine what intermediate representations have already been used in different case-studies.

Once both the input and output languages are selected, we can proceed designing our translation process, driven by the subquestion: \\
\textbf{[SQ2] How can we design a step-by-step guide to convert VexCL code to Xilinx Vitis code that targets \acp{fpga}?} \\
Here we will determine the requirements for our compiler and study design practices for \ac{fpga} compilers. Following our design, we will provide a first prototype compiler based on this design.

Next we evaluate possible general optimizations that can be applied to ported applications. Thus, we propose the following subquestion: \\
\textbf{[SQ3] What optimizations can we apply to applications ported from VexCL to Xilinx Vitis code that improve the performance of the code?} \\
To answer this question, we will research several optimizations for Xilinx \acp{fpga} and apply them to a ported application to test how they perform.

Finally, we plan to validate the step-by-step compilation guide. To do so, we formulate one final subquestion: \\
\textbf{[SQ4] How effective is the compilation guide?} \\
To find out how effective the guide is, we use a more complicated VexCL application, and port it to Xilinx Vitis following our guidelines. We use this application to assess the correctness, completeness, and limitations of our guide.

\section{Ethics}
To support the computer science community, and improve innovation in this field, all the code used in this thesis is available open-source\footnote{Source code -- \url{https://gitlab.com/tristanlaan/vexcl2fpgas}}. Furthermore, we ensure, to the best of our abilities, that all design, implementation, data collection, and analysis included in this thesis are correct and transparent.

Indiscriminate testing and exhaustive experiments with \acp{fpga} can consume significant resources. Therefore, for all our development, testing, and validation, we made use of simulation and emulation for the early stages, and only used the expensive hardware design process for the final sets of experiments.

\section{Thesis Outline}
Chapter~\ref{ch:background} provides background information about VexCL, Xilinx \acp{fpga}, and on previous work in this research area. Then, in chapter~\ref{ch:affinetrans}, we present our step-by-step porting guide with a case study of porting a VexCL application to Xilinx Vitis, here we also evaluate the compilation of the ported application and execution of the application on a Xilinx \ac{fpga}. In chapter~\ref{ch:optimizations} we propose several optimizations that can be applied to a ported application, and evaluate the performance of the optimizations applied to the application we used in our case study. We further evaluate the effectiveness of the porting guide in  chapter~\ref{ch:applications} by testing it on another VexCL application. Lastly, in chapter~\ref{ch:conclusion}, we conclude the effectiveness of our porting guide and propose future work that can be done to improve the porting process of VexCL applications to Xilinx \acp{fpga}.