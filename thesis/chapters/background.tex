\chapter{Background}\label{ch:background}
%-------------------------------------------------------------------------------
%	HIGH-LEVEL SYNTHESIS
%-------------------------------------------------------------------------------
\section{High-Level Synthesis}
Historically, special \acp{hdl}, like Verilog and VHDL, have been used to program \acp{fpga}. Such languages provide an abstract way to describe computer circuits by describing what a module does, instead of its logical implementation~\cite{Thomas2002}. A module in this case is the \ac{hdl} equivalence of a function in a programming language, and represents a logical unit on the \ac{fpga}. A module for example describes which logical gates are used and how they are used together in a high-level description. The \ac{hdl} will then compile this module into a hardware circuit that can be programmed into the \ac{fpga}. To do this, the \ac{hdl} tool has to allocate resources on the \ac{fpga}, schedule operations to clock cycles, and bind them to the resources, bind variables to storage units, and bind transfers to buses.

To make programming \acp{fpga} easier and more wide-spread, \ac{hls} tools have been introduced. A \ac{hls} tool takes a source language as input and compiles it either to a \ac{hdl}, or directly into a hardware circuit~\cite{Coussy2009}. The first \ac{hls} tools were created in the 1970s and became widely used in the mid-2000s~\cite{Martin2009}. \Ac{hls} tools have been using C/C++ languages or derivations of it (such as SystemC) as input language; in more recent years, research has been put in using languages developed specifically for heterogeneous computation, like OpenCL, as source language~\cite{Shagrithaya2013}. \Ac{fpga} manufacturers, like Xilinx and Intel, have developed toolchains that support OpenCL programs making it easier for new users to start using \acp{fpga}~\cite{xilinxVitis, IntelDCP}. The advantage of using OpenCL is that there are many applications that already use OpenCL kernels for \ac{gpu} acceleration.

%-------------------------------------------------------------------------------
%	VEXCL
%-------------------------------------------------------------------------------
\section{VexCL}
VexCL is an OpenCL library that aims to make general purpose computing on \acp{gpu} easier by providing intuitve notation for vector and matrix arithmetic~\cite{VexCL}. Although there are alternative libraries that provide better performance, its ease-of-use and the provided wrapper functions stand out~\cite{Bruce2015}. VexCL currently supports compiling to OpenCL, OpenMP and C++, but also has support for custom backends.

%-------------------------------------------------------------------------------
%	FPGA ARCHITECTURE
%-------------------------------------------------------------------------------
\section{FPGA architecture}
\Ac{fpga} architectures differ from more traditional computer designs in the fact that \acp{fpga} consist of different components. The most important components are \acp{lut}, \ac{dsp} slices, registers and \acp{bram}~\cite{Xilinxaccelarationmanual}.

A \ac{lut} takes a set amount of bits as input and produces a smaller amount of bits based on the input. This means that a \ac{lut} of $k$ bits can execute an arbitrary Boolean circuit consisting of $k$ bits by programming the truth table of the circuit into the \ac{lut}. \Acp{lut} generally have one or two output bits and five or six input bits, but it is possible to chain multiple \acp{lut} to create circuits which support an arbitrary amount of input and output bits.~\cite{Xilinxcbluserguide}

A \ac{dsp} slice consists of more complex components that are specifically targeted towards \ac{dsp} applications. They have more input and output bits than \acp{lut} and can apply more complex operations like multiplication, \ac{simd} arithmetic of up to 48 bits and applying digital filters, like a Gaussian or Bloom filter. These \ac{dsp} targeted operations are also useful for complex arithmetic in more general applications.~\cite{Xilinxdspuserguide}

Registers and \acp{bram} provide a way store data in the process. Registers can store a few bits and \acp{bram} can store larger amounts of data.~\cite{Xilinxcbluserguide}

The components in a \ac{fpga} will, once programmed, be interconnected into a pipeline which executes the design provided by the user.

\subsection{Xilinx Alveo U250 Architecture}
The Xilinx Alveo U250 accelerator card consists of four \acp{slr}. A \ac{slr} can store multiple kernels provided by the user, but a kernel can only target one \ac{slr}. Each \ac{slr} contains general \ac{fpga} components that can be programmed in to a pipeline based on a kernel provided by the user. Each \ac{slr} also connects to 16 GB of \acrshort{ddr}4 memory running at 2400 \ac{mtps}. Additionally, the first \ac{slr} is also connected to the host device via a \acrfull{pcie} Gen 3 connection using 16 lanes with a maximum speed of 8000 \ac{mtps}, and the third \ac{slr} has two network interfaces which both support speeds up to 100 Gb/s. In total, all the \acp{slr} consist of 1,728,000 \acp{lut}, 3,456,000 registers, 12,288 \acp{dsp} slices and 1,280 memory blocks. \cite{XilinxalveoU250DataSheet}

%-------------------------------------------------------------------------------
%	COMPILATION PROCESS
%-------------------------------------------------------------------------------
\section{Compilation process}
Xilinx provides three compilation targets, namely software emulation, hardware emulation and hardware execution. Xilinx provides these emulation targets to be able to test the code without access to a \ac{fpga}, but also because it is significantly faster to compile the code for the emulators than to compile the code for hardware execution. \cite{Xilinxaccelarationmanual}

The software emulator runs the kernel sequentially on the CPU and allows to check the code for correctness, while having a very short compilation time.

The hardware emulator runs the code on a simulated \ac{fpga}, and can be used to estimate the performance of the kernel on a \ac{fpga} and to check if the code would run correctly on a \ac{fpga}. Compiling for the hardware emulator than for the software emulator, and the software emulator is faster than the hardware emulator.

With hardware execution the kernel will be run on the \ac{fpga}, and it can be used to verify that the code runs correctly and to check how fast it performs on the \ac{fpga}. Compiling for hardware execution takes longer than compiling for both the software and hardware emulator.

%-------------------------------------------------------------------------------
%	RELATED WORK
%-------------------------------------------------------------------------------
\section{Related Work}
%say what is related work to you! 
In this section we describe relevant related work. Specifically, we focus on porting applications to other programming languages, and applying optimizations to OpenCL kernels for \acp{fpga}. We note that, because the Xilinx Vitis platform is still very new (it was only released in October of 2019~\cite{XilinxVitisIntroduction}), the research work using the Xilinx Vitis platform is still very limited.

\subsection{Porting}
\citeauthor{Gozillon2020} are currently working on allowing SYCL\footnote{SYCL -- \url{https://www.khronos.org/sycl}} code to target Xilinx \acp{fpga}, using their triSYCL framework\footnote{triSYCL -- \url{https://github.com/triSYCL/triSYCL}}. SYCL is a C++ library that, like VexCL, aims to make it possible to write code for heterogeneous computation without the need to write the kernel and host code separately from each other. Early work from \citeauthor{Gozillon2020} has shown that they have been successful in incorporating the Vitis compiler in the SYCL platform, allowing them to run SYCL applications using a Xilinx \ac{fpga} as accelerator~\cite{Gozillon2020}. Currently they have only verified correctness for a single Xilinx \ac{fpga}, and the performance does not yet reach the performance of Xilinx Vitis, but they are still working to improve this. This work is similar to our work, we both aim to add support for a C++ framework which does not currently support using \acp{fpga} as accelerator, and add possible optimizations to this compilation process. VexCL differs from SYCL however, in the sense that VexCL not only aims to be able to write code for heterogeneous computation in a single file, but also to simplify the code and minimize the knowledge required about heterogeneous computing to be able to write the code. Due to how tied this work is to the SYCL framework, we can not directly use their work to port VexCL applications, but we can take inspiration from their compilation pipeline to possibly integrate the Vitis compiler as VexCL back-end.

\subsection{Optimizing}
Recent works have shown that \ac{gpu}-optimized OpenCL kernels can be altered using certain strategies to be optimized for \acp{fpga}~\cite{Zohouri2016, Paulino2020}. These strategies consist of optimizations such as loop unrolling, shift registers and sliding windows. Optimized programs using such optimization techniques can be up to $66\times$ faster than running a non-optimized program on the \ac{fpga} \cite{Zohouri2016}. Although these works are not specifically targeted to the Xilinx Vitis platform, they could be adapted and integrated into our porting process.
%we always comment why or how the work is related: same idea, same optimizations, OpenCL tricks ... and how our work is different. 