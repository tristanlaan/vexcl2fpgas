\chapter{Improving the performance of a ported application}\label{ch:optimizations}
In chapter~\ref{ch:affinetrans} we provide a porting process to create a Xilinx Vitis application based on a VexCL application. In this chapter we extend this process with several optimizations that can be applied on a newly-ported Vitis application.

\section{Optimizations}
\subsection{Memory alignment}
The GNU memory allocator, used by the compiler of the host code, aligns the memory by eight or sixteen bytes depending on the specific system \cite{GNUlibc}, but Xilinx \acp{fpga} align memory to 4000 bytes internally. So if the host code allocates buffers to transfer data to the \ac{fpga} using the default allocator, the program will internally have to copy the data to a new buffer that is aligned to 4000 bytes \cite{Xilinxaccelarationmanual}. To prevent this from happening, we have to use a special allocator that aligns the data to 4000 bytes from the start. This can be done using the \texttt{posix\_memalign} function, which allows the user to specify the memory alignment to use.

\subsection{Fixed-point arithmetic}
On \acp{gpu} it is common to use floating-point numbers to represent decimal numbers. In this representation one bit is reserved to represent a sign ($s$), a fixed amount of bits is reserved to represent an exponent ($e$) and a fixed amount of bits is reserved to represent a fraction ($d$), the number is then represented as $(-1)^s \times 2^e \times (1.d)_2$. Xilinx \acp{fpga} also support floating-point numbers, but in contrast to \acp{gpu}, Xilinx \acp{fpga} additionally support a fixed-point representation for decimal numbers. This representation uses the format $n + \frac{1}{f}$, where $n$ and $f$ are numbers of a fixed amount of bits.

The biggest drawback of using fixed-point numbers is that while the accuracy of fixed-point numbers is fixed, no matter the size of the number, the limits of the numbers it can represent are lower than a floating-point number of the same amount of bits. So if both very large or very small numbers have to be represented, floating-point numbers are a better choice.

An advantage of fixed-point numbers is that fixed-point arithmetic is more efficient than floating-point arithmetic on Xilinx \acp{fpga}. \citeauthor{Xilinxfixedpoint} show that a fixed-point number implementation of their algorithm, in comparison to a floating-point number implementation, has more than seven times lower latency, more than 80\% lower power requirements and requires eleven times less \acp{lut} in the logic circuit on a Xilinx \ac{fpga} \cite{Xilinxfixedpoint}.

Another Xilinx \acp{fpga} implementation specific advantage is that while floating-point numbers can only be used with single- or double-precision on the \ac{fpga}, Xilinx allows user to specify how many bits are used to store the numbers $n$ and $f$ of the fixed-point number.

\subsection{Burst transfers}
To get data from the \acrshort{ddr} memory interface, the \ac{fpga} has to make a read request, which causes a read latency, similarly after writing data the \ac{fpga} has to wait for a write acknowledgment, which causes a write latency. To reduce the amount of time waiting for the total amount of read requests and write acknowledgments, we can increase the amount of data we request or send in one read or write request. This is called bursting. Bursting can give up to a four to five times performance improvement \cite{XilinxHLSguide}.

\subsection{Saturating data width}
The \acrshort{ddr} memory interface of the \ac{fpga} support a maximum data width of 512 bits, but by default the data width will be matched by the size of the data type. So if you use an \texttt{int} as data type the data width will be 32 bits, and if you use a \texttt{double} as data type the width will be 64 bits. To improve the maximum data transfer rate, it is possible to widen the data width and allow more data to be transferred at the same time. We can for example store sixteen 32-bit integers in a single 512-bit integer and retrieve the original sixteen 32-bit integers using bit shifts.

\section{Applying the optimizations}
\subsection{Memory alignment}\label{subsec:mem_align_strat}
The memory alignment optimization can be easily integrated into the vectors we use, because the \texttt{std::vector} class has support for a custom allocator. In listing~\ref{lst:custom_allocator} we show this custom allocator, which makes use of the \texttt{posix\_memalign} function mentioned before. To use this custom allocator we replace \texttt{std::vector} calls in the host code that was in the form of \texttt{std::vector<T> x(m);}, from the porting process in chapter~\ref{ch:affinetrans}, to \texttt{std::vector<T, aligned\_allocator<T>{}> x(m);}.

\begin{lstfloat}
    \begin{cppcode}
    template <typename T>
    struct aligned_allocator
    {
      using value_type = T;
      T* allocate(std::size_t num)
      {
        void* ptr = nullptr;
        if (posix_memalign(&ptr,4096,num*sizeof(T)))
          throw std::bad_alloc();
        return reinterpret_cast<T*>(ptr);
      }
      void deallocate(T* p, std::size_t num)
      {
        free(p);
      }
    };
    \end{cppcode}
    \caption{A custom aligned allocator that can be used by \texttt{std::vector}.}\label{lst:custom_allocator}
\end{lstfloat}

\subsection{Fixed-point arithmetic}\label{subsec:fixed_strat}
Xilinx provides fixed-point number support through the \texttt{ap\_fixed} and \texttt{ap\_ufixed} data type for signed and unsigned numbers respectively. We can create our own custom fixed-point by declaring it using \texttt{typedef ap\_fixed<$W$, $I$, $Q$, $O$, $N$> fix\_t;}.

$W$ specifies the total size of the number in bits, and $I$ the number of bits used for the non-fraction part of the number. The amount of bits used for the fraction part of the number is automatically calculated by $W - I$. To find the correct number for $W$ and $I$, we have to determine the range of numbers we want to represent. If we for example want to represent numbers ranging from -250 to 240 with a precision of at least 0.01. then we need to reserve at least $\lceil\log_2(\max(250, 240)))\rceil + 1 = 9$ bits for the non-fraction part of the number. Note that we had to add one for the sign, and choose the maximum between $250$ and $240$ because the amount of numbers that can be represented is symmetric around zero. For the fraction part of the number we need at least $\lceil\log_2(\frac{1}{0.01})\rceil = 7$ bits. So in this case we would declare $W=9+7=16$ and $I=9$.

$Q$ represent the quantization mode and describes how numbers should be rounded if the fraction is too small for the fixed-point number. Here you can choose modes to round or truncate to $\pm \infty$ or zero, by default it rounds to $+\infty$.

Lastly, $O$ and $N$ define what happens in case of an overflow. $O$ specifies the overflow mode, by default this is to wrap around like the C \texttt{int} data type, but it is also possible to use saturation, which means that numbers that are too large to be represented will be set to a specific value. For this specific value it is possible to choose the closest number to the represented number or zero. $N$ specifies the amount of saturation bits that are used in the wrap around mode, and specify how many bits will be copied when wrapping around. If $N=1$ for example, positive numbers will stay positive and negative numbers will stay negative. In contrast, if $N=0$ than positive numbers will wrap around to negative values and vice versa, a graphical example of this behaviour can be seen in figure~\ref{fig:fixed_point_range}. By default $N=0$.

\begin{figure}
    \centering
    \includegraphics{images/fixed_point_plot.pdf}
    \caption{Graphical representation of the difference between the amount of saturation bits in fixed-point numbers with wrap around overflow.}
    \label{fig:fixed_point_range}
\end{figure}

Once we have specified our data type, we can simply replace the original \texttt{double} declarations from our porting process in chapter~\ref{ch:affinetrans} with matching \texttt{fix\_t} declarations.

\subsection{Burst transfers}\label{subsec:burst_strat}
Typically our kernels have a structure similar to algorithm~\ref{alg:typ_kernel_structure}, first we read the data, then we do some computation and lastly we store the results. Sometimes we do this in one step, i.e. $out[i] \gets Compute(A[i], B[i])$, but then we have to first split it up in multiple steps before we can apply this optimization.

\begin{algfloat}
\caption{A typical kernel structure.}\label{alg:typ_kernel_structure}
\begin{algorithmic}[1]
\For{i}{0}{n}{1}
    \State $a \gets A[i]$
    \State $b \gets B[i]$
    \State $c \gets Compute(a, b)$
    \State $out[i] \gets c$
\EndFor
\end{algorithmic}
\end{algfloat}

To make use of burst transfers in algorithm~\ref{alg:typ_kernel_structure}, we increase the step size of the for-loop, and add new for-loops around the read part, compute part and write part. If we add \texttt{max\_read\_burst\_length} and \texttt{max\_write\_burst\_length} to the memory pragmas we specified in chapter~\ref{ch:affinetrans}, Xilinx can automatically use burst transfers for the new small for-loops. In algorithm~\ref{alg:typ_opt_kernel_structure} we show the new optimized kernel, which uses a burst size of 16. Note that the temporary variables have become temporary arrays, and that we have to add a boundary check for the case that $n$ is not divisible by $burst$.

\begin{algfloat}
\caption{A typical kernel structure with burst transfers of 16 elements.}\label{alg:typ_opt_kernel_structure}
\begin{algorithmic}[1]
\State $burst \gets 16$
\For{i}{0}{n}{burst}
    \State $chunk \gets burst$
    \If{$(i + burst) > n$}\Comment{Boundary check}
        \State $chunk \gets n - i$
    \EndIf
    \Statex
    \For{j}{0}{chunk}{1}
        \State $a[j] \gets A[i + j]$
        \State $b[j] \gets B[i + j]$
    \EndFor
    \Statex
    \For{j}{0}{chunk}{1}
        \State $c[j] \gets Compute(a[j], b[j])$
    \EndFor
    \Statex
    \For{j}{0}{chunk}{1}
        \State $out[i + j] \gets c[j]$
    \EndFor
\EndFor
\end{algorithmic}
\end{algfloat}

\subsection{Saturating data width}\label{subsec:data_width_strat}
To take advantage of saturated data widths, we have to make sure that our kernel can take advantage of being able to read multiple data elements at once, i.e. we have to make sure that our algorithm already makes use of burst transfers. So to show how to implement this optimization, we will start with the kernel specified in algorithm~\ref{alg:typ_opt_kernel_structure}. If we assume that $A$, $B$ and $out$ are arrays of doubles, then the current data width will be 64-bits. To increase the data width we can create a \texttt{struct} that consists of multiple doubles, then the \texttt{struct} has a larger data width than 64-bits. If we want to achieve a data width of 512-bits, we can create a \texttt{struct} that consists of $\frac{512}{64} = 8$ doubles, as shown in listing~\ref{lst:double_struct}. Now we can use this new data type in our algorithm, as can be seen in algorithm~\ref{alg:typ_opt_kernel_structure_struct}. We add a new $width$ variable to denote the amount of elements in the struct, and use bit shifts and moduli to retrieve the original data. Note that we do not need to change the host code, as the \texttt{struct} data is still in the same location when using the structs.

\begin{lstfloat}
    \begin{cppcode}
        typedef struct {
            double data[8];
        } double_v;
    \end{cppcode}
    \caption{Example \texttt{struct} consisting of eight doubles to saturate the data width.}\label{lst:double_struct}
\end{lstfloat}

\begin{algfloat}
\caption{Typical kernel structure with burst transfers and higher data width.}\label{alg:typ_opt_kernel_structure_struct}
\begin{algorithmic}[1]
\State $burst \gets 16$
\State $width \gets 8$
\State $logwidth \gets 3$
\For{i}{0}{n}{burst}
    \State $chunk \gets burst$
    \If{$(i + burst) > n$}\Comment{Boundary check}
        \State $chunk \gets n - i$
    \EndIf
    \Statex
    \For{j}{0}{chunk}{1}
        \State $k \gets i + j$
        \State $a[j] \gets A[k >> logwidth].data[k \mod width]$
        \State $b[j] \gets B[k >> logwidth].data[k \mod width]$
    \EndFor
    \Statex
    \For{j}{0}{chunk}{1}
        \State $c[j] \gets Compute(a[j], b[j])$
    \EndFor
    \Statex
    \For{j}{0}{chunk}{1}
        \State $k \gets i + j$
        \State $out[k >> logwidth].data[k \mod width] \gets c[j]$
    \EndFor
\EndFor
\end{algorithmic}
\end{algfloat}

\section{Testing the optimizations}
To see how the optimizations perform, we will apply them to the ported affine transformation application as described in chapter~\ref{ch:affinetrans}, and compare their performance and power usage by running them with different input sizes and using different profiling tools to extract the execution time and power usage.

\subsection{Implementations}\label{subsec:optimizations}
To test the optimizations we propose five optimized versions of the affine transformation application from chapter~\ref{ch:affinetrans} and compare them to the unoptimized vitis port and the original VexCL application from chapter~\ref{ch:affinetrans}.

\subsubsection{Common optimizations}
The first optimization we propose, optimization 1, applies the memory alignment strategy described in subsection~\ref{subsec:mem_align_strat} to the unoptimized version of the affine transformation application. This optimization also inlines the unnecessary \texttt{SUM\_DOUBLE} function. Lastly this optimization rearranges the memory interfaces, $t$ keeps using the first interface, $y$ and $A$ will use the second interface and $x$ will use the third interface. This rearrangement makes sure that data that can be read at the same time does not use the same interface. We use this optimization to test how much influence applying common optimizations have on the performance of the application.

\subsubsection{Fixed-point arithmetic optimization}
The second optimization, optimization 2, builds upon optimization 1 by replacing the double-precision floating-point data types with fixed-point data types as described in subsection~\ref{subsec:fixed_strat}. We specify two new data types, $\texttt{fix\_t} = \texttt{ap\_fixed<18, 7, AP\_RND, AP\_SAT>}$ and $\texttt{lfix\_t} = \texttt{ap\_fixed<64, 54, AP\_RND, AP\_SAT>}$. The \texttt{fix\_t} data type can represent numbers from $-64$ to $64$ with a precision of $\frac{1}{2048} \approx 3.88 \cdot 10^{-4}$ and the \texttt{lfix\_t} data type can represent numbers from $-2^{53}$ to $2^{53}$ with the same precision as \texttt{fix\_t}. We will replace the data types of the input buffers $A$, $x$ and $y$ with the \texttt{fix\_t} data type, and replace the data types of the output buffer $t$ and internal variable \texttt{prm\_3\_sum} with the \texttt{lfix\_t} data type. We use this optimization to test if fixed-point numbers indeed have higher performance than floating-point numbers on Xilinx \acp{fpga}.

\subsubsection{Burst transfer optimization}
Optimization 3 applies the burst transfer strategy, as described in subsection~\ref{subsec:burst_strat}, to optimization 2. We choose a burst size of 256, because the memory bus supports bursts of up to 16 kib. We include this optimization to test if burst transfers have an impact on the performance of the application.

\subsubsection{Data width saturation optimization}
Optimization 4 adapts optimization 3 to saturate the data width of the burst transfers, as described in subsection~\ref{subsec:data_width_strat}. To accomplish this, we present two new data types, $\texttt{fix\_v} = \texttt{struct \{fix\_t data[16]\}}$ and $\texttt{lfix\_v} = \texttt{struct \{lfix\_t data[8]\}}$. We change the data types of $A$, $x$ and $y$ from \texttt{fix\_t} to \texttt{fix\_v} and the data type of $t$ from \texttt{lfix\_t} to \texttt{lfix\_v}. We add this optimization to see if data width saturation improves the performance of burst transfers in the application.

\subsubsection{Smaller fixed-point numbers optimization}
The last optimization, optimization 5, is the same as optimization 4, but with a small change to the \texttt{lfix\_t} data type. $\texttt{lfix\_t} = \texttt{ap\_fixed<64, 54, AP\_RND, AP\_SAT>}$ is redefined to $\texttt{lfix\_t} = \texttt{ap\_fixed<44, 33, AP\_RND, AP\_SAT>}$, so the precision stays the same, but the data type can now only represent numbers from $-2^{32}$ to $2^{32}$, instead of numbers from $-2^{53}$ to $2^{53}$. We include this optimization to see how much impact the size of the fixed-point numbers has on the performance of the application.

\subsection{Input sizes}\label{subsec:input_sizes}
As described in chapter~\ref{ch:affinetrans}, the application calculates an affine transformation defined as $\Vec{t} = \Vec{y} + A\Vec{x}$, so if the matrix $A$ is a $m {\times} n$ matrix, then $\Vec{t}$ and $\Vec{y}$ should be of size $m$ and $\Vec{x}$ should be of size $n$. So to describe the input sizes, we only have to define the size of matrix $A$, and the sizes of the vector can be derived from the size of the matrix. We will test the application with matrix sizes $32 {\times} 32$, $256 {\times} 256$, $1024 {\times} 1024$, $5000 {\times} 5000$ and $10^4 {\times} 10^4$ to see how the applications perform as the size increases. Note that we moved away from powers of 2 with the two largest matrices, because this might be a disadvantage for optimization 3, 4 and 5, as they all have burst sizes of 256 and both $5000 {\times} 5000$ and $10^4 {\times} 10^4$ are not divisible by $256$. We also test matrix sizes of $2000 {\times} 2000$, $2 {\times} 2 {\cdot} 10^6$ and $2 {\cdot} 10^6 {\times} 2$ to see how good the applications perform when the size of the matrix is unbalanced, compared to a square matrix.

\subsection{Hardware}
All the testing will be done on a machine with a 12-core Intel Xeon Gold 6128 \ac{cpu}, 187 GiB of RAM capacity, a NVIDIA RTX 2080Ti \ac{gpu} and a Xilinx Alveo U250 \ac{fpga}. 

\subsection{Profiling tools}
In all implementation we measure the time it takes to execute the application, excluding the input data initialization and correctness verification, which we refer to as the \textit{wall time} for simplicity. To measure the wall time, we use the steady clock from the C++ standard library. On the \ac{fpga} implementations we also use the Xilinx profiler to measure the kernel execution time, and the power usage of the \ac{fpga}. For the VexCL implementation we only measure the power usage by running the \ac{nvidiasmi} program\footnote{nvidia-smi -- \url{https://developer.nvidia.com/nvidia-system-management-interface}} in the background, which can measure the power draw of a NVIDIA \ac{gpu}, while running the application.

Both the Xilinx profiler and the \ac{nvidiasmi} program measure the power usage over time, this will generally stay the same while running the application. However because we run the \ac{nvidiasmi} program in the background, the profiler of the NVIDIA \ac{gpu} will start a little earlier than the program we want to measure, so to account for this, we drop values that are smaller or equal to 1 watt.

\subsection{Method}
To compare all the implementations, we first generate input data of different sizes, as described in subsection~\ref{subsec:input_sizes}. We then run all the different implementations with the same input data and the profiling tools active to retrieve the measurement data. We repeat this experiment ten times, with different input data each run. We then calculate the average and standard deviation of the wall time, kernel time and power usage from all ten runs. Note that the power usage will be averaged over more than ten values, because the power usage is measured multiple times during each run. We can also approximate the total energy usage from the wall time and power usage, by calculating $E = P \cdot t$, where $E$ is the approximate total energy usage in watt-hours (Wh), $P$ is the average power usage in watts (W) and $t$ is the execution time in hours (h).

The input data of the larger matrices can be very large, the largest matrix has a size of $\frac{10^4 \cdot 10^4 \cdot 8}{10^6} = 800$ MB, if it is efficiently encoded, if we use the \ac{json} format as described in chapter~\ref{ch:affinetrans}, it will be approximately 2 GB. To be able to load this data efficiently into the application we need a faster solution than our \ac{json} implementation, so we use a Python script to generate C code with the input data hard-coded into arrays, and then compile this C code to a shared object that is linked to all the implementations.

\subsection{Results}
All the raw averaged data from the results of the experiment can be found in appendix~\ref{ch:test_results}.
\subsubsection{Resource utilization}
\begin{table}
    \centering
    \begin{tabular}{c|cccc}
        \textbf{Version} & \textbf{\acp{lut}} & \textbf{\acp{bram}} & \textbf{Registers} & \textbf{\ac{dsp} slices} \\
        \hline
        Unoptimized & 7,346 (0.43\%) & 2 (0.07\%) & 10,022 (0.34\%) & 17 (0.14\%) \\
        Optimization 1 & 8,074 (0.47\%) & 3 (0.11\%) & 11,138 (0.38\%) & 17 (0.14\%) \\
        Optimization 2 & 7,285 (0.42\%) & 2 (0.07\%) & 9,317 (0.32\%) & 7 (0.06\%) \\
        Optimization 3 & 8,921 (0.52\%) & 17 (0.63\%) & 10,344 (0.35\%) & 7 (0.06\%) \\
        Optimization 4 & 11,152 (0.62\%) & 26 (0.97\%) & 19,116 (0.65\%) & 7 (0.06\%) \\
        Optimization 5 & 8,734 (0.51\%) & 17 (0.63\%) & 10,202 (0.35\%) & 7 (0.06\%)
    \end{tabular}
    \caption{Resource utilization of Xilinx Vitis kernels compiled for a Xilinx Alveo U250 accelerator card.}
    \label{tab:resource_utilization}
\end{table}

In table~\ref{tab:resource_utilization} we show the resource utilization of the compiled kernels on the \ac{fpga}. We can see that optimization 1 uses more resources than the unoptimized kernel. We can also see that optimization 2 uses less resources than both optimization 1 and the unoptimized version. Optimization 3 has a large increase in resources compared to the previous versions, especially in \ac{bram} usage. Optimization 4 uses even more resources, and uses the most resources of all implementations. Lastly optimization 5 uses less resources than the version it was based off, optimization 3.

\subsubsection{Wall time}
\begin{figure}
    \begin{subfigure}{0.55\linewidth}
        \centering
        \includegraphics{images/affine_results/walltime_0_plot.pdf}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.45\linewidth}
        \centering
        \includegraphics{images/affine_results/walltime_1_plot.pdf}
        \caption{}
    \end{subfigure}\\[1ex]
    \begin{subfigure}{\linewidth}
        \centering
        \includegraphics{images/affine_results/walltime_2_plot.pdf}
        \caption{}
    \end{subfigure}

    \caption{The wall time of the different affine transformation implementations with different matrix configurations, averaged over ten runs.}
    \label{fig:walltime_results}
\end{figure}

% \begin{figure}
%     \begin{subfigure}{0.55\linewidth}
%         \centering
%         \includegraphics{images/affine_results/kerneltime_0_plot.pdf}
%         \caption{}
%     \end{subfigure}
%     \begin{subfigure}{0.45\linewidth}
%         \centering
%         \includegraphics{images/affine_results/kerneltime_1_plot.pdf}
%         \caption{}
%     \end{subfigure}\\[1ex]
%     \begin{subfigure}{\linewidth}
%         \centering
%         \includegraphics{images/affine_results/kerneltime_2_plot.pdf}
%         \caption{}
%     \end{subfigure}

%     \caption{Kernel time}
%     \label{fig:kerneltime_results}
% \end{figure}

In figure~\ref{fig:walltime_results} we show the average wall time of each implementation per dimension.

We can see that the VexCL implementation is generally faster, especially as the matrix grows bigger, except for the wide  $2 {\times} 2 {\cdot} 10^6$ matrix.

Optimization 1 is always faster than the unoptimized Vitis implementation, generally around 25\%, except for the tall matrix, there optimization 1 is only about 5\% faster than the unoptimized version.

Optimization 2 is mostly between 5\% and 20\% faster than optimization 1, except for the $1024 {\times} 1024$ matrix, where it is 3\% slower.

Looking at optimization 3, we can see that, while it is 18\% faster than optimization 2 for the $32 {\times} 32$ matrix and 8\% faster for the $256 {\times} 256$ matrix, it gets slower as the matrices grow. Optimization 3 is 3\% slower than optimization 2 for the $1024 {\times} 1024$ matrix and 17\% slower for the $10^4 {\times} 10^4$ matrix.

Optimization 4 is almost always between 25\% and 30\% slower than optimization 3, except for the tall $2 {\cdot} 10^6 {\times} 2$ matrix, where it is 14\% faster than optimization 3.

Optimization 5 is only slower than optimization 3 with the $32 {\times} 32$ matrix, where it is 20\% slower. With the tall $2 {\cdot} 10^6 {\times} 2$ matrix Optimization 5 is about as fast as optimization 3, and with the other matrices it is between 7\% and 9\% faster.

\subsubsection{Power usage}
\begin{figure}
    \centering
    \includegraphics{images/affine_results/power_plot.pdf}
    \caption{The power usage of the different affine transformation implementations with different matrix configurations, averaged over ten runs.}
    \label{fig:power_results}
\end{figure}

In figure~\ref{fig:power_results} we show the average power usage of each implementation per dimension.

We can see that the VexCL version, running on the \ac{gpu}, always uses more power than the Vitis versions, running on the \ac{fpga}. Generally the VexCL version uses about 50 W of power, which is between 50\% and 75\% more power than the unoptimized Vitis version uses. Only with the wide $2 {\times} 2 {\cdot} 10^6$ matrix it uses more power, at the start of the computation it uses about 50 W of power, but at the end of the computation it uses about 80 W of power. On average it uses 69 W of power for the wide matrix.

The power consumption of the \ac{fpga} is very close for all Vitis versions, between 27 W and 32 W. Optimizations 2, 3, 4 and 5 all use about the same amount of power, 32 W. Optimization 1 generally uses a little less power, between 28 and 30 W of power, except for the largest two matrices and the wide matrix, where it also uses about 32 W of power. The unoptimized version always uses between 27 W and 28 W of power.

\subsubsection{Approximate energy usage}
\begin{figure}
    \begin{subfigure}{0.55\linewidth}
        \centering
        \includegraphics{images/affine_results/energy_0_plot.pdf}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.45\linewidth}
        \centering
        \includegraphics{images/affine_results/energy_1_plot.pdf}
        \caption{}
    \end{subfigure}\\[1ex]
    \begin{subfigure}{\linewidth}
        \centering
        \includegraphics{images/affine_results/energy_2_plot.pdf}
        \caption{}
    \end{subfigure}

    \caption{The approximate energy usage of the different affine transformation implementations with different matrix configurations, derived from the wall time and power usage results.}
    \label{fig:energy_results}
\end{figure}

In figure~\ref{fig:energy_results} we show the approximate energy usage of each implementation per dimension. We can see that even though the VexCL implementation uses more power, it still generally has a lower energy usage, because it has a shorter execution time. Only for the wide $2 {\times} 2 {\cdot} 10^6$ matrix the VexCL version has a higher energy usage than the Vitis versions, because of the higher execution time. Because the power usage of the Vitis implementations was very close, the differences in energy usage between the implementation is about the same as for the execution time.

\subsection{Discussion}
Looking at the resource utilization of optimization 4, we could have predicted that this optimization would be slower than the other optimization due to the high \ac{bram} usage. A probable cause for this is that the Xilinx Vitis compiler can not handle the \texttt{structs} we used very efficiently. We can also see that the fixed-point numbers from optimization 2 indeed use less resources than the floating-point numbers from optimization 1, and that the fixed-point number with a smaller range in optimization 5 use less resources than the fixed-point numbers in optimization 3.

The results indicate that the common optimizations help with increasing the performance of the application.

The fixed-point arithmetic used in optimization 2 seems to indeed be faster than the floating-point arithmetic used in optimization 1. It also, however, seems to use more power than the floating-point arithmetic: the power usage of optimization 2 is higher than that of optimization 1. We can also see that fixed-point numbers with a smaller range seem to have higher performance than fixed-point numbers with a larger range, because our results indicate that optimization 5 is generally faster than optimization 3.

The wall-time results comparison between optimizations 2 and 3 indicate that the burst transfers from optimization 3 only improve the performance of the application when the matrix is small, and seem to hurt the performance when the matrix is large. The performance penalty of the burst transfers could be caused by the increased computation complexity, and because the results of the summation for the matrix multiplication must be written to the same variable.

The data width saturation technique likely degrades the performance of the application, as we see that optimization 4 is, in almost all, cases slower than optimization 3. This behaviour was to be expected, given the measured resource utilization.

In general, the Vitis implementations do not reach the performance of the VexCL implementation running on the GPU. Especially as the input size gets larger, the relative difference in execution time between the VexCL version and the Vitis version increases. This indicates that the VexCL version can better parallelize the application on the \ac{gpu} than the Vitis versions can on the \ac{fpga}. Only the very wide matrix performs worse in the VexCL implementation, likely due to the fact that the VexCL version only parallelizes the outer loop, and runs the inner loop sequentially.

\section{Summary}
Our empirical analysis indicates that, even with the \textit{successful} proposed optimizations, our Vitis implementation running on the \ac{fpga} is neither faster, nor more energy efficient than the VexCL implementation running on the \ac{gpu}.

We can also conclude that using aligned memory allocation, inlining small functions, and using more memory interfaces generally improves the performance of the application.

If the disadvantages of fixed-point numbers are acceptable for the application, the performance of the application can be improved by replacing floating-point numbers with fixed-point numbers, and to get the most performance out of the fixed-point numbers, the range should be set as small as the application requirements allow.

The burst transfer optimization and the data saturation optimization both generally do not increase the performance of the applications and most of the time decrease the performance of the application, so these optimizations should not be applied in their current form.